#!/usr/bin/python2.7
import os, sys, re, textwrap

def main():
  os.chdir(sys.path[0] + "/../projects/premake")

  # generate projects
  for projects in [ "xcode4", "vs2017", "gmake2" ]:
  	print("Building " + projects)
  	os.system("./premake5 " + projects)

if __name__ == "__main__":
  main()