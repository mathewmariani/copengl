workspace "lite"
  configurations { "Debug", "Release" }
  platforms { "64bit", "32bit" }
  defaultplatform "64bit"
  startproject "lite"
  location ("../" .. _ACTION)

  filter "configurations:Debug"
    targetsuffix "_d"
    defines { "DEBUG" }
    symbols "On"

  filter "configurations:Release"
    defines { "nDEBUG" }
    optimize "On"

  filter { "action:vs2017" }
    location ("../vs2017")
    
  filter { "action:xcode4" }
    location ("../xcode4")

  filter { "action:gmake2" }
    location ("../make")

  filter { "action:gmake2", "system:macosx" }
    location ("../make.mac")

  filter { "platforms:32bit" }
    architecture "x86"

  filter { "platforms:64bit" }
    architecture "x86_64"

  filter { "system:windows" }
    systemversion "latest"
    defines { "_CRT_SECURE_NO_WARNINGS" }

  filter { "system:linux" }
    links { "m" }

project "lite"
  kind "ConsoleApp"
  language "C"
  cdialect "C99"
  targetdir "../../bin"
  basedir "../../bin"
  debugdir "../../bin"

  links {
    "SDL2",
    "wren",
  }

  files {
    "../../src/**.c",
    "../../src/**.h",
  }

  includedirs {
    "../../src",
    "../../src/modules",
  }

if os.host() == "windows" then
  links { "OpenGL32" }

  sysincludedirs {
    os.getenv("SDL2DIR").."/include",
    os.getenv("WRENDIR").."/src/include",
  }

  libdirs {
    os.getenv("SDL2DIR").."/lib/x86",
    os.getenv("WRENDIR").."/lib",
  }
elseif os.host() == "macosx" then
  links { "/Library/Frameworks/OpenGL.framework" }

  sysincludedirs {
    "/usr/local/include",
  }

  libdirs {
    "/usr/local/lib",
  }
end
