/* stdlib */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* engine */
#include "platform.h"
#include "graphics.h"
#include "window.h"
#include "palette.h"
#include "layer.h"
#include "image.h"

/* glew */
#include "lib/gl3w/gl3w.h"

/* scripts */
// #include "embed/vertex.vs.inc"
// #include "embed/fragment.fs.inc"

// FIXME: use this
#include "embed/vertex.vs.h"
#include "embed/fragment.fs.h"

// NOTE: used for debugging
static GLenum glCheckError_(const char *title, const char *file, int line) {
  GLenum errorCode;
  while ((errorCode = glGetError()) != GL_NO_ERROR) {
    const char *error;
    switch (errorCode) {
    case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
    case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
    case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
    case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
    case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
    case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
    case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
    }
    printf("[%s] %s | %s (%d)\n", title, error, file, line);
  }
  return errorCode;
}

#define glCheckError(t) glCheckError_(t, __FILE__, __LINE__)

#define MAX_LAYERS 3

#define MIN(a, b)           ((b) < (a) ? (b) : (a))
#define MAX(a, b)           ((b) > (a) ? (b) : (a))
#define CLAMP(x, a, b)      (MAX(a, MIN(x, b)))
#define LERP(bits, a, b, p) ((a) + ((((b) - (a)) * (p)) >> (bits)))


// When the draw state has a clipping rectangle set,
// all draw operations will not affect any pixels
// in the graphics buffer outside of this rectangle.
// This is useful for reserving parts of the screen

// When called without arguments,
// the function resets the clipping region to be the entire screen

typedef struct {
  int32_t x;
  int32_t y;
  int32_t w;
  int32_t h;
} clip_t;

typedef struct {
  bool enabled;
  uint8_t blend;
} blend_t;

/* opengl resources */
GLuint texture;

#ifdef USE_PALETTE_IMAGE
  GLuint palette_texture;
#endif

/* state */
uint32_t background;
uint8_t blendTable[MAX_LAYERS];

#ifdef USE_INDEXED_LAYER
  uint8_t color_index[2];
#else
  #ifdef USE_COLOR_LAYER
    color_t color[2];
  #else
    uint32_t color[2];
  #endif
#endif

color_t *palette;
uint16_t pattern = 0b1110101011100000;

/* layers */
layer_t layers[MAX_LAYERS];
layer_t *current_layer;

// FIXME: not being used right now
uint32_t *mask;
clip_t clip;


/* set up vertex data
 *
 * 0-------1
 * |     / |
 * |   /   |
 * | /     |
 * 3-------2
 *
 * triangle-1: 0-1-3 (cw)
 * triangle-2: 1-2-3 (cw) 
 */

/* set up vertex data */
float vertices[] = {
  /* vertices */        /* texture coords */
   1.0f,  1.0f, 0.0f,   1.0f, 0.0f,
   1.0f, -1.0f, 0.0f,   1.0f, 1.0f,
  -1.0f, -1.0f, 0.0f,   0.0f, 1.0f,
  -1.0f,  1.0f, 0.0f,   0.0f, 0.0f 
};

/* set up element data */
unsigned int indices[] = {
  0, 1, 3,
  1, 2, 3,
};

unsigned int shaderProgram;
unsigned int vao, vbo, ebo;

static inline bool check_clipping(int x, int y) {
  return (x < clip.x || x >= clip.w || y < clip.y || y > clip.h);
}

#ifdef USE_INDEXED_LAYER
  static inline uint8_t peek(uint8_t *vram, int x, int y) {
    return *(vram + x + y * GRAPHICS_WIDTH);
  }

  static inline void poke(uint8_t *vram, int x, int y, uint8_t value) {
    if (!check_clipping(x, y)) {
      *(vram + x + y * GRAPHICS_WIDTH) = value;
    }
  }
#else
  static inline color_t peek(color_t *vram, int x, int y) {
    return *(vram + x + y * GRAPHICS_WIDTH);
  }

  static inline void poke(color_t *vram, int x, int y, color_t value) {
    *(vram + x + y * GRAPHICS_WIDTH) = value;
  }
#endif

// inline uint32_t rgba(const color_t *c) {
//   return (0xff << 24) | (c->b << 16) | (c->g << 8) | (c->r << 0);
// }

graphics_t *graphics_create(void) {
  graphics_t *graphics = LITE_MALLOC(sizeof(graphics_t));
  *graphics = (graphics_t) {
    /* body */
  };

  return graphics;
}

void graphics_destroy(graphics_t *graphics) {
  free(graphics);
}

void graphics_init(void) {
  printf("graphics initialized\n");

  // if (gl3wInit()) {
  //   fprintf(stderr, "failed to initialize OpenGL\n");
  //   return;
  // }

  // initialize 4 layers
  for (int i = 0; i < MAX_LAYERS; i++) {
    layer_init(&layers[i], GRAPHICS_WIDTH, GRAPHICS_HEIGHT);
  }

  // make the default buffer layer0
  current_layer = &layers[0];

  /* clipping region */
  // clip.x = 0;
  // clip.y = 0;
  // clip.w = GRAPHICS_WIDTH;
  // clip.h = GRAPHICS_HEIGHT;
  graphics_setClip(0, 0, GRAPHICS_WIDTH, GRAPHICS_HEIGHT);

  /* bitstring pattern */
  pattern = 0b1110101011100000;

  /* initialize blendtable */
  for (int i = 0; i < MAX_LAYERS; i++) {
    blendTable[i] = 0;
  }

  glViewport(0.0f, 0.0f, GRAPHICS_WIDTH, GRAPHICS_HEIGHT);

  glEnable(GL_TEXTURE_2D);

  /* set blending */
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  /* culling */
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CW);  

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) 0);
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) (3 * sizeof(float)));
  glEnableVertexAttribArray(1);  

  /* build and compile our shader program */

  /* vertex shader */
  unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, (const char *) vertex_vs, NULL);
  glCompileShader(vertexShader);

  /* check for shader compile errors */
  int success;
  char infoLog[512];
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
  }

  /* fragment shader */
  unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragment_fs, NULL);
  glCompileShader(fragmentShader);

  /* check for shader compile errors */
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
  }

  /* link shaders */
  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);

  /* check for linking errors */
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

  if (!success) {
    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
  }

  /* cleanup resources */
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  /* attach shader */
  glUseProgram(shaderProgram);

  glUniform1i(glGetUniformLocation(shaderProgram, "image"), 0);
  glUniform1i(glGetUniformLocation(shaderProgram, "image2"), 1);

  /* create opengl texture */
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);

  /* texture filtering */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  /* texture wrapping */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_CLAMP_TO_EDGE);

  /* this needs to be called at lease once */
#ifdef USE_INDEXED_LAYER
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, GRAPHICS_WIDTH, GRAPHICS_HEIGHT, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
#else
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, GRAPHICS_WIDTH, GRAPHICS_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
#endif

#ifdef USE_PALETTE_IMAGE
  /* create opengl texture */
  glGenTextures(1, &palette_texture);
  glBindTexture(GL_TEXTURE_2D, palette_texture);

  /* texture filtering */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  /* texture wrapping */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_CLAMP_TO_EDGE);

  /* this needs to be called at lease once */
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glCheckError("create palette texture");
#endif
}

void graphics_deinit(void) {
  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);
  glDeleteBuffers(1, &ebo);
  glDeleteProgram(shaderProgram);
}

void graphics_set_mode(int width, int height) {
#ifdef USE_GL3W
  /* initialize opengl functions */
  if (gl3wInit()) {
    fprintf(stderr, "Failed to initialize OpenGL.\n");
  }
#endif

  /* FIXME: break this up */
  graphics_init();
}

void graphics_present(void) {
  #define BLEND_REP(dst, src)\
    *dst = src;

  #define BLEND_ADD(dst, src)\
    r = MIN(((*dst >> 0) & 0xFF) + ((src >> 0) & 0xFF), 0xFF);\
    g = MIN(((*dst >> 8) & 0xFF) + ((src >> 8) & 0xFF), 0xFF);\
    b = MIN(((*dst >> 16) & 0xFF) + ((src >> 16) & 0xFF), 0xFF);\
    a = (*dst >> 24) & 0xFF;\
    *dst = (a << 24) | (b << 16) | (g << 8) | (r);

  #define BLEND_SUBT(dst, src)\
    if ((*dst & 0x00FFFFFF) != 0x0) {\
    r = MAX(((*dst >> 0) & 0xFF) - ((src >> 0) & 0xFF), 0x00);\
    g = MAX(((*dst >> 8) & 0xFF) - ((src >> 8) & 0xFF), 0x00);\
    b = MAX(((*dst >> 16) & 0xFF) - ((src >> 16) & 0xFF), 0x00);\
    a = (*dst >> 24) & 0xFF;\
    *dst = (a << 24) | (b << 16) | (g << 8) | (r);\
    }

  #define BLEND_AVG(dst, src)\
    r = MIN((((*dst >> 0) & 0xFF) + ((src >> 0) & 0xFF) * 0.5f), 0xFF);\
    g = MIN((((*dst >> 8) & 0xFF) + ((src >> 8) & 0xFF) * 0.5f), 0xFF);\
    b = MIN((((*dst >> 16) & 0xFF) + ((src >> 16) & 0xFF) * 0.5f), 0xFF);\
    a = (*dst >> 24) & 0xFF;\
    *dst = (a << 24) | (b << 16) | (g << 8) | (r);

  /* clear */
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

#ifdef USE_INDEXED_LAYER
  uint8_t* buffer = current_layer->buffer;
#else
  #ifdef USE_COLOR_LAYER
    color_t* buffer = current_layer->buffer;
  #else
    uint32_t* buffer = current_layer->buffer;
  #endif
#endif

  /* 
   * glTexSubImage2D is usually much faster
   * since it only updates pixel inside the existing texture, 
   * while glTexImage2D creates entirely new texture
   *
   * https://stackoverflow.com/questions/3887636/how-to-manipulate-texture-content-on-the-fly/10702468#10702468
   *
   * Another good option would be FBO (Frame Buffer Object) or PBO (Pixel Buffer Object)
   * https://stackoverflow.com/questions/9863969/updating-a-texture-in-opengl-with-glteximage2d
   */

  glCheckError("Update pixel data");

  // FIXME: doesn't need to be called every frame
  // since we only have one shader
  // /* attach shader */
  // glUseProgram(shaderProgram);

  // FIXME: doesn't need to be called every frame
  // since we only have one shader
  // glUniform3fv(glGetUniformLocation(shaderProgram, "u_palette"),
  //   9, palette_getPalette());

  /* bind texture to texture unit */
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);

  /* update existing texture */
#ifdef USE_INDEXED_LAYER
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, GRAPHICS_WIDTH, GRAPHICS_HEIGHT, GL_RED, GL_UNSIGNED_BYTE, (void *)buffer);
#else
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, GRAPHICS_WIDTH, GRAPHICS_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, (void *)buffer);
#endif


#ifdef USE_PALETTE_IMAGE
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, palette_texture);
#endif

  /* draw fullscreen quad */
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  glCheckError("Draw");

  /* swap buffer */
  window_swap();
}

void graphics_setBackground(uint8_t index) {
  palette_idxToColor(index, &background);
}

void graphics_setColor(uint16_t index) {
  uint8_t low = index & 0xFF;
  uint8_t high = (index >> 8) & 0xFF;

#ifdef USE_INDEXED_LAYER
  color_index[0] = low;
  color_index[1] = high;
#else
  #ifdef USE_COLOR_LAYER
    palette_idxToColor(low, &color[0].rgba);
    palette_idxToColor(high, &color[1].rgba);
  #else
    palette_idxToColor(low, &color[0]);
    palette_idxToColor(high, &color[1]);
  #endif
#endif
}

void graphics_setLayer(uint8_t index) {
  current_layer = &layers[index];
}

void graphics_setPattern(uint16_t bstr) {
  pattern = bstr;
}

void graphics_setPalette(void) {
#ifdef USE_PALETTE_IMAGE
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, palette_texture);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 16, 16, GL_RGBA, GL_UNSIGNED_BYTE, (void *) palette_getLookupImage());
#else
  /* attach shader */
  glUseProgram(shaderProgram);

  glUniform4fv(glGetUniformLocation(shaderProgram, "u_palette"),
    256, (const GLfloat *) palette_getPalette());
#endif

  glCheckError("Set palette");
}

void graphics_setClip(int x, int y, int w, int h) {
  clip.x = x;
  clip.y = y;
  clip.w = x+w;
  clip.h = y+h;
}

void graphics_drawPixel(int x, int y) {
  /* check clipping rect */
  // FIXME: this function is confusing, backwards probably
  uint8_t i = 0xF - ((x & 0x3) + 0x4 * (y & 0x3));
  uint8_t b = (pattern & (1 << i)) >> i;

#ifdef USE_INDEXED_LAYER
  uint8_t c = (b == 1) ? color_index[0] : color_index[1];
#else
  #ifdef USE_COLOR_LAYER
    color_t c = (b == 1) ? color[0] : color[1];
  #else
    uint32_t c = (b == 1) ? color[0] : color[1];
  #endif
#endif
  poke(current_layer->buffer, x, y, c);
}

void graphics_setBlend(uint8_t index, uint8_t mode) {
  printf("BlendMode (%d) set to (%d)\n", index, mode);
  blendTable[index] = mode;
}


void graphics_cls(uint16_t index) {
#ifdef USE_COLOR_LAYER
  const size_t size = GRAPHICS_WIDTH * GRAPHICS_HEIGHT * sizeof(color_t);
#else
  const size_t size = GRAPHICS_WIDTH * GRAPHICS_HEIGHT * sizeof(uint32_t);
#endif

  memset(current_layer->buffer, 0x00000000, size);
}

// FIXME: we should check cliping here.
void graphics_circ(int x, int y, int r) {
  int dx = r;
  int dy = 0;
  int err = 1 - dx;
  while (dx >= dy) {
    graphics_drawPixel( dx + x,   dy + y);
    graphics_drawPixel(-dx + x,   dy + y);
    graphics_drawPixel( dx + x,  -dy + y);
    graphics_drawPixel(-dx + x,  -dy + y);
    graphics_drawPixel( dy + x,   dx + y);
    graphics_drawPixel(-dy + x,   dx + y);
    graphics_drawPixel( dy + x,  -dx + y);
    graphics_drawPixel(-dy + x,  -dx + y);
    dy++;
    if (err < 0) {
      err += 2 * dy + 1;
    } else {
      dx--;
      err += 2 * (dy - dx + 1);
    }
  }
}

// FIXME: we should check cliping here.
void graphics_circfill(int x, int y, int r) {
  int dx = r;
  int dy = 0;
  int err = 1 - dx;
  while(dx >= dy) {
    #define FILL_ROW(startx, endx, starty)\
      do {\
        int sx = (startx);\
        int ex = (endx);\
        int sy = (starty);\
        if (sy < 0 || sy >= GRAPHICS_HEIGHT) break;\
        if (sx < 0) sx = 0;\
        if (sx > GRAPHICS_WIDTH) sx = GRAPHICS_WIDTH;\
        if (ex < 0) ex = 0;\
        if (ex > GRAPHICS_WIDTH) ex = GRAPHICS_WIDTH;\
        if (sx == ex) break;\
        for (int i = 0; i < (ex - sx); i++) {\
        graphics_drawPixel(sx+i, sy);\
        }\
      } while (0)

    FILL_ROW( -dx + x,  dx + x,   dy + y );
    FILL_ROW( -dx + x,  dx + x,  -dy + y );
    FILL_ROW( -dy + x,  dy + x,   dx + y );
    FILL_ROW( -dy + x,  dy + x,  -dx + y );
    #undef FILL_ROW

    dy++;
    if (err < 0) {
      err += 2 * dy + 1;
    } else {
      dx--;
      err += 2 * (dy - dx + 1);
    }
  }
}

// FIXME: we should check cliping here.
void graphics_line(int x1, int y1, int x2, int y2) {
  #define SWAP_INT(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))
  int steep = abs(y2 - y1) > abs(x2 - x1);
  if (steep) {
    SWAP_INT(x1, y1);
    SWAP_INT(x2, y2);
  }
  if (x1 > x2) {
    SWAP_INT(x1, x2);
    SWAP_INT(y1, y2);
  }
  #undef SWAP_INT
  int dx = x2 - x1;
  int dy = abs(y2 - y1);
  int err = dx / 2;
  int ystep = (y1 < y2) ? 1 : -1;
  int x, y = y1;
  for (x = x1; x < x2; x++) {
    if (steep) {
      graphics_drawPixel(y, x);
    } else {
      graphics_drawPixel(x, y);
    }
    err -= dy;
    if (err < 0) {
      y += ystep;
      err += dx;
    }
  }
}

void graphics_point(int x, int y) {
  graphics_drawPixel(x, y);
}

// FIXME: we should check cliping here.
void graphics_rect(int x, int y, int w, int h) {
  for (int i = 0; i < w; i++) {
    graphics_drawPixel(x+i, y);
    graphics_drawPixel(x+i, y+h-1);
  }

  for (int j = y + 1; j < (y + h-1); j++) {
    graphics_drawPixel(x+0, j);
    graphics_drawPixel(x+w-1, j);
  }
}

// FIXME: we should check cliping here.
void graphics_rectfill(int x, int y, int w, int h) {
  for (int j = y; j < (y + h); j++) {
    for (int i = 0; i < w; i++) {
      graphics_drawPixel(x+i, j);
    }
  }
}

// FIXME: we should check cliping here.
void graphics_blit(image_t *image, int dx, int dy, int sx, int sy, int sw, int sh) {
  int srci = sx + sy * image->width;
  int dsti = dx + dy * GRAPHICS_WIDTH;
  for (int y = 0; y < sh; y++) {
    memcpy(current_layer->buffer + dsti, image->indices + srci, sw);
    srci += image->width;
    dsti += GRAPHICS_WIDTH;
  }
}

void graphics_blit_test(uint8_t *data, int x, int y, int w, int h) {
  for (int i = 0; i < w * h; i++) {
    if (*(data + i) > 0x0) {
      int _x = x + (i % w);
      int _y = y + (i / w);
      poke(current_layer->buffer, _x, _y, color_index[0]);
    }
  }
}
