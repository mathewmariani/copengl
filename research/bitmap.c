/* std */
#include <stdio.h>
#include <stdlib.h>

/* engine */
#include "bitmap.h"
#include "palette.h"

/* libs */
#include "lib/stb/stb_image.h"

static inline u8 peek(bitmap_t *bitmap, i16 x, i16 y) {
  return *(bitmap->pixels + x + y * bitmap->width);
}

static inline void poke(bitmap_t *bitmap, i16 x, i16 y, u8 c) {
  *(bitmap->pixels + x + y * bitmap->width) = c;
}

static inline u32 convert_to_rgba(u32 abgr) {
  return ((abgr & 0xFF000000) >> 24) | // ______AA
         ((abgr & 0x00FF0000) >>  8) | // ____RR__
         ((abgr & 0x0000FF00) <<  8) | // __GG____
         ((abgr & 0x000000FF) << 24);  // BB______
}

void bitmap_init(bitmap_t *bitmap, const palette_t *palette, const data_t *data) {
  /* load image data */
  i16 width, height;
  u32 *imgdata = (u32 *) stbi_load_from_memory(
    (const stbi_uc *) data->data, data->size, &width, &height, NULL, STBI_rgb_alpha
  );

  if (!imgdata) {
    printf("%s\n", stbi_failure_reason());
    return;
  }

  /* initialize bitmap */
  const size_t nitems = width * height;
  *bitmap = (bitmap_t) {
    .width = width,
    .height = height,
    .pixels = malloc(nitems * sizeof(u8)),
  };

  /* convert colors to indicies */
  for (int i = 0; i < (width * height); i++) {
    u32 rgba = convert_to_rgba(*(imgdata + i));
    u8 idx = palette_find(palette, rgba);
    
    if (idx < 0) {
      printf("this isnt an indexed color\n");
    }

    *(bitmap->pixels + i) = idx;
  }

  /* free resources */
  stbi_image_free(imgdata);
}

void bitmap_destroy(bitmap_t *bitmap) {
  free(bitmap->pixels);
  free(bitmap);
}

uint8_t bitmap_get(bitmap_t *bitmap, i16 x, i16 y) {
  return peek(bitmap, x, y);
}

void bitmap_set(bitmap_t *bitmap, i16 x, i16 y, u8 c) {
  return poke(bitmap, x, y, c);
}

buffer_t *image_create(const palette_t *palette, const data_t *data) {
  /* load image data */
  int width, height;
  u32 *image_data = (u32 *) stbi_load_from_memory(
    (const stbi_uc *) data->data, data->size, &width, &height, NULL, STBI_rgb_alpha
  );

  if (!image_data) {
    printf("%s\n", stbi_failure_reason());
    return NULL;
  }

  /* initialize bitmap */
  const size_t size = width * height;
  buffer_t *buffer = malloc(sizeof(buffer_t));
  buffer = &(buffer_t) {
    .width = width,
    .height = height,
    .data = malloc(size * sizeof(uint8_t)),
  };

  /* convert colors to indicies */
  for (int i = 0; i < size; i++) {
    u32 rgba = convert_to_rgba(*(image_data + i));
    u8 idx = palette_find(palette, rgba);
    
    if (idx < 0) {
      printf("this isnt an indexed color\n");
    }

    *(buffer->data + i) = idx;
  }

  /* free resources */
  stbi_image_free(image_data);

  return buffer;
}
