/* stdlib */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* engine */
#include "app.h"
#include "graphics.h"
#include "palette.h"
#include "font.h"
#include "runtime.h"

#define self ((graphics_t *) ((app_t *) wrenGetUserData(vm))->graphics)

void w_graphics_getWidth(WrenVM *vm) {
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, 400);
}

void w_graphics_getHeight(WrenVM *vm) {
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, 300);
}

void w_graphics_setBackground(WrenVM *vm) {
  uint16_t index = (uint16_t) wrenGetSlotDouble(vm, 1);
  graphics_setBackground(index);  
}

void w_graphics_setColor(WrenVM *vm) {
  uint16_t index = (uint16_t) wrenGetSlotDouble(vm, 1);
  graphics_setColor(index);
}

void w_graphics_setLayer(WrenVM *vm) {
  uint8_t index = (uint8_t) wrenGetSlotDouble(vm, 1);
  graphics_setLayer(index);
}

void w_graphics_setPalette(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  palette_init(filename);

  graphics_setPalette();
}

void w_graphics_setPattern(WrenVM *vm) {
  uint16_t pattern = (uint16_t) wrenGetSlotDouble(vm, 1);
  graphics_setPattern(pattern);
}

void w_graphics_setClip(WrenVM *vm) {
  int x, y, w, h;

  int argc = wrenGetSlotCount(vm);
  if (argc > 1) {
    x = (int) wrenGetSlotDouble(vm, 1);
    y = (int) wrenGetSlotDouble(vm, 2);
    w = (int) wrenGetSlotDouble(vm, 3);
    h = (int) wrenGetSlotDouble(vm, 4);
  } else {
    x = 0;
    y = 0;
    w = GRAPHICS_WIDTH;
    h = GRAPHICS_HEIGHT;
  }

  graphics_setClip(x, y, w, h);
}

void w_graphics_blend(WrenVM *vm) {
  uint8_t index = (uint8_t) wrenGetSlotDouble(vm, 1);
  const char *mode = wrenGetSlotString(vm, 2);

  int _mode = 0;
  if (strcmp(mode, "none") == 0) {
    _mode = 0;
  } else if (strcmp(mode, "add") == 0) {
    _mode = 1;
  } else if (strcmp(mode, "subt") == 0) {
    _mode = 2;
  } else if (strcmp(mode, "avg") == 0) {
    _mode = 3;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown BlendMode");
    wrenAbortFiber(vm, 0);
  }

  graphics_setBlend(index, _mode);
}

void w_graphics_clear(WrenVM *vm) {
  uint16_t idx = wrenGetSlotDouble(vm, 1);
  graphics_cls(idx);
}

void w_graphics_draw(WrenVM *vm) {
  image_t *image = (image_t *) wrenGetSlotForeign(vm, 1);
  int x = (int) wrenGetSlotDouble(vm, 2);
  int y = (int) wrenGetSlotDouble(vm, 3);
  int w = image->width;
  int h = image->height;

  graphics_blit(image, x, y, 0, 0, w, h);
}

void w_graphics_drawq(WrenVM *vm) {
  image_t *image = (image_t *) wrenGetSlotForeign(vm, 1);
  rect_t *quad = (rect_t *) wrenGetSlotForeign(vm, 2);
  int x = (int) wrenGetSlotDouble(vm, 3);
  int y = (int) wrenGetSlotDouble(vm, 4);
  int qx = quad->x;
  int qy = quad->y;
  int qw = quad->w;
  int qh = quad->h;

  graphics_blit(image, x, y, qx, qy, qw, qh);
}

void w_graphics_print(WrenVM *vm) {
  ttf_t *ttf = (ttf_t *) wrenGetSlotForeign(vm, 1);
  const char *str = wrenGetSlotString(vm, 2);
  int x = (int) wrenGetSlotDouble(vm, 3);
  int y = (int) wrenGetSlotDouble(vm, 4);

  int w, h;
  uint8_t *bitmap = ttf_render(ttf, str, &w, &h);

  // FIXME: I should have access to the color state...
  for (int i = 0; i < w * h; i++) {
    *(bitmap + i) = *(bitmap + i) > 0 ? 0x1 : 0x0;
  }

  graphics_blit_test(self, bitmap, x, y, w, h);

  /* free resources */
  free(bitmap);
}

void w_graphics_present() {
  printf("we made it this far!\n");
  graphics_present();
}

void w_graphics_line(WrenVM *vm) {
  int x1 = wrenGetSlotDouble(vm, 1);
  int y1 = wrenGetSlotDouble(vm, 2);
  int x2 = wrenGetSlotDouble(vm, 3);
  int y2 = wrenGetSlotDouble(vm, 4);

  graphics_line(x1, y1, x2, y2);
}

void w_graphics_point(WrenVM *vm) {
  int x = wrenGetSlotDouble(vm, 1);
  int y = wrenGetSlotDouble(vm, 2);

  graphics_point(x, y);
}

void w_graphics_rectangle(WrenVM *vm) {
  const char *mode = wrenGetSlotString(vm, 1);
  int x = wrenGetSlotDouble(vm, 2);
  int y = wrenGetSlotDouble(vm, 3);
  int w = wrenGetSlotDouble(vm, 4);
  int h = wrenGetSlotDouble(vm, 5);

  int fill = 0;
  if (!strcmp(mode, "fill")) {
    fill = 1;
  } else if (!strcmp(mode, "line")) {
    fill = 0;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
  }

  if (fill) {
    graphics_rectfill(x, y, w, h);  
  } else {
    graphics_rect(x, y, w, h);
  }
}

void w_graphics_circle(WrenVM *vm) {
  const char *mode = wrenGetSlotString(vm, 1);
  int x = wrenGetSlotDouble(vm, 2);
  int y = wrenGetSlotDouble(vm, 3);
  int r = wrenGetSlotDouble(vm, 4);

  int fill = 0;
  if (!strcmp(mode, "fill")) {
    fill = 1;
  } else if (!strcmp(mode, "line")) {
    fill = 0;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
  }

  if (fill) {
    graphics_circfill(x, y, r);
  } else {
    graphics_circ(x, y, r);
  }
}

const static function_desc functions[] = {
  /* properties */
  { true, "width", w_graphics_getWidth },
  { true, "height", w_graphics_getHeight },

  /* state */
  { true, "background=(_)", w_graphics_setBackground },
  { true, "color=(_)", w_graphics_setColor },
  { true, "layer=(_)", w_graphics_setLayer },
  { true, "palette=(_)", w_graphics_setPalette },
  { true, "pattern=(_)", w_graphics_setPattern },
  { true, "clip()", w_graphics_setClip },
  { true, "clip(_,_,_,_)", w_graphics_setClip },

  /* drawing */
  { true, "blend(_,_)", w_graphics_blend },
  { true, "clear(_)", w_graphics_clear },
  { true, "draw(_,_,_)", w_graphics_draw },
  { true, "draw(_,_,_,_)", w_graphics_drawq },
  { true, "print(_,_,_,_)", w_graphics_print },
  { true, "present()", w_graphics_present },

  /* primitives */
  { true, "circle(_,_,_,_)", w_graphics_circle },
  { true, "line(_,_,_,_)", w_graphics_line },
  { true, "point(_,_)", w_graphics_point },
  { true, "rectangle(_,_,_,_,_)", w_graphics_rectangle },

  /* always last. */
  { false, NULL, NULL },
};

void wren_graphics(WrenVM *vm) {
  /* initialize module */
  app_t *app = (app_t *) wrenGetUserData(vm);
  app->graphics = graphics_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Graphics",
    .type = CLASS_GRAPHICS,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
