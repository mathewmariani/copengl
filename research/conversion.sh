#!/bin/bash

echo "convert .wren to wren.inc..."
for f in `find src -name "*.wren"`; do
  python3 ./util/wren_to_c_string.py "${f}.inc" $f
done

# echo "convert .wren using xxd..."
for f in `find src -name "*.wren"`; do
  xxd -i $f > "${f}.h"
done

echo "convert shaders to shader.inc..."
for f in `find src -name "*.vs" -or -name "*.fs"`; do
  python3 ./util/wren_to_c_string.py "${f}.inc" $f
done

# echo "convert shaders using xxd..."
for f in `find src -name "*.vs" -or -name "*.fs"`; do
  xxd -i $f > "${f}.h"
  # xxd -i $f | sed 's/\([0-9a-f]\)$/\0, 0x00/' > "${f}.h"
done