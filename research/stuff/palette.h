#ifndef PALETTE_H
#define PALETTE_H

/* engine */
#include "platform.h"

typedef union {
  struct {
    f32 r, g, b, a;
  };
  f32 rgba;
} color_t;

typedef struct {
  u8 begin;
  u8 end;
  u8 it;
} range_t;

typedef struct {
  color_t colors[256];
  range_t range;
} palette_t;

void palette_init(palette_t *palette);
void palette_add(palette_t *palette, u32 color);
u32 palette_get(u8 index);
u8 palette_find(const palette_t *palette, u32 color);


#endif
