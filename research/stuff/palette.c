/* std */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* engine */
#include "palette.h"

typedef struct {
  u32 rgba;
  color_t *color;
} color_entry_t;

color_entry_t color_map[256];

void palette_init(palette_t *palette) {
  memset(palette, 0, sizeof(palette_t));
  palette->range.begin = 1;
  palette->range.end = 0xFF;
  palette->range.it = 1;
}

void palette_add(palette_t *palette, u32 color) {
  range_t *r = &palette->range;
  if (r->it == r->end) {
    return;
  }

  printf("Added color (0x%08X) at index %d.\n", color, r->it);

  palette->colors[r->it] = (color_t) {
    .r = ((color >> 24) & 0xFF) / 255.0f,
    .g = ((color >> 16) & 0xFF) / 255.0f,
    .b = ((color >>  8) & 0xFF) / 255.0f,
    .a = ((color >>  0) & 0xFF) / 255.0f,
  };

  color_map[r->it] = (color_entry_t) {
    .rgba = color,
    .color = &(palette->colors[r->it]),
  };

  r->it++;  
}

u32 palette_get(u8 index) {
  return color_map[index].rgba;
}

u8 palette_find(const palette_t *palette, u32 rgba) {
  const range_t *r = &palette->range;
  const color_entry_t *it = &color_map[r->begin];
  for (int i = r->begin; i != r->it; ++i, ++it) {
    if (it->rgba == rgba) {
      return i;
    }
  }
  return -1;
}
