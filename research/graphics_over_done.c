/* std */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* engine */
#include "common/platform.h"
#include "common/config.h"
#include "graphics.h"

/* opengl */
// #define OPENGL_DEBUG
#define OPENGL_USE_GLAD
#define OPENGL_USE_SDL2
#define OPENGL_IMPLEMENTATION
#include "system/opengl.h"

/* embedded */
#include "embedded/vertex.vs.h"
#include "embedded/fragment.fs.h"

/* initialize opengl functions */
typedef struct {
  u8 background;
  clip_t clip;
  struct {
    u8 low;
    u8 high;
  } color;
  u16 pattern;
} drawstate_t;

/* graphics module */
struct graphics_t {
  /* video memory */
  struct {
    u8 *data;
    u8 mapping[256];
  } vram;

  /* current draw state */
  drawstate_t draw_state;

  /* opengl resources */
  struct {
    u32 vbo;
	u32 ebo;
	u32 shader;
	u32 texture;
	u32 pal;
  } opengl;
};

#define POKE(dst, src, value) \
  (*(dst + src) = value)

static inline u8 get_pattern(const graphics_t *graphics, const i32 x, const i32 y) {
  const u8 i = 0xF - ((x & 0x3) + 0x4 * (y & 0x3));
  return (((graphics->draw_state.pattern & (1 << i)) >> i) == 0) ?
    graphics->draw_state.color.low :
    graphics->draw_state.color.high;
}

static inline void poke(const void *dst, const i32 offset, const u8 c) {
  *((u8 *)dst + offset) = c;
}

static inline u8 mapped_color(const graphics_t *graphics, u8 c) {
  return *(graphics->vram.mapping + c);
}

typedef struct { i32 x, y; u8 c; } pixel_t;
static inline void set_pixel(graphics_t *graphics, const pixel_t p) {
  const u32 x = p.x;
  const u32 y = p.y;

  if (x <  graphics->draw_state.clip.x1 ||
      y <  graphics->draw_state.clip.y1 ||
      x >= graphics->draw_state.clip.x2 ||
      y >= graphics->draw_state.clip.y2) {
    return;
  }
  
  const u8 c = p.c ? p.c : get_pattern(graphics, x, y);
  poke(graphics->vram.data, x + y * LITE_WIDTH, mapped_color(graphics, c));
}

static void draw_hline(graphics_t *graphics, i32 x, i32 y, i32 w) {
  for (i16 i = x; i < (x + w); ++i) {
    set_pixel(graphics, (pixel_t) { .x = i, .y = y });
  }
}

static void draw_vline(graphics_t *graphics, i32 x, i32 y, i32 h) {
  for (i16 j = y; j < (y + h); ++j) {
    set_pixel(graphics, (pixel_t) { .x = x, .y = j });
  }
}

static void draw_rect(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  for (i16 j = y; j < (y + h); ++j) {
    for (i16 i = x; i < (x + w); ++i) {
      set_pixel(graphics, (pixel_t) { .x = i, .y = j });
    }
  }
}

graphics_t *graphics_create(void) {
  graphics_t *graphics = LITE_MALLOC(sizeof(graphics_t));
  *graphics = (graphics_t) {
    /* video ram */
    .vram = {
      .data = NULL,
      .mapping = 0,
    },

    /* draw state */
    .draw_state = (drawstate_t) {
      .background = 0x00,
      .clip = (clip_t) {
        .x1 = 0,
        .y1 = 0,
        .x2 = LITE_WIDTH,
        .y2 = LITE_HEIGHT,
      },
      .color = 0x00,
      .pattern = 0b0000000000000000,
    },

    /* opengl resources */
    .opengl = {
      .vbo = 0,
      .ebo = 0,
	  .texture = 0,
      .shader = 0,
    },
  };

  return graphics;
}

void graphics_destroy(graphics_t *graphics) {
  /* opengl resources */
  opengl_delete_buffer(1, &graphics->opengl.vbo);
  opengl_delete_buffer(1, &graphics->opengl.ebo);
  opengl_delete_shader(graphics->opengl.shader);

  /* cleanup context */
  opengl_shutdown();

  free(graphics);
}

void graphics_set_mode(graphics_t *graphics) {
  /* screen buffer */
  graphics->vram.data = LITE_CALLOC(LITE_HEIGHT * LITE_WIDTH, sizeof(u8));
  if (graphics->vram.data == NULL) {
    printf("\n===\nHey I think there was an error here...\n===\n");
  }

  /* initialize palette mapping */
  for (i32 i = 0; i < MAX_COLORS; ++i) {
    *(graphics->vram.mapping + i) = i;
  }

  /* initialize opengl */
  opengl_setup();

  /* opengl information */
  printf("OpenGL %s, GLSL %s\n",
    glGetString(GL_VERSION),
    glGetString(GL_SHADING_LANGUAGE_VERSION)
  );

  /* opengl state */

  /* viewport */
  opengl_apply_viewport(0, 0, LITE_WIDTH, LITE_HEIGHT);

  /* set up element data */
  const u8 indices[] = {
    0, 1, 3,
    1, 2, 3,
  };

  /* set up vertex data */
  const f32 vertices[] = {
    /* vertices */       /* texture coords */
    1.0f,  1.0f, 0.0f,   1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,   1.0f, 1.0f,
   -1.0f, -1.0f, 0.0f,   0.0f, 1.0f,
   -1.0f,  1.0f, 0.0f,   0.0f, 0.0f 
  };

  /* create opengl resources  */
  u32 vbo = opengl_create_buffer(&(opengl_buffer_desc) {
    .target = opengl_buffertarget_array,
    .usage = opengl_usage_static,
    .udata = (opengl_userdata) {
      .data = vertices,
      .size = sizeof(vertices),
    },
  });

  u32 ebo = opengl_create_buffer(&(opengl_buffer_desc) {
    .target = opengl_buffertarget_element,
    .usage = opengl_usage_static,
    .udata = (opengl_userdata) {
      .data = indices,
      .size = sizeof(indices),
    },
  });

  u32 shader = opengl_create_shader(&(opengl_shader_desc) {
    .vs_source = (const char *) vertex_vs,
    .fs_source = (const char *) fragment_fs,
  });

  /* pipeline settings */
  opengl_apply_pipeline(&(opengl_pipeline_desc) {
    .blend_mode = false,
    .cull_mode = opengl_cull_mode_back,
    .depth_test = opengl_depth_test_none,
    .winding = opengl_winding_cw,
  });

  /* position attribute */
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(f32), (void *) 0);
  glEnableVertexAttribArray(0);

  /* texcoord attribute */
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(f32), (void *) (3 * sizeof(f32)));
  glEnableVertexAttribArray(1);

#ifdef OPENGL_DEBUG
  gl_has_error("attribute pointers");
#endif

  /* screen texture */
  u32 image = opengl_create_texture(&(opengl_texture_desc) {
    .width = LITE_WIDTH,
    .height = LITE_HEIGHT,
    .target = opengl_texturetarget_2d,
    .internal = opengl_pixelformat_r8,
    .filter = opengl_filtertype_nearest,
    .wrap = opengl_wraptype_clamp,
  });

  /* cache */
  graphics->opengl.vbo = vbo;
  graphics->opengl.ebo = ebo;
  graphics->opengl.shader = shader;
  graphics->opengl.texture = image;

  /* bind texture to texture unit */
  opengl_bind_texture_to_unit(image, 0);

  /* attach shader */
  glUseProgram(shader);

  /* FIXME: not sure why texture is `0` in this case */
  //GLint location = glGetUniformLocation(shader, "image");
  //glUniform1i(location, 0);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

i32 graphics_get_width(const graphics_t *graphics) {
  return LITE_WIDTH;
}

i32 graphics_get_height(const graphics_t *graphics) {
  return LITE_HEIGHT;
}

u8 graphics_get_background(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->background;
}

void graphics_set_background(graphics_t *graphics, u8 color) {
  drawstate_t *state = &graphics->draw_state;
  state->background = color;
}

clip_t *graphics_get_clipping(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return &state->clip;
}

void graphics_set_clipping(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 y2) {
  drawstate_t *state = &graphics->draw_state;
  state->clip = (clip_t) {
    .x1 = x1,
    .y1 = y1,
    .x2 = x2,
    .y2 = y2,
  };
}

u8 graphics_get_color(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->color.low;
}

void graphics_set_color(graphics_t *graphics, const u8 c0, const u8 c1) {
  drawstate_t *state = &graphics->draw_state;
  state->color.low = c0;
  state->color.high = c1;
}

u16 graphics_get_pattern(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->pattern;
}

void graphics_set_pattern(graphics_t *graphics, const u16 bstr) {
  drawstate_t *state = &graphics->draw_state;
  state->pattern = bstr;
}

static void draw_ellipse(graphics_t *graphics, i32 x0, i32 y0, i32 a, i32 b) {
  if ((a <= 0) || (b <= 0)) {
   return;
  }

  i64 aa2 = a * a * 2;
  i64 bb2 = b * b * 2;

  {
   i64 x = a, y = 0;
   i64 dx = (1 - 2 * a)*b*b, dy = a * a;
   i64 sx = bb2 * a, sy = 0;
   i64 e = 0;

   while (sx >= sy) {
     graphics_set_pixel(graphics, x0 + x, y0 + y); /*   I. Quadrant */
     graphics_set_pixel(graphics, x0 + x, y0 - y); /*  II. Quadrant */
     graphics_set_pixel(graphics, x0 - x, y0 + y); /* III. Quadrant */
     graphics_set_pixel(graphics, x0 - x, y0 - y); /*  IV. Quadrant */

     y++; sy += aa2; e += dy; dy += aa2;
     if (2 * e + dx >0) {
       x--; sx -= bb2; e += dx; dx += bb2;
     }
   }
  }

  {
   i64 x = 0, y = b;
   i64 dx = b * b, dy = (1 - 2 * b)*a*a;
   i64 sx = 0, sy = aa2 * b;
   i64 e = 0;

   while (sy >= sx) {
     graphics_set_pixel(graphics, x0 + x, y0 + y); /*   I. Quadrant */
     graphics_set_pixel(graphics, x0 + x, y0 - y); /*  II. Quadrant */
     graphics_set_pixel(graphics, x0 - x, y0 + y); /* III. Quadrant */
     graphics_set_pixel(graphics, x0 - x, y0 - y); /*  IV. Quadrant */

     x++; sx += bb2; e += dx; dx += bb2;
     if (2 * e + dy >0) {
       y--; sy -= aa2; e += dy; dy += aa2;
     }
   }
  }
}

static void draw_ellipse_filled(graphics_t *graphics, i32 x0, i32 y0, i32 a, i32 b) {
 #define FILL_ROW(startx, endx, starty)\
   do {\
     i16 sx = (startx);\
     i16 ex = (endx);\
     i16 sy = (starty);\
     if (sy < 0 || sy >= LITE_HEIGHT) break;\
     if (sx < 0) sx = 0;\
     if (sx > LITE_WIDTH) sx = LITE_WIDTH;\
     if (ex < 0) ex = 0;\
     if (ex > LITE_WIDTH) ex = LITE_WIDTH;\
     if (sx == ex) break;\
     for (i16 i = 0; i < (ex - sx); i++) {\
       graphics_set_pixel(graphics, sx+i, sy);\
     }\
   } while (0)

 if ((a <= 0) || (b <= 0)) {
   return;
 }

 i64 aa2 = a * a * 2;
 i64 bb2 = b * b * 2;

 {
   i64 x = a, y = 0;
   i64 dx = (1 - 2 * a)*b*b, dy = a * a;
   i64 sx = bb2 * a, sy = 0;
   i64 e = 0;

   while (sx >= sy) {
     FILL_ROW((x0 - x), (x0 + x), (y0 + y));
     FILL_ROW((x0 - x), (x0 + x), (y0 - y));

     y++; sy += aa2; e += dy; dy += aa2;
     if (2 * e + dx >0) {
       x--; sx -= bb2; e += dx; dx += bb2;
     }
   }
 }

 {
   i64 x = 0, y = b;
   i64 dx = b * b, dy = (1 - 2 * b)*a*a;
   i64 sx = 0, sy = aa2 * b;
   i64 e = 0;

   while (sy >= sx) {
     FILL_ROW((x0 - x), (x0 + x), (y0 + y));
     FILL_ROW((x0 - x), (x0 + x), (y0 - y));

     x++; sx += bb2; e += dx; dx += bb2;
     if (2 * e + dy >0) {
       y--; sy -= aa2; e += dy; dy += aa2;
     }
   }
 }

#undef FILL_ROW
}

void graphics_circ(graphics_t *graphics, i32 x, i32 y, i32 r) {
  draw_ellipse(graphics, x, y, r, r);
}

void graphics_circfill(graphics_t *graphics, i32 x, i32 y, i32 r) {
  draw_ellipse_filled(graphics, x, y, r, r);
}

void graphics_clear(graphics_t *graphics) {
  const size_t size = (LITE_WIDTH * LITE_HEIGHT);
  const u8 c = graphics->draw_state.background;
  for (i32 i = 0; i < size; ++i) {
    *(graphics->vram.data + i) = mapped_color(graphics, c);
  }
}

void graphics_ellipse(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b) {
  draw_ellipse(graphics, x, y, a, b);
}

void graphics_ellipsefill(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b) {
  draw_ellipse_filled(graphics, x, y, a, b);
}

void graphics_line(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 y2) {
 #define SWAP_INT(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))
 i16 steep = abs(y2 - y1) > abs(x2 - x1);
 if (steep) {
   SWAP_INT(x1, y1);
   SWAP_INT(x2, y2);
 }
 if (x1 > x2) {
   SWAP_INT(x1, x2);
   SWAP_INT(y1, y2);
 }
 #undef SWAP_INT
 const i16 dx = x2 - x1;
 const i16 dy = abs(y2 - y1);
 const i16 ystep = (y1 < y2) ? 1 : -1;
 i16 err = dx / 2;
 i16 x, y = y1;
 for (x = x1; x < x2; x++) {
   if (steep) {
     graphics_set_pixel(graphics, x, y);
   } else {
     graphics_set_pixel(graphics, x, y);
   }
   err -= dy;
   if (err < 0) {
     y += ystep;
     err += dx;
   }
 }
}

void graphics_pal(graphics_t *graphics, u8 c0, u8 c1, bool reset) {
  if (reset) {
    for (i32 i = 0; i < MAX_COLORS; ++i) {
      *(graphics->vram.mapping + i) = i;
    }
    return;
  }
  *(graphics->vram.mapping + c0) = c1;
}

void graphics_points(graphics_t *graphics, i32 *positions, int count) {
  for (i32 i = 0; i < count; i += 2) {
    set_pixel(graphics, (pixel_t) {
      .x = *(positions + i + 0),
      .y = *(positions + i + 1),
    });
  }
}

void graphics_present(const graphics_t *graphics) {
  /* clear to transparent black */
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  /* copy buffer to gpu */
  opengl_update_texture(graphics->opengl.texture, &(opengl_userdata) {
    .data = graphics->vram.data,
    .size = sizeof(graphics->vram.data),
  });
  
  /* draw fullscreen quad */
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

void graphics_rect(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  draw_hline(graphics, x, y, w);
  draw_hline(graphics, x, y + h - 1, w);
  draw_vline(graphics, x, y, h);
  draw_vline(graphics, x + w - 1, y, h);
}

void graphics_rectfill(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  draw_rect(graphics, x, y, w, h);
}

void graphics_set_pixel(graphics_t *graphics, i32 x, i32 y) {
  set_pixel(graphics, (pixel_t) {
    .x = x,
    .y = y
  });
}
