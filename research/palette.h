#ifndef PALETTE_H
#define PALETTE_H

/* engine */
#include "platform.h"

// typedef union {
//   struct { float r, g, b, a; };
//   float rgba[4];
// } fcolor_t;

typedef struct {
  float r;
  float b;
  float g;
  float a;
} fcolor_t;

typedef union {
  struct {
    float r, g, b, a;
  };
  float rgba[4];
} color_t;

void palette_init(palette_t *palette);
void palette_add(palette_t *palette, color_t color);

// void palette_init(const char *filename);
// void palette_deinit(void);

// int palette_idxToColor(uint8_t idx, uint32_t *rgb);
// uint8_t palette_colorToIdx(uint32_t color);

// fcolor_t *palette_getPalette();
// uint32_t *palette_getLookupImage();

#endif
