/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>

#define MIN(a, b)           ((b) < (a) ? (b) : (a))
#define MAX(a, b)           ((b) > (a) ? (b) : (a))

#define BLEND_SUBT(dst, src)\
    if ((dst & 0x00FFFFFF) != 0x0) {\
    r = MAX(((dst >> 0) & 0xFF) - ((src >> 0) & 0xFF), 0x00);\
    g = MAX(((dst >> 8) & 0xFF) - ((src >> 8) & 0xFF), 0x00);\
    b = MAX(((dst >> 16) & 0xFF) - ((src >> 16) & 0xFF), 0x00);\
    a = (dst >> 24) & 0xFF;\
    dst = (a << 24) | (b << 16) | (g << 8) | (r);\
    }
    
int main()
{
    printf("0x%d\n\n", (0xFF00FFFF & 0x00FFFFFF) != 0x0);
    //             0xAABBGGRR
    //     yellow: 0xFF00FFFF
    uint32_t dst = 0xFF000000;
    uint32_t src = 0xFF0000FF;
    
    //
    // (dst) |    0xFF00FFFF  |    0xFF000000  |    0xFF00FFFF  |    0xFF000000  |
    // (src) |  - 0xFF00FFFF  |  - 0xFF00FFFF  |  - 0xFF000000  |  - 0xFF000000  |
    //       |  ------------  |  ------------  |  ------------  |  ------------  |
    // (res) |    0xFF000000  |    0xFF000000  |    0xFF00FFFF  |    0xFF000000  |
    //       |  correct       |  correct       |  correct       |  correct       |
    
    uint8_t r, g, b, a;
    // r = MAX(((dst >> 0) & 0xFF) - ((src >> 0) & 0xFF), 0x00);\
    // g = MAX(((dst >> 8) & 0xFF) - ((src >> 8) & 0xFF), 0x00);\
    // b = MAX(((dst >> 16) & 0xFF) - ((src >> 16) & 0xFF), 0x00);\
    // a = (dst >> 24) & 0xFF;\
    // dst = (a << 24) | (b << 16) | (g << 8) | (r);
    BLEND_SUBT(dst, src);
    // BLEND_SUBT(src, dst);
    // BLEND_SUBT(src, dst);
    
    printf("0x%x\n", dst);
    

    return 0;
}
