#ifndef IMAGE_H
#define IMAGE_H

/* engine */
#include "platform.h"
#include "data.h"

typedef struct {
  int width;
  int height;
  uint8_t *indices;
} image_t;

void image_init(image_t *self, const data_t *data);
void image_deinit(image_t *self);

#endif
