/* stblib */
#include <stdio.h>
#include <stdlib.h>

/* engine */
#include "image.h"
#include "palette.h"

/* libs */
#include "lib/stb/stb_image.h"

void image_init(image_t *self, const data_t *data) {
  /* load image data */
  uint32_t *imgdata = (uint32_t *) stbi_load_from_memory(
    (const stbi_uc *) data->data, data->size, &self->width, &self->height, NULL, 4
  );

  if (!imgdata) {
    printf("%s\n", stbi_failure_reason());
    return;
  }

  /* allocate indices */
  size_t size = self->width * self->height;
  self->indices = (uint8_t *) calloc(size, sizeof(uint8_t));

  /* convert to index table */
  // FIXME: move to `palette.c` to avoid code duplication
  for (int i = 0; i < size; i++) {
    uint32_t color = *(imgdata + i);
    uint8_t idx = palette_colorToIdx(color);
    *(self->indices + i) = idx;
  }

  /* free resources */
  stbi_image_free(imgdata);
}

void image_deinit(image_t *self) {
  free(self->indices);
}
