#ifndef BITMAP_H
#define BITMAP_H

/* engine */
#include "platform.h"
#include "buffer.h"
#include "palette.h"
#include "data.h"

/* NOTE: this is just a buffer */
typedef struct {
  i16 width;
  i16 height;
  u8 *pixels;
} bitmap_t;

void bitmap_init(bitmap_t *bitmap, const palette_t *palette, const data_t *data);
void bitmap_destroy(bitmap_t *bitmap);

u8 bitmap_get(bitmap_t *bitmap, i16 x, i16 y);
void bitmap_set(bitmap_t *bitmap, i16 x, i16 y, u8 c);

buffer_t *image_create(const palette_t *palette, const data_t *data);

#endif
