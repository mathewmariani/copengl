/* FIXME: not the right values */
static struct { char *name; SDL_Scancode code; } key_entries[] = {
  { "unknown", SDL_SCANCODE_UNKNOWN },

  { "return", SDL_SCANCODE_RETURN },
  { "escape", SDL_SCANCODE_ESCAPE },
  { "backspace", SDL_SCANCODE_BACKSPACE },
  { "tab", SDL_SCANCODE_TAB },
  { "space", SDL_SCANCODE_SPACE },
  { "!", SDL_SCANCODE_EXCLAIM },
  { "\"", SDL_SCANCODE_QUOTEDBL },
  { "#", SDL_SCANCODE_HASH },
  { "%", SDL_SCANCODE_PERCENT },
  { "$", SDL_SCANCODE_DOLLAR },
  { "&", SDL_SCANCODE_AMPERSAND },
  { "'", SDL_SCANCODE_QUOTE },
  { "(", SDL_SCANCODE_LEFTPAREN },
  { ")", SDL_SCANCODE_RIGHTPAREN },
  { "*", SDL_SCANCODE_ASTERISK },
  { "+", SDL_SCANCODE_PLUS },
  { ",", SDL_SCANCODE_COMMA },
  { "-", SDL_SCANCODE_MINUS },
  { ".", SDL_SCANCODE_PERIOD },
  { "/", SDL_SCANCODE_SLASH },
  { "0", SDL_SCANCODE_0 },
  { "1", SDL_SCANCODE_1 },
  { "2", SDL_SCANCODE_2 },
  { "3", SDL_SCANCODE_3 },
  { "4", SDL_SCANCODE_4 },
  { "5", SDL_SCANCODE_5 },
  { "6", SDL_SCANCODE_6 },
  { "7", SDL_SCANCODE_7 },
  { "8", SDL_SCANCODE_8 },
  { "9", SDL_SCANCODE_9 },
  { ":", SDL_SCANCODE_COLON },
  { ";", SDL_SCANCODE_SEMICOLON },
  { "<", SDL_SCANCODE_LESS },
  { "=", SDL_SCANCODE_EQUALS },
  { ">", SDL_SCANCODE_GREATER },
  { "?", SDL_SCANCODE_QUESTION },
  { "@", SDL_SCANCODE_AT },

  { "[", SDL_SCANCODE_LEFTBRACKET },
  { "\\", SDL_SCANCODE_BACKSLASH },
  { "]", SDL_SCANCODE_RIGHTBRACKET },
  { "^", SDL_SCANCODE_CARET },
  { "_", SDL_SCANCODE_UNDERSCORE },
  { "`", SDL_SCANCODE_BACKQUOTE },

  { "a", SDL_SCANCODE_A },
  { "b", SDL_SCANCODE_B },
  { "c", SDL_SCANCODE_C },
  { "d", SDL_SCANCODE_D },
  { "e", SDL_SCANCODE_E },
  { "f", SDL_SCANCODE_F },
  { "g", SDL_SCANCODE_G },
  { "h", SDL_SCANCODE_H },
  { "i", SDL_SCANCODE_I },
  { "j", SDL_SCANCODE_J },
  { "k", SDL_SCANCODE_K },
  { "l", SDL_SCANCODE_L },
  { "m", SDL_SCANCODE_M },
  { "n", SDL_SCANCODE_N },
  { "o", SDL_SCANCODE_O },
  { "p", SDL_SCANCODE_P },
  { "q", SDL_SCANCODE_Q },
  { "r", SDL_SCANCODE_R },
  { "s", SDL_SCANCODE_S },
  { "t", SDL_SCANCODE_T },
  { "u", SDL_SCANCODE_U },
  { "v", SDL_SCANCODE_V },
  { "w", SDL_SCANCODE_W },
  { "x", SDL_SCANCODE_X },
  { "y", SDL_SCANCODE_Y },
  { "z", SDL_SCANCODE_Z },

  { "capslock", SDL_SCANCODE_CAPSLOCK },

  { "f1", SDL_SCANCODE_F1 },
  { "f2", SDL_SCANCODE_F2 },
  { "f3", SDL_SCANCODE_F3 },
  { "f4", SDL_SCANCODE_F4 },
  { "f5", SDL_SCANCODE_F5 },
  { "f6", SDL_SCANCODE_F6 },
  { "f7", SDL_SCANCODE_F7 },
  { "f8", SDL_SCANCODE_F8 },
  { "f9", SDL_SCANCODE_F9 },
  { "f10", SDL_SCANCODE_F10 },
  { "f11", SDL_SCANCODE_F11 },
  { "f12", SDL_SCANCODE_F12 },

  { "printscreen", SDL_SCANCODE_PRINTSCREEN },
  { "scrolllock", SDL_SCANCODE_SCROLLLOCK },
  { "pause", SDL_SCANCODE_PAUSE },
  { "insert", SDL_SCANCODE_INSERT },
  { "home", SDL_SCANCODE_HOME },
  { "pageup", SDL_SCANCODE_PAGEUP },
  { "delete", SDL_SCANCODE_DELETE },
  { "end", SDL_SCANCODE_END },
  { "pagedown", SDL_SCANCODE_PAGEDOWN },
  { "right", SDL_SCANCODE_RIGHT },
  { "left", SDL_SCANCODE_LEFT },
  { "down", SDL_SCANCODE_DOWN },
  { "up", SDL_SCANCODE_UP },

  { "numlock", SDL_SCANCODE_NUMLOCKCLEAR },
  { "kp/", SDL_SCANCODE_KP_DIVIDE },
  { "kp*", SDL_SCANCODE_KP_MULTIPLY },
  { "kp-", SDL_SCANCODE_KP_MINUS },
  { "kp+", SDL_SCANCODE_KP_PLUS },
  { "kpenter", SDL_SCANCODE_KP_ENTER },
  { "kp0", SDL_SCANCODE_KP_0 },
  { "kp1", SDL_SCANCODE_KP_1 },
  { "kp2", SDL_SCANCODE_KP_2 },
  { "kp3", SDL_SCANCODE_KP_3 },
  { "kp4", SDL_SCANCODE_KP_4 },
  { "kp5", SDL_SCANCODE_KP_5 },
  { "kp6", SDL_SCANCODE_KP_6 },
  { "kp7", SDL_SCANCODE_KP_7 },
  { "kp8", SDL_SCANCODE_KP_8 },
  { "kp9", SDL_SCANCODE_KP_9 },
  { "kp.", SDL_SCANCODE_KP_PERIOD },
  { "kp,", SDL_SCANCODE_KP_COMMA },
  { "kp=", SDL_SCANCODE_KP_EQUALS },

  { "application", SDL_SCANCODE_APPLICATION },
  { "power", SDL_SCANCODE_POWER },
  { "f13", SDL_SCANCODE_F13 },
  { "f14", SDL_SCANCODE_F14 },
  { "f15", SDL_SCANCODE_F15 },
  { "f16", SDL_SCANCODE_F16 },
  { "f17", SDL_SCANCODE_F17 },
  { "f18", SDL_SCANCODE_F18 },
  { "f19", SDL_SCANCODE_F19 },
  { "f20", SDL_SCANCODE_F20 },
  { "f21", SDL_SCANCODE_F21 },
  { "f22", SDL_SCANCODE_F22 },
  { "f23", SDL_SCANCODE_F23 },
  { "f24", SDL_SCANCODE_F24 },
  { "execute", SDL_SCANCODE_EXECUTE },
  { "help", SDL_SCANCODE_HELP },
  { "menu", SDL_SCANCODE_MENU },
  { "select", SDL_SCANCODE_SELECT },
  { "stop", SDL_SCANCODE_STOP },
  { "again", SDL_SCANCODE_AGAIN },
  { "undo", SDL_SCANCODE_UNDO },
  { "cut", SDL_SCANCODE_CUT },
  { "copy", SDL_SCANCODE_COPY },
  { "paste", SDL_SCANCODE_PASTE },
  { "find", SDL_SCANCODE_FIND },
  { "mute", SDL_SCANCODE_MUTE },
  { "volumeup", SDL_SCANCODE_VOLUMEUP },
  { "volumedown", SDL_SCANCODE_VOLUMEDOWN },

  { "alterase", SDL_SCANCODE_ALTERASE },
  { "sysreq", SDL_SCANCODE_SYSREQ },
  { "cancel", SDL_SCANCODE_CANCEL },
  { "clear", SDL_SCANCODE_CLEAR },
  { "prior", SDL_SCANCODE_PRIOR },
  { "return2", SDL_SCANCODE_RETURN2 },
  { "separator", SDL_SCANCODE_SEPARATOR },
  { "out", SDL_SCANCODE_OUT },
  { "oper", SDL_SCANCODE_OPER },
  { "clearagain", SDL_SCANCODE_CLEARAGAIN },

  { "thsousandsseparator", SDL_SCANCODE_THOUSANDSSEPARATOR },
  { "decimalseparator", SDL_SCANCODE_DECIMALSEPARATOR },
  { "currencyunit", SDL_SCANCODE_CURRENCYUNIT },
  { "currencysubunit", SDL_SCANCODE_CURRENCYSUBUNIT },

  { "lctrl", SDL_SCANCODE_LCTRL },
  { "lshift", SDL_SCANCODE_LSHIFT },
  { "lalt", SDL_SCANCODE_LALT },
  { "lgui", SDL_SCANCODE_LGUI },
  { "rctrl", SDL_SCANCODE_RCTRL },
  { "rshift", SDL_SCANCODE_RSHIFT },
  { "ralt", SDL_SCANCODE_RALT },
  { "rgui", SDL_SCANCODE_RGUI },

  { "mode", SDL_SCANCODE_MODE },

  { "audionext", SDL_SCANCODE_AUDIONEXT },
  { "audioprev", SDL_SCANCODE_AUDIOPREV },
  { "audiostop", SDL_SCANCODE_AUDIOSTOP },
  { "audioplay", SDL_SCANCODE_AUDIOPLAY },
  { "audiomute", SDL_SCANCODE_AUDIOMUTE },
  { "mediaselect", SDL_SCANCODE_MEDIASELECT },
  { "www", SDL_SCANCODE_WWW },
  { "mail", SDL_SCANCODE_MAIL },
  { "calculator", SDL_SCANCODE_CALCULATOR },
  { "computer", SDL_SCANCODE_COMPUTER },
  { "appsearch", SDL_SCANCODE_AC_SEARCH },
  { "apphome", SDL_SCANCODE_AC_HOME },
  { "appback", SDL_SCANCODE_AC_BACK },
  { "appforward", SDL_SCANCODE_AC_FORWARD },
  { "appstop", SDL_SCANCODE_AC_STOP },
  { "apprefresh", SDL_SCANCODE_AC_REFRESH },
  { "appbookmarks", SDL_SCANCODE_AC_BOOKMARKS },

  { "brightnessdown", SDL_SCANCODE_BRIGHTNESSDOWN },
  { "brightnessup", SDL_SCANCODE_BRIGHTNESSUP },
  { "displayswitch", SDL_SCANCODE_DISPLAYSWITCH },
  { "kbdillumtoggle", SDL_SCANCODE_KBDILLUMTOGGLE },
  { "kbdillumdown", SDL_SCANCODE_KBDILLUMDOWN },
  { "kbdillumup", SDL_SCANCODE_KBDILLUMUP },
  { "eject", SDL_SCANCODE_EJECT },
  { "sleep", SDL_SCANCODE_SLEEP },

  /* always last. */
  { 0, 0 },
};