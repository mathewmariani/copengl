#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "image.h"

#define GFX_WIDTH   480
#define GFX_HEIGHT  270

/* image */

// typedef struct {
//   int width;
//   int height;
//   uint8_t *indices;
// } image_t;

// void image_init(image_t *self, const char *filename);
// void image_deinit(image_t *self);

/* quad */

typedef struct {
  int x, y;
  int w, h;
} rect_t;

/* graphics module */
typedef struct {

} graphics_t;

graphics_t *graphics_create(void);
void graphics_destroy(graphics_t *graphics);

void graphics_set_mode(int width, int height);


/*
 *  OLD GRAPHICS MODULE
 */

void graphics_init(void);
void graphics_deinit(void);

/* graphics state */

void graphics_setBackground(uint8_t index);
void graphics_setColor(uint16_t index);
void graphics_setLayer(uint8_t index);
void graphics_setPattern(uint16_t pattern);
void graphics_setPalette(void);
void graphics_setClip(int x, int y, int w, int h);
void graphics_setBlend(uint8_t index, uint8_t mode);

void graphics_blendPixel(uint32_t *d, uint32_t s);
void graphics_drawPixel(int x, int y);
void graphics_drawRect(int x, int y, int w, int h);

/* graphics drawing */

void graphics_cls(uint16_t index);
void graphics_present(void);

void graphics_circ(int x, int y, int r);
void graphics_circfill(int x, int y, int r);

void graphics_line(int x1, int y1, int x2, int y2);
void graphics_point(int x, int y);

void graphics_rect(int x, int y, int w, int h);
void graphics_rectfill(int x, int y, int w, int h);

void graphics_blit(image_t *image, int dx, int dy, int sx, int sy, int sw, int sh);

void graphics_blit_test(uint8_t *data, int x, int y, int w, int h);

#endif
