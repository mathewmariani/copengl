/* stdlib */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// TODO: this should be a in library I think
#include "app.h"

/* engine */
#include "audio.h"
#include "event.h"
#include "filesystem.h"
#include "graphics.h"
#include "keyboard.h"
#include "mouse.h"
#include "timer.h"
#include "window.h"
#include "runtime.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* scripts */
// #include "embed/boot.wren.h"

/* forward declaration */
void wren_engine(WrenVM *vm);

char *strToLower(const char *str) {
  size_t length = strlen(str) + 1;
  char *result = malloc(length);
  for (size_t i = 0; i < length; i++) {
    result[i] = tolower(str[i]);
  }
  return result;
}

int main(int argc, char **argv) {

  /* technically this can all be moved to the modules folder:
   * the w_module.c can call module_init() itself.
   * although, the order cannot be determined.
   */

  app_t *app = app_create(&(app_desc) {
    .title = "fraise (SDL)",
    .width = GRAPHICS_WIDTH,
    .height = GRAPHICS_HEIGHT,
    .argv = argc > 1 ? argv[1] : NULL,
  });

  if (!app) {
    return EXIT_FAILURE;
  }

  app_run(app);
  app_destroy(app);

  return EXIT_SUCCESS;

  /* init everything */
  // event_init(vm);
  window_init();
  audio_init();
  filesystem_init();
  graphics_init();
  // keyboard_init();
  // mouse_init();
  // timer_init();


  /* initialize wren */
  WrenConfiguration config;
  wrenInitConfiguration(&config);

  /* setup callbacks */
  config.loadModuleFn = wren_load_module;
  config.bindForeignClassFn = wren_bind_class;
  config.bindForeignMethodFn = wren_bind_method;
  config.writeFn = wren_write;
  config.errorFn = wren_error;

  WrenVM *vm = wrenNewVM(&config);

  /* initialize wren modules */
  wren_engine(vm);

  ////
  //// FIXME: all this should be cleaned up with better error handling
  ////

  WrenInterpretResult result;// = wrenInterpret(vm, "boot", src_embed_boot_wren);

  switch (result) {
  case WREN_RESULT_COMPILE_ERROR:
    printf("Compile Error!\n");
    break;
  case WREN_RESULT_RUNTIME_ERROR:
    printf("Runtime Error!\n");
    break;
  case WREN_RESULT_SUCCESS:
    printf("Success!\n");
    break;
  }

  if (result != WREN_RESULT_SUCCESS) {
    return 0;
  }

  ////
  //// END FIXME
  ////

  wrenEnsureSlots(vm, 1);
  wrenGetVariable(vm, "boot", "App", 0);
  WrenHandle *appClass = wrenGetSlotHandle(vm, 0);

  /* create handles */
  WrenHandle *tickMethod = wrenMakeCallHandle(vm, "tick()");
  WrenHandle *keyboardMethod = wrenMakeCallHandle(vm, "i_keyboardState(_,_)");
  WrenHandle *mouseMethod = wrenMakeCallHandle(vm, "i_mouseState(_,_)");

  SDL_Event e;
  bool running = true;
  while (running) {
    while (SDL_PollEvent(&e) != 0) {
      switch (e.type) {
      /* quit event */
      case SDL_QUIT:
        running = false;
        break;

      /* keyboard events */
      case SDL_KEYDOWN:
      case SDL_KEYUP: {
        /* check for key repeat */
        // if (e.key.repeat) {
        //   if (!keyboard_has_key_repeat()) {
        //     break;
        //   }
        // }

        /* determine keyname and state */
        SDL_Keycode keyCode = e.key.keysym.sym;
        char *name = strToLower(SDL_GetKeyName(keyCode));
        bool state = e.key.state == SDL_PRESSED;

        /* return to wren */
        wrenEnsureSlots(vm, 3);
        wrenSetSlotHandle(vm, 0, appClass);
        wrenSetSlotString(vm, 1, name);
        wrenSetSlotBool(vm, 2, state);
        result = wrenCall(vm, keyboardMethod);

        /* free resouces */
        free(name);

        /* FIXME: this can still be interesting */
        // if (key = pun_sdl_remap_sym(sdl_event.key.keysym.sym)) {
        //   punity_on_key_down(key, 0);
        // }
        } break;

      /* mouse events */
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP: {
        /* determine button and state */
        uint8_t button = e.button.button;
        bool state = e.type == SDL_MOUSEBUTTONDOWN;

        /* swap button index */
        switch (button) {
        case SDL_BUTTON_RIGHT:
          button = 2;
          break;
        case SDL_BUTTON_MIDDLE:
          button = 3;
          break;
        }

        /* return to wren */
        wrenEnsureSlots(vm, 3);
        wrenSetSlotHandle(vm, 0, appClass);
        wrenSetSlotDouble(vm, 1, button);
        wrenSetSlotBool(vm, 2, state);
        result = wrenCall(vm, mouseMethod);

        } break;
      }
    }

    /* tick */
    wrenEnsureSlots(vm, 1);
    wrenSetSlotHandle(vm, 0, appClass);
    result = wrenCall(vm, tickMethod);
    
    switch (result) {
      case WREN_RESULT_COMPILE_ERROR:
        printf("Compile Error!\n");
        break;
      case WREN_RESULT_RUNTIME_ERROR:
        printf("Runtime Error!\n");
        break;
      case WREN_RESULT_SUCCESS:
        break;
    }
  }

  /* release handles */
  wrenReleaseHandle(vm, appClass);
  wrenReleaseHandle(vm, tickMethod);
  wrenReleaseHandle(vm, keyboardMethod);
  wrenReleaseHandle(vm, mouseMethod);

  /* close virtual machine */
  wrenFreeVM(vm);

  /* quit sdl */
  SDL_Quit();

  return 0;
}
