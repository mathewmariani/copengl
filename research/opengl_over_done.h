#ifndef OPENGL_H
#define OPENGL_H

/* FIXME: opengl abstraction should be specific to LITE... */
/* NOTE: it doesnt need to be flexible */

/* std */
#include <stdbool.h>
#include <stdint.h>

typedef enum {
  opengl_pixelformat_r8,
  opengl_pixelformat_r8ui,
  opengl_pixelformat_rgba8,
  opengl_pixelformat_rgba32,
  opengl_pixelformat_rgba8ui,

  /* always last. */
  opengl_pixelformat_max,
} opengl_pixel_format;

typedef enum {
  opengl_buffertarget_array,
  opengl_buffertarget_element,

  /* always last. */
  opengl_buffertarget_max,
} opengl_buffer_target;

typedef enum {
  opengl_texturetarget_2d,
  opengl_texturetarget_cube,
  opengl_texturetarget_3d,
  opengl_texturetarget_array,

  /* always last. */
  opengl_texturetarget_max,
} opengl_texture_target;

typedef enum {
  opengl_usage_static,
  opengl_usage_dynamic,
  opengl_usage_stream,

  /* always last. */
  opengl_usage_max,
} opengl_usage;

typedef enum {
  opengl_filtertype_nearest,
  opengl_filtertype_linear,

  /* always last. */
  opengl_filtertype_max,
} opengl_filter_type;

typedef enum {
  opengl_wraptype_clamp,
  opengl_wraptype_mirrored,
  opengl_wraptype_repeat,

  /* always last. */
  opengl_wraptype_max,
} opengl_wrap_type;

typedef enum {
  opengl_cull_mode_none,
  opengl_cull_mode_front,
  opengl_cull_mode_back,
  opengl_cull_mode_both,

  /* always last. */
  opengl_cull_mode_max,
} opengl_cull_mode;

typedef enum {
  opengl_depth_test_none,
  opengl_depth_test_never,
  opengl_depth_test_always,
  opengl_depth_test_less,
  opengl_depth_test_lequal,
  opengl_depth_test_greater,
  opengl_depth_test_gequal,
  opengl_depth_test_equal,
  opengl_depth_test_notequal,

  /* always last. */
  opengl_depth_test_max,
} opengl_depth_test;

typedef enum {
  opengl_winding_cw,
  opengl_winding_ccw,

  /* always last. */
  opengl_winding_max,  
} opengl_winding;

typedef enum {
  opengl_uniform_float,
  opengl_uniform_matrix,
  opengl_uniform_int,
  opengl_uniform_uint,
  opengl_uniform_bool,
  opengl_uniform_sampler,

  /* always last. */
  opengl_uniform_max,
} opengl_uniform_type;

typedef struct opengl_buffer_desc opengl_buffer_desc;
typedef struct opengl_texture_desc opengl_texture_desc;
typedef struct opengl_uniform_desc opengl_uniform_desc;
typedef struct opengl_shader_desc opengl_shader_desc;
typedef struct opengl_pipeline_desc opengl_pipeline_desc;

typedef struct opengl_userdata opengl_userdata;

/* context */
void opengl_setup();
void opengl_shutdown();

/* pipeline */
void opengl_apply_pipeline(const opengl_pipeline_desc *desc);

/* viewport */
void opengl_apply_viewport(int32_t x, int32_t y, int32_t w, int32_t h);

/* framebuffer */
uint32_t opengl_create_framebuffer(uint32_t width, uint32_t height, uint32_t texture);

/* buffers */
uint32_t opengl_create_buffer(const opengl_buffer_desc *desc);
void opengl_delete_buffer(size_t nitems, uint32_t *buffers);

/* texture */
uint32_t opengl_create_texture(const opengl_texture_desc *desc);
void opengl_bind_texture_to_unit(uint32_t image, int unit);
void opengl_update_texture(uint32_t image, opengl_userdata *data);

/* shaders */
uint32_t opengl_create_shader(const opengl_shader_desc *desc);
void opengl_delete_shader(uint32_t program);

void opengl_update_uniform(uint32_t program, int count, const opengl_uniform_desc *uniform);

struct opengl_userdata {
  const void *data;
  size_t size;
};

struct opengl_buffer_desc {
  opengl_buffer_target target;
  opengl_usage usage;
  opengl_userdata udata;
};

struct opengl_texture_desc {
  int32_t width;
  int32_t height;
  opengl_texture_target target;
  opengl_pixel_format internal;
  opengl_filter_type filter;
  opengl_wrap_type wrap;
};

struct opengl_uniform_desc {
  int components;
  opengl_uniform_type type;
  const char *name;

  union {
    void *data;
    float *floats;
    int *ints;
    unsigned int *uints;
  };
};

struct opengl_shader_desc {
  const char *vs_source;
  const char *fs_source;
};

struct opengl_pipeline_desc {
  /* blending */
  bool blend_mode;

  /* culling */
  opengl_cull_mode cull_mode;

  /* depth */
  opengl_depth_test depth_test;

  /* winding */
  opengl_winding winding;
};

#endif

#ifdef OPENGL_IMPLEMENTATION
#undef OPENGL_IMPLEMENTATION

#ifndef OPENGL_MALLOC
  #include <stdlib.h>
  #define OPENGL_MALLOC(sz)  malloc(sz)
  #define OPENGL_MALLOC(ptr) free(ptr)
#endif

#ifndef _OPENGL_PRIVATE
  #define _OPENGL_PRIVATE static
#endif

/* opengl loading library */
#if defined (OPENGL_USE_GL3W)
  #include "libraries/gl3w/gl3w.h"
#elif defined (OPENGL_USE_GLAD)
  #include "libraries/glad/glad.h"
#else
  #error OpenGL Loading Library not specified.
#endif

/* glad requires a proc address */
#if defined (OPENGL_USE_GLAD)
#if defined (OPENGL_USE_SDL2)
  #include <SDL2/SDL.h>
#elif defined (OPENGL_USE_GLFW)
  #include <glfw.h>
#else
  #error Platform abstraction not specified.
#endif
_OPENGL_PRIVATE void *opengl_getprocaddress(const char *name) {
#if defined (OPENGL_USE_SDL2)
  return SDL_GL_GetProcAddress(name);
#elif defined (OPENGL_USE_GLFW)
  return (GLADloadproc) glfwGetProcAddress;
#endif
}
#endif

typedef struct {
  int32_t width;
  int32_t height;

  /* opengl specifics */
  struct {
    GLuint id;
	GLenum target;
	GLenum internal;
	GLenum format;
	GLenum type;
  } gl;
} opengl_image_t;

#ifdef OPENGL_DEBUG
  _OPENGL_PRIVATE GLenum gl_has_error(const char *title) {
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR) {
      const char *error;
      switch (errorCode) {
        case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
        case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
        case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
        case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
        case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
        case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
      }
      printf("[%s] %s | %s (%d)\n", title, error, __FILE__, __LINE__);
    }
    return errorCode;
  }
#endif

_OPENGL_PRIVATE GLenum _opengl_pixelformat(opengl_pixel_format format) {
  switch (format) {
    case opengl_pixelformat_r8: return GL_R8;
    case opengl_pixelformat_r8ui: return GL_R8UI;
    case opengl_pixelformat_rgba8: return GL_RGBA8;
    case opengl_pixelformat_rgba32: return GL_RGBA32F;
    case opengl_pixelformat_rgba8ui: return GL_RGBA8UI;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_buffertarget(opengl_buffer_target target) {
  switch (target) {
    case opengl_buffertarget_array: return GL_ARRAY_BUFFER;
    case opengl_buffertarget_element: return GL_ELEMENT_ARRAY_BUFFER;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_texturetarget(opengl_texture_target target) {
  switch (target) {
    case opengl_texturetarget_2d: return GL_TEXTURE_2D;
    case opengl_texturetarget_cube: return GL_TEXTURE_CUBE_MAP;
    case opengl_texturetarget_3d: return GL_TEXTURE_3D;
    case opengl_texturetarget_array: return GL_TEXTURE_2D_ARRAY;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_usage(opengl_usage usage) {
  switch (usage) {
    case opengl_usage_static: return GL_STATIC_DRAW;
    case opengl_usage_dynamic: return GL_DYNAMIC_DRAW;
    case opengl_usage_stream: return GL_STREAM_DRAW;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_cullmode(opengl_cull_mode mode) {
  switch (mode) {
    case opengl_cull_mode_front: return GL_FRONT;
    case opengl_cull_mode_back: return GL_BACK;
    case opengl_cull_mode_both: return GL_FRONT_AND_BACK;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_depthtest(opengl_depth_test mode) {
  switch (mode) {
    case opengl_depth_test_always: return GL_ALWAYS;
    case opengl_depth_test_less: return GL_LESS;
    case opengl_depth_test_lequal: return GL_LEQUAL;
    case opengl_depth_test_greater: return GL_GREATER;
    case opengl_depth_test_gequal: return GL_GEQUAL;
    case opengl_depth_test_equal: return GL_EQUAL;
    case opengl_depth_test_notequal: return GL_NOTEQUAL;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_winding(opengl_winding mode) {
  switch (mode) {
    case opengl_winding_cw: return GL_CW;
    case opengl_winding_ccw: return GL_CCW;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_filtertype(opengl_filter_type type) {
  switch (type) {
    case opengl_filtertype_nearest: return GL_NEAREST;
    case opengl_filtertype_linear: return GL_LINEAR;
    default: return 0;
  }
}

_OPENGL_PRIVATE GLenum _opengl_wraptype(opengl_wrap_type type) {
  switch (type) {
    case opengl_wraptype_clamp: return GL_CLAMP_TO_EDGE;
    case opengl_wraptype_mirrored: return GL_MIRRORED_REPEAT;
    case opengl_wraptype_repeat: return GL_REPEAT;
    default: return 0;
  }
}

typedef struct {
  GLuint vao;

  struct {
    opengl_image_t textures[1];
  } cache;
} opengl_context;

_OPENGL_PRIVATE opengl_context _context;

void opengl_setup() {
#if defined (OPENGL_USE_GL3W)
  /* initialize opengl functions */
  if (gl3wInit()) {
    fprintf(stderr, "Failed to initialize OpenGL.\n");
    return;
  }
#elif defined (OPENGL_USE_GLAD)
  if (!gladLoadGLLoader(opengl_getprocaddress)) {
    fprintf(stderr, "Failed to initialize OpenGL.\n");
    return;
  }
#else
  #error OpenGL Loading Library not specified.
#endif

  /* core profile */
  glGenVertexArrays(1, &_context.vao);
  glBindVertexArray(_context.vao);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

void opengl_shutdown() {
  glDeleteVertexArrays(1, &_context.vao);
}

void opengl_apply_pipeline(const opengl_pipeline_desc *desc) {
  /* blending */
  if (!desc->blend_mode) {
    glDisable(GL_BLEND);
  } else {
    glEnable(GL_BLEND);
  }

  /* culling */
  if (desc->cull_mode == opengl_cull_mode_none) {
    glDisable(GL_CULL_FACE);
  } else {
    glEnable(GL_CULL_FACE);
    glCullFace(_opengl_cullmode(desc->cull_mode));
  }

  /* depth test */
  if (desc->depth_test == opengl_depth_test_none) {
    glDisable(GL_DEPTH_TEST);
  } else {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(_opengl_depthtest(desc->depth_test));
  }

  /* winding */
  glFrontFace(_opengl_winding(desc->winding));

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

void opengl_apply_viewport(int32_t x, int32_t y, int32_t w, int32_t h) {
  glViewport(x, y, w, h);
}

uint32_t opengl_create_framebuffer(uint32_t width, uint32_t height, uint32_t texture) {
  uint32_t framebuffer;

  /* create framebuffer */
  glGenFramebuffers(1, &framebuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

  /* attach color attachement */
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    printf("framebuffer is not complete\n");
    return 0;
  }

  /* unbind before returning */
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return framebuffer;
}

uint32_t opengl_create_buffer(const opengl_buffer_desc *desc) {
  const GLenum target = _opengl_buffertarget(desc->target);
  const GLenum usage = _opengl_usage(desc->usage);

  uint32_t buffer;
  glGenBuffers(1, &buffer);
  glBindBuffer(target, buffer);
  glBufferData(target, (GLsizeiptr) desc->udata.size, desc->udata.data, usage);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif

  return buffer;
}

void opengl_delete_buffer(size_t nitems, uint32_t *buffers) {
  glDeleteBuffers(nitems, buffers);
}

/* reference: https://www.khronos.org/registry/OpenGL-Refpages/es3.0/html/glTexImage2D.xhtml */
_OPENGL_PRIVATE GLenum _opengl_textureformat(opengl_pixel_format internal) {
  switch (internal) {
    case opengl_pixelformat_r8:      return GL_RED;
    case opengl_pixelformat_r8ui:    return GL_RED_INTEGER;
    case opengl_pixelformat_rgba8:   return GL_RGBA;
    case opengl_pixelformat_rgba32:  return GL_RGBA;
    case opengl_pixelformat_rgba8ui: return GL_RGBA_INTEGER;
  }
}

/* reference: https://www.khronos.org/registry/OpenGL-Refpages/es3.0/html/glTexImage2D.xhtml */
_OPENGL_PRIVATE GLenum _opengl_texturetype(opengl_pixel_format internal) {
  switch (internal) {
    case opengl_pixelformat_r8:      return GL_UNSIGNED_BYTE;
    case opengl_pixelformat_r8ui:    return GL_UNSIGNED_BYTE;
    case opengl_pixelformat_rgba8:   return GL_UNSIGNED_BYTE;
    case opengl_pixelformat_rgba32:  return GL_FLOAT;
    case opengl_pixelformat_rgba8ui: return GL_UNSIGNED_BYTE;
  }
}

_OPENGL_PRIVATE inline opengl_image_t _opengl_init_image(const opengl_texture_desc *desc) {
  return (opengl_image_t) {
	.width = desc->width,
	.height = desc->height,
	.gl = {
	  .id = 0,
	  .target = _opengl_texturetarget(desc->target),
	  .internal = _opengl_pixelformat(desc->internal),
      .format = _opengl_textureformat(desc->internal),
      .type = _opengl_texturetype(desc->internal),
	},
  };
}

uint32_t opengl_create_texture(const opengl_texture_desc *desc) {
  opengl_image_t img = _opengl_init_image(desc);

  /* create texture */
  glGenTextures(1, &img.gl.id);
  glBindTexture(GL_TEXTURE_2D, img.gl.id);

  /* texture filtering */
  const GLenum filter = _opengl_filtertype(desc->filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

  /* texture wrapping */
  const GLenum wrap = _opengl_wraptype(desc->wrap);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

  /* this needs to be called at least once */
  const GLenum target = img.gl.target;
  const GLenum internal = img.gl.internal;
  const GLenum format = img.gl.format;
  const GLenum type = img.gl.type;
  glTexImage2D(target, 0, internal, img.width, img.height, 0, format, type, NULL);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif

  /* cache intenals */
  _context.cache.textures[img.gl.id] = img;

  return img.gl.id;
}

void opengl_bind_texture_to_unit(uint32_t image, int unit) {
  /* cached */
  opengl_image_t *img = &_context.cache.textures[image];

  glActiveTexture(((GLenum) GL_TEXTURE0 + unit));
  glBindTexture(img->gl.target, img->gl.id);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

void opengl_update_texture(uint32_t image, opengl_userdata *data) {
  /* cached */
  opengl_image_t *img = &_context.cache.textures[image];

  /* determine format and type */
  const GLenum format = img->gl.format;
  const GLenum type = img->gl.type;
  glTexSubImage2D(img->gl.target, 0, 0, 0, img->width, img->height, format, type, data->data);

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

_OPENGL_PRIVATE uint32_t _opengl_compile_shader(GLenum glstage, const char *source) {
  /* create shader stage */
  uint32_t stage = glCreateShader(glstage);
  glShaderSource(stage, 1, &source, NULL);
  glCompileShader(stage);

  /* check for shader compile errors */
  GLint success;
  char log[512];
  glGetShaderiv(stage, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(stage, 512, NULL, log);
    printf("%s\n%s\n", __FUNCTION__, log);
  }

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif

  return stage;
}

uint32_t opengl_create_shader(const opengl_shader_desc *desc) {
  /* compile shader stages */
  uint32_t vs_stage = _opengl_compile_shader(GL_VERTEX_SHADER, desc->vs_source);
  uint32_t fs_stage = _opengl_compile_shader(GL_FRAGMENT_SHADER, desc->fs_source);

  uint32_t program = glCreateProgram();
  
  /* attach stages */
  glAttachShader(program, vs_stage);
  glAttachShader(program, fs_stage);
  
  /* link shaders */
  glLinkProgram(program);
  
  /* check for linking errors */
  GLint success;
  char log[512];
  glGetProgramiv(program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(program, 512, NULL, log);
  }

  /* cleanup resources */
  glDeleteShader(vs_stage);
  glDeleteShader(fs_stage);

  /* vertex attributes */

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif

  return program;
}

void opengl_delete_shader(uint32_t program) {
  glDeleteProgram(program);
}

void opengl_update_uniform(uint32_t program, int count, const opengl_uniform_desc *info) {
  GLint location = glGetUniformLocation(program, info->name);

  opengl_uniform_type type = info->type;
  if (type == opengl_uniform_float) {
    switch (info->components) {
      case 1:
        glUniform1fv(location, count, info->floats);
        break;
      case 2:
        glUniform2fv(location, count, info->floats);
        break;
      case 3:
        glUniform3fv(location, count, info->floats);
        break;
      case 4:
        glUniform4fv(location, count, info->floats);
        break;
    }
  } else if (type == opengl_uniform_int ||
             type == opengl_uniform_sampler) {
    switch (info->components) {
      case 1:
        glUniform1iv(location, count, info->ints);
        break;
      case 2:
        glUniform2iv(location, count, info->ints);
        break;
      case 3:
        glUniform3iv(location, count, info->ints);
        break;
      case 4:
        glUniform4iv(location, count, info->ints);
        break;
    }
  } else if (type == opengl_uniform_uint) {
    switch (info->components) {
      case 1:
        glUniform1uiv(location, count, info->uints);
        break;
      case 2:
        glUniform2uiv(location, count, info->uints);
        break;
      case 3:
        glUniform3uiv(location, count, info->uints);
        break;
      case 4:
        glUniform4uiv(location, count, info->uints);
        break;
    }
  }

#ifdef OPENGL_DEBUG
  gl_has_error(__FUNCTION__);
#endif
}

void opengl_draw_elements(GLenum p_type, GLenum i_type, int32_t base_element, int32_t num_elements, int32_t offset) {
  const GLsizei size = (i_type == GL_UNSIGNED_SHORT) ? 2 : 4;
  const GLvoid *indices = (const GLvoid *) (GLintptr) (base_element * size + offset);
  glDrawElements(p_type, num_elements, i_type, indices);
}

#endif
