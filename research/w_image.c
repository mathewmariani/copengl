/* std */
#include <stdio.h>

/* engine */
#include "app.h"
#include "filesystem.h"
#include "graphics.h"
#include "bitmap.h"
#include "runtime.h"

static void w_image_allocate(WrenVM *vm) {
  /* retrieve from wren */
  bitmap_t *self = (bitmap_t *) wrenSetSlotNewForeign(vm, 0, 0, sizeof(bitmap_t));
  const char *filename = wrenGetSlotString(vm, 1);

  /* load file data */
  size_t size;
  void *filedata = filesystem_read(filename, &size);
  if (!filedata) {
    printf("could not read file\n");
    return;
  }

  data_t data = (data_t) {
    .data = filedata,
    .size = size,
  };

  graphics_t *gfx = (graphics_t *) ((app_t *) wrenGetUserData(vm))->graphics;

  bitmap_init(self, &(gfx->state.palette), &data);
  // image_init(self, &(data_t) {
  //   .data = filedata,
  //   .size = size,
  // });

  /* free resources */
  free(filedata);
}

static void w_image_finalize(void *bytes) {
  // image_t *self = bytes;
  // image_deinit(self);
}

void w_image_getWidth(WrenVM *vm) {
  /* retrieve from wren */
  bitmap_t *self = (bitmap_t *) wrenGetSlotForeign(vm, 0);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, self->width);
}

void w_image_getHeight(WrenVM *vm) {
  /* retrieve from wren */
  bitmap_t *self = (bitmap_t *) wrenGetSlotForeign(vm, 0);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, self->height);
}

void w_image_pget(WrenVM *vm) {
  /* retrieve from wren */
  bitmap_t *self = (bitmap_t *) wrenGetSlotForeign(vm, 0);
  uint32_t x = (uint32_t) wrenGetSlotDouble(vm, 1);
  uint32_t y = (uint32_t) wrenGetSlotDouble(vm, 2);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, bitmap_get(self, x, y));
}

void w_image_pset(WrenVM *vm) {
  /* retrieve from wren */
  bitmap_t *self = (bitmap_t *) wrenGetSlotForeign(vm, 0);
  uint32_t x = (uint32_t) wrenGetSlotDouble(vm, 1);
  uint32_t y = (uint32_t) wrenGetSlotDouble(vm, 2);
  uint8_t *color = (uint8_t) wrenGetSlotDouble(vm, 3);

  bitmap_set(self, x, y, color);
}

const static function_desc functions[] = {
  /* properties */
  { false, "width", w_image_getWidth },
  { false, "height", w_image_getHeight },

  /* properties */
  { false, "get(_,_)", w_image_pget },
  { false, "set(_,_,_)", w_image_pset },

  /* always last. */
  { false, NULL, NULL },
};

void wren_image(WrenVM *vm) {
  wren_register_class(vm, &(class_desc) {
    .name = "Image",
    .type = CLASS_IMAGE,
    .methods = {
      .allocate = w_image_allocate,
      .finalize = w_image_finalize,
    },
    .functions = functions,
  });
}
