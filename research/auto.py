#!/usr/bin/python2.7
import os, sys, shutil, re, textwrap

EMBED_DIR = "../src/embed"

PATTERN = """\
const unsigned char %s[] = 
{
%s
};
"""

def listdir(path):
  return [os.path.join(dp, f) for dp, dn, fn in os.walk(path) for f in fn if f.endswith(".wren") ]

def make_c_include(name, data):
    # FIXME: can this be pythonified
    res = ""
    for c in data:
        res += str("0x{:02x}".format(ord(c))) + ", "
    res = res.rstrip(", ")

    return (PATTERN % (re.sub("[^a-z0-9]", "_", name.lower()), textwrap.fill(res, width=79)))


def main():
  os.chdir(sys.path[0])

  for filename in listdir(EMBED_DIR):
    name = os.path.basename(filename)
    text = make_c_include(name, open(filename).read())
    open("%s/%s.h" % (EMBED_DIR, name), "wb").write(text.encode())


if __name__ == "__main__":
  main()
