#ifndef RUNTIME_H
#define RUNTIME_H

#include "wren.h"

#define ASSERT_SLOT_TYPE(vm, slot, type, name)           \
  if (wrenGetSlotType(vm, slot) != WREN_TYPE_##type) {   \
    do {                                                 \
      wrenSetSlotString(vm, 0, #name " was not " #type); \
      wrenAbortFiber(vm, 0);                             \
    } while(false);                                      \
    return;                                              \
  }

typedef enum {
  CLASS_AUDIO,
  CLASS_FILESYSTEM,
  CLASS_FONT,
  CLASS_GRAPHICS,
  CLASS_KEYBOARD,
  CLASS_MOUSE,
  CLASS_RANDOM,
  CLASS_SOURCE,         // FIXME: remove me?
  CLASS_TIMER,
  CLASS_WINDOW,

  // always last.
  CLASS_COUNT,
} lite_classes_t;

typedef enum {
  MODULE_LITE,

  // always last.
  MODULE_COUNT,
} lite_modules_t;

typedef void (*wren_CFunction) (WrenVM *vm);

typedef struct {
  bool is_static;
  const char *signature;
  WrenForeignMethodFn method;
} function_desc;

typedef struct {
  const char *name;
  const int type;
  WrenForeignClassMethods methods;
  const function_desc *functions;
} class_desc;

typedef struct {
  void *module;
  const char *name;
  const char *source;
  const int type;
  const wren_CFunction *classes;
} module_desc;

extern void wren_register_class(WrenVM *vm, class_desc *desc);
extern void wren_register_module(WrenVM *vm, module_desc *desc);

extern WrenLoadModuleResult wren_load_module(WrenVM *vm, const char *moduleName);
extern WrenForeignMethodFn wren_bind_method(WrenVM *vm, const char *moduleName, const char *className, bool isStatic, const char *signature);
extern WrenForeignClassMethods wren_bind_class(WrenVM *vm, const char *moduleName, const char *className);

extern void wren_write(WrenVM *vm, const char *text);
extern void wren_error(WrenVM *vm, WrenErrorType type, const char *module, int line, const char *str);

#endif
