#ifndef DATA_H
#define DATA_H

/* std */
#include <stdlib.h>

typedef struct {
  void *data;
  size_t size;
} data_t;

#endif
