#ifndef CONFIG_H
#define CONFIG_H

/* compile time definitions */
enum {
  /* graphics */
  MAX_COLORS     = 256,

  /* keyboard */
  MAX_KEYS       = 512,

  /* window */
  LITE_WIDTH     = 256,
  LITE_HEIGHT    = 144,
};

/* graphics definitions */
#define COLOR_A            0xFF000000
#define COLOR_R            0x00FF0000
#define COLOR_G            0x0000FF00
#define COLOR_B            0x000000FF

#define COLOR_MASK_A       0xFF000000
#define COLOR_MASK_R       0x00FF0000
#define COLOR_MASK_G       0x0000FF00
#define COLOR_MASK_B       0x000000FF

#define COLOR_COMP_R(c)    (((c >> 24) & 0xFF) / 255.0f)
#define COLOR_COMP_G(c)    (((c >> 16) & 0xFF) / 255.0f)
#define COLOR_COMP_B(c)    (((c >>  8) & 0xFF) / 255.0f)
#define COLOR_COMP_A(c)    (((c >>  0) & 0xFF) / 255.0f)

#endif
