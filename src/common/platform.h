#ifndef PLATFORM_H
#define PLATFORM_H

/* stubbed */
#define STUBBED(x) \
  fprintf(stderr, "STUBBED: %s at %s (%s:%d)\n", x, __FUNCTION__, __FILE__, __LINE__);

/* unused */
#define UNUSED(x) (void)(x)

/* platform */
#if defined(WIN32) || defined(_WIN32)
  #define OS_WINDOWS 1
#endif
#if defined(__APPLE__)
  #include <TargetConditionals.h>
  #if defined(TARGET_OS_MAC)
    #define OS_MACOS 1
  #elif defined(TARGET_OS_IPHONE)
    #define OS_IOS 1
  #endif
#endif
#if defined(__linux__)
  #define OS_LINUX 1
#endif
#if defined(__FreeBSD__)
  #define OS_FREEBSD 1
#endif

/* check platform */
#if !defined(OS_WINDOWS) && !defined(OS_MACOS) && !defined(OS_IOS) && !defined(OS_LINUX) && !defined(OS_FREEBSD)
  #error Could not detect target platform
#endif

/* debug */
#if defined(_DEBUG)
  #define DEBUG 1
#endif

/* library stuff */
#ifdef OS_WINDOWS
  #define LITE_EXPORT __declspec(dllexport)
#else
  #define LITE_EXPORT
#endif

#ifndef LITE_PRIVATE
  #define LITE_PRIVATE static
#endif

#ifndef LITE_ASSERT
  #include <assert.h>
  #define LITE_ASSERT(c) assert(c)
#endif

/* memory allocators */
#ifndef LITE_MALLOC
  #include <stdlib.h>
  #define LITE_MALLOC(sz)       malloc(sz)
  #define LITE_CALLOC(n,sz)     calloc(n,sz)
  #define LITE_REALLOC(ptr,sz)  realloc(ptr,sz)
  #define LITE_FREE(ptr)        free(ptr)
#endif

/* boolean types */
#include <stdbool.h>

/* floating point types */
typedef float f32;
typedef double f64;

/* integer types */
#include <stdint.h>
typedef int8_t   i8;
typedef uint8_t  u8;
typedef int16_t  i16;
typedef uint16_t u16;
typedef int32_t  i32;
typedef uint32_t u32;
typedef int64_t  i64;
typedef uint64_t u64;

// typedef signed char        i8;
// typedef unsigned char      u8;
// typedef short              i16;
// typedef unsigned short     u16;
// typedef int                i32;
// typedef unsigned int       u32;
// typedef long long          i64;
// typedef unsigned long long u64;

#endif
