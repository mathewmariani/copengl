/* std */
#include <stdio.h>
#include <string.h>

/* engine */
#include "engine.h"
#include "common/runtime.h"
#include "filesystem.h"

typedef struct {
  bool is_static;
  const char *signature;
  const WrenForeignMethodFn method;
} wren_function_desc;

typedef struct {
  const char *name;
  WrenForeignClassMethods methods;
  const function_desc *functions;
} wren_class_desc;

typedef struct {
  const char *name;
  const char *source;
} wren_module_desc;

static wren_class_desc classes[CLASS_COUNT];
static wren_module_desc modules[MODULE_COUNT];

void wren_register_class(WrenVM *vm, class_desc *desc) {
  printf("registering class: \"%s\"\n", desc->name);
  classes[(int) desc->type] = (wren_class_desc) {
    .name = desc->name,
    .methods = desc->methods,
    .functions = desc->functions,
  };    
}

void wren_register_module(WrenVM *vm, module_desc *desc) {
  printf("Registering module: \"%s\"\n", desc->name);

  modules[(int) desc->type] = (wren_module_desc) {
    .name = desc->name,
    .source = desc->source,
  };
}

static wren_class_desc *_find_class(const char *name) {
  for (int i = 0; i < CLASS_COUNT; i++) {
    if (classes[i].name != NULL && strcmp(classes[i].name, name) == 0) {
      return &classes[i];
    }
  }
  printf("Could not find class %s\n", name);
  return NULL;
}

static wren_module_desc *_find_module(const char *name) {
  for (int i = 0; i < MODULE_COUNT; ++i) {
    if (strcmp(modules[i].name, name) == 0) {
      return &modules[i];
    }
  }
  printf("Could not find module %s\n", name);
  return NULL;
}

static char *read_built_in_module(WrenVM *vm, const char *moduleName) {
  wren_module_desc *module = _find_module(moduleName);
  if (module == NULL) {
    return NULL;
  }
  size_t length = strlen(module->source);

  /* FIXME: memory leak */
  char *source = LITE_CALLOC(length + 1, sizeof(char));
  strcpy(source, module->source);
  source[length] = '\0';
  return source;
}

static char *read_module_from_file(WrenVM *vm, const char *moduleName) {
  // add `.wren` extension to load external modules
  const char *extension = ".wren";
  char *filename = malloc(strlen(moduleName) + strlen(extension) + 1);
  strcpy(filename, moduleName);
  strcat(filename, extension);

  // FIXME: do we need to copy here?
  filesystem_t *fs = (filesystem_t *) ((engine_t *) wrenGetUserData(vm))->filesystem;
  char *filedata = (char *) filesystem_read(fs, filename, NULL);
  size_t length = strlen(filedata);
  char *source = calloc(length + 1, sizeof(char));
  strcpy(source, filedata);
  source[length] = '\0';

  /* free resources */
  free(filedata);
  free(filename);

  return source;
}

WrenLoadModuleResult wren_load_module(WrenVM *vm, const char *moduleName) {
  WrenLoadModuleResult result = { 0 };

  printf("Trying to read module %s\n", moduleName);
  char *module = read_built_in_module(vm, moduleName);
  if (module != NULL) {
    result.source = module;
    return result;
  }
  module = read_module_from_file(vm, moduleName);
  if (module != NULL) {
    result.source = module;
    return result;
  }

  return result;
}

void wren_write(WrenVM *vm, const char *text) {
  printf("%s", text);
}

void wren_error(WrenVM *vm, WrenErrorType type, const char *module, int line, const char *str) {
  switch (type) {
  case WREN_ERROR_COMPILE:
    fprintf(stderr, "[%s line %d] %s\n", module, line, str);
    break;
  case WREN_ERROR_RUNTIME:
    fprintf(stderr, "%s\n", str);
    break;
  case WREN_ERROR_STACK_TRACE:
    fprintf(stderr, "[%s line %d] in %s\n", module, line, str);
    break;
  }
}

WrenForeignMethodFn wren_bind_method(WrenVM *vm, const char *moduleName, const char *className, bool isStatic, const char *signature) {
  printf("Looking for %s:%s:%s:%s\n", __FUNCTION__, moduleName, className, signature);

  wren_class_desc *class = _find_class(className);
  if (class == NULL) {
    return NULL;
  }
  for (const function_desc *fn = class->functions; fn->signature != NULL; fn++) {
    if (strcmp(fn->signature, signature) == 0 && (fn->is_static == isStatic)) {
      return fn->method;
    }
  }
  return NULL;
}

WrenForeignClassMethods wren_bind_class(WrenVM *vm, const char *moduleName, const char *className) {
  printf("Looking for %s:%s:%s\n", __FUNCTION__, moduleName, className);

  wren_class_desc *class = _find_class(className);
  if (class == NULL) {
    return (WrenForeignClassMethods) {
      .allocate = NULL,
      .finalize = NULL
    };
  }
  return class->methods;
}