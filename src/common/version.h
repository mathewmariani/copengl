#ifndef VERSION_H
#define VERSION_H

#define VERSION_MAJOR      0
#define VERSION_MINOR      1
#define VERSION_REVISION   0
#define RELEASE_CANDIDATE  0

#define VERSION_NUMBER     \
  ((VERSION_MAJOR << 16) | \
   (VERSION_MINOR <<  8) | \
   (VERSION_REVISION))

#ifdef DEBUG
  #define _CONCAT_VERSION(m, n, r) \
    #m "." #n "." #r "-debug"
#else
  #define _CONCAT_VERSION(m, n, r) \
    #m "." #n "." #r
#endif

#define _MAKE_VERSION(m, n, r) \
  _CONCAT_VERSION(m, n, r)

#define VERSION_STRING\
  _MAKE_VERSION(VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION)

#endif
