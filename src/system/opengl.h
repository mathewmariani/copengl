#ifndef OPENGL_H
#define OPENGL_H

/* std */
#include <stdbool.h>
#include <stdint.h>

/* opengl loading library */
#if defined (GL_USE_GL3W)
  #include "libraries/gl3w/gl3w.h"
#elif defined (GL_USE_GLAD)
  #include "libraries/glad/glad.h"
#else
  #error OpenGL Loading Library not specified.
#endif

/* compile time constants */
enum {
  GL_MAX_SHADERSTAGE_IMAGES = 12,
};

#define GL_NO_MODE GL_INVALID_ENUM
#define GL_NO_TEST GL_INVALID_ENUM

typedef struct gl_buffer_t gl_buffer_t;
typedef struct gl_pipeline_t gl_pipeline_t;
typedef struct gl_shader_t gl_shader_t;
typedef struct gl_image_t gl_image_t;

typedef struct {
  float r, g, b, a;
} gl_color;

typedef struct {
  const void *data;
  size_t size;
} gl_userdata_t;

typedef struct {
  gl_buffer_t *vertex_buffer;
  gl_buffer_t *index_buffer;
  gl_image_t *image[GL_MAX_SHADERSTAGE_IMAGES];
} gl_bindings_t;

typedef struct {
  GLenum target;
  GLenum usage;
  gl_userdata_t udata;
} gl_buffer_desc;

typedef struct {
  int32_t width;
  int32_t height;
  GLenum target;
  GLenum internal;
  GLenum filter;
  GLenum wrap;
} gl_texture_desc;

typedef struct {
  const char *vs_source;
  const char *fs_source;
} gl_shader_desc;

typedef struct {
  GLenum action;
  gl_color value;
} gl_clear_desc;

typedef struct {
  gl_shader_t *shader;

  GLenum blend_mode;
  GLenum cull_mode;
  GLenum depth_test;
  GLenum winding;

  GLenum primitive_type;
  GLenum index_type;

  struct {
    struct {
      GLenum format;
      GLint size;
      GLint offset;
    } attr[2];
  } layout;
} gl_pipeline_desc;

typedef struct {
  gl_buffer_t *vertex_buffer;
  gl_buffer_t *index_buffer;
  gl_image_t *image[GL_MAX_SHADERSTAGE_IMAGES];
} gl_bindings_desc;

/* context */
void gl_setup();
void gl_shutdown();

void gl_clear(const gl_clear_desc *desc);

/* pipeline */
gl_pipeline_t *gl_create_pipeline(const gl_pipeline_desc *desc);
void gl_destroy_pipeline(gl_pipeline_t *pipeline);
void gl_apply_pipeline(const gl_pipeline_t *desc);

/* bindings */
void gl_apply_bindings(const gl_bindings_t *bindings);

/* viewport */
void gl_apply_viewport(int32_t x, int32_t y, int32_t w, int32_t h);

/* buffers */
gl_buffer_t *gl_create_buffer(const gl_buffer_desc *desc);
void gl_destroy_buffer(gl_buffer_t *buffer);

/* texture */
gl_image_t *gl_create_image(const gl_texture_desc *desc);
void gl_destroy_image(gl_image_t *image);
void gl_update_image(gl_image_t *image, gl_userdata_t *data);

/* shaders */
gl_shader_t *gl_create_shader(const gl_shader_desc *desc);
void gl_destroy_shader(gl_shader_t *shader);

/* draw */
void gl_draw(int32_t base_element, int32_t num_elements, int32_t offset);

#endif

#ifdef GL_IMPLEMENTATION
#undef GL_IMPLEMENTATION

#ifndef GL_MALLOC
  #include <stdlib.h>
  #define GL_MALLOC(sz) malloc(sz)
  #define GL_FREE(ptr)  free(ptr)
#endif

#ifndef GL_ASSERT
  #include <assert.h>
  #define GL_ASSERT(c) assert(c)
#endif

#ifndef _GL_PRIVATE
  #define _GL_PRIVATE static
#endif

/* debug */
#ifndef GL_DEBUG
  #ifdef _DEBUG
    #define GL_DEBUG (1)
  #endif
#endif

/* glad requires a proc address */
#if defined (GL_USE_GLAD)
#if defined (GL_USE_SDL2)
  #include <SDL2/SDL.h>
#elif defined (OPENGL_USE_GLFW)
  #include <glfw.h>
#else
  #error Platform abstraction not specified.
#endif
_GL_PRIVATE void *_gl_getprocaddress(const char *name) {
#if defined (GL_USE_SDL2)
  return SDL_GL_GetProcAddress(name);
#elif defined (GL_USE_GLFW)
  return (GLADloadproc) glfwGetProcAddress;
#endif
}
#endif

#ifdef GL_DEBUG
  _GL_PRIVATE GLenum _gl_has_error(const char *title, int line) {
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR) {
      const char *error;
      switch (errorCode) {
        case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
        case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
        case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
        case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
        case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
        case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
      }
      printf("[%s] %s (%d)\n", title, error, line);
    }
    return errorCode;
  }
#endif

#ifdef GL_DEBUG
  #define _GL_CHECK_ERROR() _gl_has_error(__FUNCTION__, __LINE__)
#else
  #define _GL_CHECK_ERROR() do {} while(0)
#endif

struct gl_buffer_t {
  GLuint buffer;
  GLenum target;
};

struct gl_shader_t {
  GLuint program;
};

typedef struct {
  GLenum format;
  GLint size;
  GLint offset;
} gl_attr_t;

struct gl_pipeline_t {
  gl_shader_t *shader;
  GLenum blend_mode;
  GLenum cull_mode;
  GLenum depth_test;
  GLenum winding;

  GLenum primitive_type;
  GLenum index_type;

  gl_attr_t attributes[2];
};

struct gl_image_t {
  int32_t width;
  int32_t height;
  GLuint texture;
  GLenum target;
  GLenum internal;
  GLenum format;
  GLenum type;
};

typedef struct {
  GLenum depth;
  GLenum blend;
  GLenum cull_mode;
  GLenum winding;

  GLuint vertex_buffer;
  GLuint index_buffer;
  GLuint program;

  GLenum primitive_type;
  GLenum index_type;

  gl_pipeline_t *pipeline;
} _gl_state;

typedef struct {
  GLuint vao;
  _gl_state state;
} gl_context;

_GL_PRIVATE gl_context _context;

void gl_setup() {
#if defined (GL_USE_GL3W)
  /* initialize opengl functions */
  if (gl3wInit()) {
    fprintf(stderr, "Failed to initialize OpenGL.\n");
    return;
  }
#elif defined (GL_USE_GLAD)
  if (!gladLoadGLLoader(_gl_getprocaddress)) {
    fprintf(stderr, "Failed to initialize OpenGL.\n");
    return;
  }
#else
  #error OpenGL Loading Library not specified.
#endif

#ifdef GL_DEBUG
  /* opengl information */
  printf("OpenGL %s, GLSL %s\n",
    glGetString(GL_VERSION),
    glGetString(GL_SHADING_LANGUAGE_VERSION)
  );
#endif

  /* core profile */
  glGenVertexArrays(1, &_context.vao);
  glBindVertexArray(_context.vao);

  _GL_CHECK_ERROR();
}

void gl_shutdown() {
  glDeleteVertexArrays(1, &_context.vao);
}

void gl_clear(const gl_clear_desc *desc) {
  const gl_color *c = &desc->value;
  glClearColor(c->r, c->g, c->b, c->a);
  glClear(desc->action);
}

_GL_PRIVATE GLsizei _gl_type_size(GLenum t) {
  switch (t) {
    case GL_FLOAT: return 4;
  }
}

_GL_PRIVATE inline void _gl_init_pipeline(gl_pipeline_t *pipeline, const gl_pipeline_desc *desc) {
  *pipeline = (gl_pipeline_t) {
    /* program */
    .shader = desc->shader,

    /* state */
    .blend_mode = desc->blend_mode,
    .cull_mode = desc->cull_mode,
    .depth_test = desc->depth_test,
    .winding = desc->winding,

    /* buffer */
    .primitive_type = desc->primitive_type,
    .index_type = desc->index_type,
  };
}

gl_pipeline_t *gl_create_pipeline(const gl_pipeline_desc *desc) {
  gl_pipeline_t *pipeline = GL_MALLOC(sizeof(gl_pipeline_t));
  _gl_init_pipeline(pipeline, desc);

  /* vertex attributes */
  for (GLint i = 0; i < 2; ++i) {
    pipeline->attributes[i] = (gl_attr_t) {
      .format = desc->layout.attr[i].format,
      .size = desc->layout.attr[i].size,
      .offset = desc->layout.attr[i].offset,
    };
  }

  return pipeline;
}

void gl_destroy_pipeline(gl_pipeline_t *pipeline) {
  GL_FREE(pipeline);
  pipeline = NULL;
}

/* FIXME: this should take a pipeline and not a description */
void gl_apply_pipeline(const gl_pipeline_t *pipeline) {
  /* buffer stuff */
  _context.state.primitive_type = pipeline->primitive_type;
  _context.state.index_type = pipeline->index_type;
  _context.state.pipeline = pipeline;

  /* blending */
  if (pipeline->blend_mode == GL_INVALID_ENUM) {
    glDisable(GL_BLEND);
  } else {
    glEnable(GL_BLEND);
  }

  /* culling */
  if (pipeline->cull_mode == GL_INVALID_ENUM) {
    glDisable(GL_CULL_FACE);
  } else {
    glEnable(GL_CULL_FACE);
    glCullFace(pipeline->cull_mode);
  }

  /* depth test */
  if (pipeline->depth_test == GL_INVALID_ENUM) {
    glDisable(GL_DEPTH_TEST);
  } else {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(pipeline->depth_test);
  }

  /* winding */
  glFrontFace(pipeline->winding);

  /* attach shader */
  glUseProgram(pipeline->shader->program);

  _GL_CHECK_ERROR();
}

void gl_apply_bindings(const gl_bindings_t *bindings) {
  /* texture */
  for (GLint i = 0; i < GL_MAX_SHADERSTAGE_IMAGES; ++i) {
    const gl_image_t *image = bindings->image[i];
      if (image == NULL) {
      continue;
    }

    /* FIXME: not sure what do do with glActiveTexture */
    //glActiveTexture(((GLenum) GL_TEXTURE0 + i));
    glBindTexture(image->target, image->texture);
  }

  /* vertex buffer */
  const gl_buffer_t *vb = bindings->vertex_buffer;
  glBindBuffer(vb->target, vb->buffer);

  /* index buffer */
  const gl_buffer_t *ib = bindings->index_buffer;
  glBindBuffer(ib->target, ib->buffer);

  /* vertex attributes */
  gl_pipeline_t *pip = _context.state.pipeline;
  GLint attr_offset = 0;
  for (GLint i = 0; i < 2; ++i) {
    glEnableVertexAttribArray(i);

    const gl_attr_t *attr = &pip->attributes[i];
    const GLint size = _gl_type_size(attr->format);

    /* FIXME: `4` shouldn't be hardcoded */
    glVertexAttribPointer(i, attr->size, attr->format,
      GL_FALSE, 4 * size,
      (const GLvoid *) (GLintptr) (attr->offset * size));
  }

  _GL_CHECK_ERROR();
}

void gl_apply_viewport(int32_t x, int32_t y, int32_t w, int32_t h) {
  glViewport(x, y, w, h);
}

_GL_PRIVATE inline void _gl_init_buffer(gl_buffer_t *buffer, const gl_buffer_desc *desc) {
  *buffer = (gl_buffer_t) {
    .buffer = GL_INVALID_VALUE,
    .target = desc->target,
  };
}

gl_buffer_t *gl_create_buffer(const gl_buffer_desc *desc) {
  gl_buffer_t *buffer = GL_MALLOC(sizeof(gl_buffer_t));
  _gl_init_buffer(buffer, desc);

  const GLenum usage = desc->usage;

  glGenBuffers(1, &buffer->buffer);
  glBindBuffer(buffer->target, buffer->buffer);
  glBufferData(buffer->target, (GLsizeiptr) desc->udata.size, desc->udata.data, usage);

  glBindBuffer(buffer->target, 0);

  _GL_CHECK_ERROR();

  return buffer;
}

void gl_destroy_buffer(gl_buffer_t *buffer) {
  GL_FREE(buffer);
  buffer = NULL;
}

_GL_PRIVATE GLenum _gl_textureformat(GLenum f) {
  switch (f) {
    case GL_R8:      return GL_RED;
    case GL_R8UI:    return GL_RED_INTEGER;
    case GL_RGBA8:   return GL_RGBA;
    case GL_RGBA32F: return GL_RGBA;
    case GL_RGBA8UI: return GL_RGBA_INTEGER;
  }
}

_GL_PRIVATE GLenum _gl_texturetype(GLenum f) {
  switch (f) {
    case GL_R8:      return GL_UNSIGNED_BYTE;
    case GL_R8UI:    return GL_UNSIGNED_BYTE;
    case GL_RGBA8:   return GL_UNSIGNED_BYTE;
    case GL_RGBA32F: return GL_FLOAT;
    case GL_RGBA8UI: return GL_UNSIGNED_BYTE;
  }
}

_GL_PRIVATE inline void _gl_init_image(gl_image_t *image, const gl_texture_desc *desc) {
  *image = (gl_image_t) {
    .width = desc->width,
    .height = desc->height,
    .texture = 0,
    .target = desc->target,
    .internal = desc->internal,
    .format = _gl_textureformat(desc->internal),
    .type = _gl_texturetype(desc->internal),
  };
}

/* FIXME: should we return a pointer? */
gl_image_t *gl_create_image(const gl_texture_desc *desc) {
  gl_image_t *image = GL_MALLOC(sizeof(gl_image_t));
  _gl_init_image(image, desc);

  const GLenum target = image->target;

  /* create texture */
  glGenTextures(1, &image->texture);
  glBindTexture(target, image->texture);

  /* texture filtering */
  const GLenum filter = desc->filter;
  glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
  glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);

  /* texture wrapping */
  const GLenum wrap = desc->wrap;
  glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap);
  glTexParameteri(target, GL_TEXTURE_WRAP_T, wrap);

  /* this needs to be called at least once */
  glTexImage2D(target,
    0, desc->internal,
    image->width, image->height,
    0,
    image->format,
    image->type,
    NULL);

  /* unbind texture */
  glBindTexture(target, 0);

  _GL_CHECK_ERROR();

  return image;
}

void gl_destroy_image(gl_image_t *image) {
  glDeleteTextures(1, &image->texture);
  image = NULL;
}

void gl_update_image(gl_image_t *image, gl_userdata_t *data) {
  glBindTexture(image->target, image->texture);
  glTexSubImage2D(image->target,
    0, 0, 0,
    image->width, image->height,
    image->format, image->type,
    data->data);
  glBindTexture(image->target, 0);
  
  _GL_CHECK_ERROR();
}

_GL_PRIVATE uint32_t _gl_compile_shader(GLenum glstage, const char *source) {
  /* create shader stage */
  uint32_t stage = glCreateShader(glstage);
  glShaderSource(stage, 1, &source, NULL);
  glCompileShader(stage);

  /* check for shader compile errors */
  GLint success;
  char log[512];
  glGetShaderiv(stage, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(stage, 512, NULL, log);
    printf("%s\n%s\n", __FUNCTION__, log);
  }

  _GL_CHECK_ERROR();

  return stage;
}

gl_shader_t *gl_create_shader(const gl_shader_desc *desc) {
  /* compile shader stages */
  uint32_t vs_stage = _gl_compile_shader(GL_VERTEX_SHADER, desc->vs_source);
  uint32_t fs_stage = _gl_compile_shader(GL_FRAGMENT_SHADER, desc->fs_source);

  gl_shader_t *shader = GL_MALLOC(sizeof(gl_shader_t));

  shader->program = glCreateProgram();
  
  /* attach stages */
  glAttachShader(shader->program, vs_stage);
  glAttachShader(shader->program, fs_stage);
  
  /* link shaders */
  glLinkProgram(shader->program);
  
  /* check for linking errors */
  GLint success;
  char log[512];
  glGetProgramiv(shader->program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(shader->program, 512, NULL, log);
    printf("%s\n%s\n", __FUNCTION__, log);
    gl_destroy_shader(shader);
  }

  /* cleanup resources */
  glDeleteShader(vs_stage);
  glDeleteShader(fs_stage);

  _GL_CHECK_ERROR();

  return shader;
}

void gl_destroy_shader(gl_shader_t *shader) {
  glDeleteProgram(shader->program);
  shader = NULL;
}

void gl_draw(int32_t base_element, int32_t num_elements, int32_t offset) {
  const GLenum i_type = _context.state.index_type;
  const GLenum p_type = _context.state.primitive_type;

  if (GL_INVALID_VALUE != i_type) {
    const GLsizei size = (i_type == GL_UNSIGNED_SHORT) ? 2 : 4;
    const GLvoid *indices = (const GLvoid *) (GLintptr) (base_element * size + offset);
    glDrawElements(p_type, num_elements, i_type, indices);
  } else {
    glDrawArrays(p_type, base_element, num_elements);
  }

  _GL_CHECK_ERROR();
}

#endif
