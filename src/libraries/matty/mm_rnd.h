#ifndef MM_RND_H
#define MM_RND_H

#ifndef MM_INT_TYPES_DEFINED
#define MM_INT_TYPES_DEFINED
#include <stdint.h>
typedef int8_t   mm_i8;
typedef uint8_t  mm_u8;
typedef int16_t  mm_i16;
typedef uint16_t mm_u16;
typedef int32_t  mm_i32;
typedef uint32_t mm_u32;
typedef int64_t  mm_i64;
typedef uint64_t mm_u64;
#endif

typedef struct mmrnd_context mmrnd_context;

void mmrnd_init(mmrnd_context *context);
mm_u16 mmrnd_number(mmrnd_context *context);

struct mmrnd_context {
  mm_u8 reg[4];
};

#endif

#ifdef MM_RND_IMPLEMENTATION
#undef MM_RND_IMPLEMENTATION

void mmrnd_init(mmrnd_context *context) {
  mm_u8 *reg = context->reg;
  for (mm_i8 i = 4; i; --i) {
    *(reg + (i-1)) = 0x0;
  }
}

mm_u16 mmrnd_number(mmrnd_context *context) {
  mm_u8 *reg = context->reg;
  for (mm_i8 i = 2; i; --i) {
    *(reg + 0) = 5 * *(reg + 0) + 1;
    *(reg + 1) = ((*(reg + 1) & 0x80) == (*(reg + 1) & 0x10)) ?
      2 * *(reg + 1) + 1 : 2 * *(reg + 1);
    *(reg + 2 + (i-1)) = (*(reg + 0) ^ *(reg + 1));
  }
  return ((mm_u16) *(reg + 2) << 0x8) | *(reg + 3);
}

#endif