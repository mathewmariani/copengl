#ifndef MM_LIST_H
#define MM_LIST_H

typedef struct mm_list_node mm_list_node;

struct mm_list_node {
  int val;
  mm_list_node *next;
};

#endif

#ifdef MM_LIST_IMPLEMENTATION
#undef MM_LIST_IMPLEMENTATION

/* body */

#endif