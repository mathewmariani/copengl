#ifndef MM_OPENGL_H
#define MM_OPENGL_H

void mmopengl_init_context();
void mmopengl_init_opengl_functions();

#endif

#ifdef MM_OPENGL_IMPLEMENTATION
#undef MM_OPENGL_IMPLEMENTATION

void mmopengl_init_context() {
  /* body */
}

void mmopengl_init_opengl_functions() {
  /* body */
}

#endif