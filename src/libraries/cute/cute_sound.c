#include "common/platform.h"

#ifdef AUDIO_USE_CUTE
  #include "libraries/stb/stb_vorbis.h"

  // #define CUTE_SOUND_FORCE_SDL
  #define CUTE_SOUND_IMPLEMENTATION
  #include "cute_sound.h"
#endif