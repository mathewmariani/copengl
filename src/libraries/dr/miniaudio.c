#include "common/platform.h"

#ifdef AUDIO_USE_MINIAUDIO
  #define MINIAUDIO_IMPLEMENTATION
  #include "miniaudio.h"
#endif