/* std */
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

/* engine */
#include "common/version.h"
#include "engine.h"

int main(int argc, char **argv) {
  printf("lite version: %s (%d)\n", VERSION_STRING, VERSION_NUMBER);

  engine_t *engine = engine_create(&(engine_desc) {
    .title = "lite",
    .width = 0,
    .height = 0,
    .argv = argc > 1 ? argv[1] : NULL,
  });

  if (!engine) {
    return EXIT_FAILURE;
  }

  engine_run(engine);
  engine_destroy(engine);

  return EXIT_SUCCESS;
}
