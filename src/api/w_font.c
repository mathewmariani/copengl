/* std */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* engine */
#include "engine.h"
#include "font.h"
#include "filesystem.h"
#include "common/runtime.h"

static void w_font_allocate(WrenVM *vm) {
  ttf_t *self = (ttf_t *) wrenSetSlotNewForeign(vm, 0, 0, sizeof(ttf_t));

  const char *filename = wrenGetSlotString(vm, 1);
  int ptsize = (int) wrenGetSlotDouble(vm, 2);

  /* filesystem module */
  filesystem_t *fs = (filesystem_t *) ((engine_t *) wrenGetUserData(vm))->filesystem;

  /* load file data */
  size_t size;
  void *filedata = filesystem_read(fs, filename, &size);
  if (!filedata) {
    printf("could not read file\n");
    return;
  }

  /* copy filedata */
  // self->data = malloc(size);
  // memcpy(self->data, filedata, size);

  ttf_init(self, ptsize);
  
  /* free resources */
  free(filedata);
}

static void w_font_finalize(void *bytes) {
  ttf_deinit((ttf_t *) bytes);
}

void w_font_getWidth(WrenVM *vm) {
  /* retrieve from wren */
  ttf_t *self = (ttf_t *) wrenGetSlotForeign(vm, 0);
  const char *str = wrenGetSlotString(vm, 1);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, ttf_width(self, str));
}

void w_font_getHeight(WrenVM *vm) {
  /* retrieve from wren */
  ttf_t *self = (ttf_t *) wrenGetSlotForeign(vm, 0);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, ttf_height(self));
}

const static function_desc functions[] = {
  /* properties */
  { false, "height", w_font_getHeight },

  /* functions */
  { false, "width(_)", w_font_getWidth },

  /* always last. */
  { false, NULL, NULL },
};

void wren_font(WrenVM *vm) {
  wren_register_class(vm, &(class_desc) {
    .name = "Font",
    .type = CLASS_FONT,
    .methods = {
      .allocate = w_font_allocate,
      .finalize = w_font_finalize,
    },
    .functions = functions,
  });
}
