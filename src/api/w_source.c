/* std */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* engine */
#include "common/platform.h"
#include "audio.h"
#include "source.h"
#include "common/runtime.h"

static void w_source_allocate(WrenVM *vm) {
  printf("%s\n", __FUNCTION__);
  source_t *self = (source_t*) wrenSetSlotNewForeign(vm, 0, 0, sizeof(source_t));

  const char *filename = wrenGetSlotString(vm, 1);
  source_init(self, filename);
}

static void w_source_finalize(void *bytes) {
  source_t *self = bytes;
  source_deinit(self);
}

void w_source_isPaused(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  float volume = (float) wrenGetSlotDouble(vm, 1);

  // FIXME: implement
  (void)self;
  (void)volume;
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, false);
}

void w_source_isPlaying(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  float volume = (float) wrenGetSlotDouble(vm, 1);

  // FIXME: implement
  (void)self;
  (void)volume;
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, false);
}

void w_source_isStopped(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  double volume = (float) wrenGetSlotDouble(vm, 1);

  // FIXME: implement
  (void)self;
  (void)volume;
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, false);
}

void w_source_getPitch(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  float pitch = (float) wrenGetSlotDouble(vm, 1);

  // FIXME: implement
  (void)pitch;
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, self->volume);
}

void w_source_setPitch(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  float pitch = (float) wrenGetSlotDouble(vm, 1);

  // FIXME: implement
  self->volume = pitch;
}

void w_source_getVolume(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);

  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, self->volume);
}

void w_source_setVolume(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  float volume = (float) wrenGetSlotDouble(vm, 1);

  self->volume = volume;
}

void w_source_pause(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  (void)self;
  // FIXME: implement
  // audio_play(self->data);
}

void w_source_play(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);

#ifdef AUDIO_USE_CUTE
  printf("[%s] info:\n", __FUNCTION__);
  printf("\tloaded  : %p\n", &(self->loaded));
  printf("\tsound   : %p\n", &(self->sound));
  printf("\tdef     : %p\n", &(self->def));
#endif
  (void)self;
}

void w_source_stop(WrenVM *vm) {
  source_t *self = (source_t *) wrenGetSlotForeign(vm, 0);
  (void)self;
}

const static function_desc functions[] = {
  /* properties */
  { false, "isPaused", w_source_isPaused },
  { false, "isPlaying", w_source_isPlaying },
  { false, "isStopped", w_source_isStopped },
  { false, "pitch", w_source_getPitch },
  { false, "pitch=(_)", w_source_setPitch },
  { false, "volume", w_source_getVolume },
  { false, "volume=(_)", w_source_setVolume },

  /* functions */
  { false, "pause()", w_source_pause },
  { false, "play()", w_source_play },
  { false, "stop()", w_source_stop },

  /* always last. */
  { false, NULL, NULL },
};

void wren_source(WrenVM *vm) {
  wren_register_class(vm, &(class_desc) {
    .name = "Source",
    .type = CLASS_SOURCE,
    .methods = {
      .allocate = w_source_allocate,
      .finalize = w_source_finalize,
    },
    .functions = functions,
  });
}