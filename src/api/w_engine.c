/* engine */
#include "common/runtime.h"

/* scripts */
#include "embedded/lite.wren.h"

/* forward declarations */
void wren_audio(WrenVM*);
void wren_filesystem(WrenVM*);
void wren_font(WrenVM*);
void wren_graphics(WrenVM*);
void wren_keyboard(WrenVM*);
void wren_mouse(WrenVM*);
void wren_random(WrenVM*);
void wren_source(WrenVM*);
void wren_timer(WrenVM*);
void wren_window(WrenVM*);

void wren_engine(WrenVM *vm) {
  wren_register_module(vm, &(module_desc) {
    .module = NULL,
    .name = "lite",
    .source = (const char *) lite_wren,
    .type = MODULE_LITE,
    .classes = NULL,
  });

  struct { char *name; void (*fn)(WrenVM *); } module[] = {
    { "audio", wren_audio },
    { "filesystem", wren_filesystem },
    { "font", wren_font },
    { "graphics", wren_graphics },
    { "keyboard", wren_keyboard },
    { "mouse", wren_mouse },
    { "random", wren_random },
    { "source", wren_source },
    { "timer", wren_timer },
    { "window", wren_window },
    { 0 },
  };

  for (int i = 0; module[i].name; ++i) {
    module[i].fn(vm);
  }
}
