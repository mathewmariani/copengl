/* engine */
#include "engine.h"
#include "common/runtime.h"

// #define self ((mouse_t *) ((engine_t *) wrenGetUserData(vm))->mouse)

/* FIXME: dirty implementation */
#define MM_RND_IMPLEMENTATION
#include "libraries/matty/mm_rnd.h"

static mmrnd_context context;

void w_random_number(WrenVM *vm) {
  /* generate random number */
  u16 number = (u16) mmrnd_number(&context);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, number);
}

void w_random_reset(WrenVM *vm) {
  mmrnd_init(&context);
}

const static function_desc functions[] = {
  /* properties */
  /* functions */
  { true, "number()", w_random_number },
  { true, "reset()", w_random_reset },

  /* always last. */
  { false, NULL, NULL },
};

void wren_random(WrenVM *vm) {
  /* initialize module */
  mmrnd_init(&context);
  // engine_t *app = (engine_t *) wrenGetUserData(vm);
  // app->random = mouse_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Random",
    .type = CLASS_RANDOM,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}