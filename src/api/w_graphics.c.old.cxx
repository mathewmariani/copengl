/*
class Graphics {
  foreign static width
  foreign static height
  
  foreign static background=( idx )
  foreign static color=( idx )
  foreign static layer=( idx )
  foreign static palette=( filename )
  foreign static pattern=( bstr )
  foreign static blend( idx, mode )
  foreign static clear()
  foreign static clear( idx )
  foreign static present()
  foreign static clip()
  foreign static clip( x, y, w, h )
  foreign static pal()
  foreign static pal( c0, c1 )

  foreign static spritesheet=( ptr )
  foreign static spritesheet

  foreign static rotate( r )
  foreign static scale( sx, sy )
  foreign static translate( x, y )

  foreign static circle( mode, x, y, r )
  foreign static line( x1, y1, x2, y2 )
  foreign static tline( x1, y1, x2, y2, sx, sy, sdx, sdy )
  foreign static point( x, y )
  foreign static rectangle( mode, x, y, w, h )
  foreign static print( font, str, x, y )

  foreign static pget( x, y )
  foreign static pset( x, y )
  foreign static pset( x, y, c )

  foreign static sget( x, y )
  foreign static sset( x, y, c )
  foreign static spr( sx, sy, sw, sh, dx, dy, flip_x, flip_y )
  foreign static sspr( sx, sy, sw, sh, dx, dy, dw, dh, flip_x, flip_y )

  static spr( src, x, y ) {
    Graphics.spr(src, x, y, false, false)
  }

  static spr( src, x, y, flip_x, flip_y ) {
    Graphics.spr(src.x, src.y, src.w, src.h, x, y, flip_x, flip_y)
  }

  static sspr( src, x, y ) {
    Graphics.sspr(src.x, src.y, src.w, src.h, x, y, 0, 0)
  }

  static sspr( src, dst ) {
    Graphics.sspr(src, dst, false, false)
  }

  static sspr( src, dst, flip_x, flip_y ) {
    Graphics.sspr(src.x, src.y, src.w, src.h, dst.x, dst.y, dst.w, dst.h, flip_x, flip_y)
  }

  static renderTo( idx, fn ) {
    Graphics.layer = idx
    fn.call()
  }
}
*/

/* std */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* engine */
#include "engine.h"
#include "graphics.h"
#include "common/runtime.h"
#include "window.h"
#include "common/platform.h"
#include "font.h"

#define self ((graphics_t *) ((engine_t *) wrenGetUserData(vm))->graphics)

void w_graphics_get_width(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_width(self));
}

void w_graphics_get_height(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_height(self));
}

void w_graphics_get_spritesheet(WrenVM *vm) {
  /* return to wren */
  // wrenEnsureSlots(vm, 1);
  // wrenSetSlotForeign(vm, 0, spritesheet_get_height(self));
}

void w_graphics_set_spritesheet(WrenVM *vm) {
  /* retrieve from wren */
  spritesheet_t *spritesheet = (spritesheet_t *) wrenGetSlotForeign(vm, 1);
  graphics_set_spritesheet(self, spritesheet);
}

void w_graphics_set_background(WrenVM *vm) {
  /* retrieve from wren */
  u16 bstr = (u16) wrenGetSlotDouble(vm, 1);
  graphics_set_pattern(self, bstr);
}

void w_graphics_set_color(WrenVM *vm) {
  /* retrieve from wren */
  u16 color = (u16) wrenGetSlotDouble(vm, 1);
  graphics_set_color(self, color);
}

void w_graphics_set_layer(WrenVM *vm) {
  /* retrieve from wren */
  u16 layer_index = (u16) wrenGetSlotDouble(vm, 1);

  UNUSED(layer_index);
  STUBBED("haven't implemented layers.");
}

void w_graphics_set_palette(WrenVM *vm) {
  /* retrieve from wren */
  int count = wrenGetListCount(vm, 1);

  if (count > 256) {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
    return;
  }

  palette_t *palette = graphics_get_palette(self);
  for (int i = 0; i < count; i++) {
    wrenGetListElement(vm, 1, i, 0);
    u32 color = (u32) wrenGetSlotDouble(vm, 0);
    palette_add(palette, color);
  }

  /* sends palette to the shader */
  graphics_set_palette(self);
}

void w_graphics_set_pattern(WrenVM *vm) {
  /* retrieve from wren */
  u16 bstr = (u16) wrenGetSlotDouble(vm, 1);
  graphics_set_pattern(self, bstr);
}

void w_graphics_set_clip(WrenVM *vm) {
  i16 x1, y1, x2, y2;

  int argc = wrenGetSlotCount(vm);
  if (argc > 0) {
    x1 = (i16) wrenGetSlotDouble(vm, 1);
    y1 = (i16) wrenGetSlotDouble(vm, 2);
    x2 = (i16) wrenGetSlotDouble(vm, 3);
    y2 = (i16) wrenGetSlotDouble(vm, 4);
  } else {
    x1 = 0;
    y1 = 0;
    x2 = graphics_get_width(self);
    y2 = graphics_get_height(self);
  }

  graphics_set_clip(self, x1, y1, x2, y2);
}

void w_graphics_pal(WrenVM *vm) {
  /* retrieve from wren */
  int argc = wrenGetSlotCount(vm);
  if (argc > 1) {
    u8 c0 = (u8) wrenGetSlotDouble(vm, 1);
    u8 c1 = (u8) wrenGetSlotDouble(vm, 2);

    graphics_pal(self, c0, c1);
  } else {
    graphics_reset_pal(self);
  }
}

void w_graphics_set_rotation(WrenVM *vm) {
  /* retrieve from wren */
  i16 r = (i16) wrenGetSlotDouble(vm, 1);

  graphics_set_rotation(self, r);
}

void w_graphics_set_scale(WrenVM *vm) {
  /* retrieve from wren */
  i16 sx = (i16) wrenGetSlotDouble(vm, 1);
  i16 sy = (i16) wrenGetSlotDouble(vm, 2);

  graphics_set_scale(self, sx, sy);
}

void w_graphics_set_translation(WrenVM *vm) {
  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);

  graphics_set_translation(self, x, y);
}

void w_graphics_blend(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);

  if (!strcmp(mode, "replace")) {
  } else if (!strcmp(mode, "add")) {
  } else if (!strcmp(mode, "average")) {
  } else if (!strcmp(mode, "subtract")) {
  } else {
    /* FIXME: this should crash as a runtime error. */
    wrenSetSlotString(vm, 0, "Error: Unknown BlendMode");
    wrenAbortFiber(vm, 0);
  }

  STUBBED("haven't implemented blend modes.");
}

void w_graphics_clear(WrenVM *vm) {
  u8 color_idx = 0;

  int argc = wrenGetSlotCount(vm);
  if (argc > 1) {
    color_idx = (u8) wrenGetSlotDouble(vm, 1);
  } else {
    color_idx = graphics_get_background(self);
  }

  graphics_clear(self, color_idx);
}

void w_graphics_print(WrenVM *vm) {
  /* retrieve from wren */
  ttf_t *ttf = (ttf_t *) wrenGetSlotForeign(vm, 1);
  const char *str = wrenGetSlotString(vm, 2);
  i16 x = (i16) wrenGetSlotDouble(vm, 3);
  i16 y = (i16) wrenGetSlotDouble(vm, 4);

  graphics_apply_transformation(self, &x, &y);
  
  graphics_blit_font(self, ttf, str, x, y);
}

void w_graphics_present(WrenVM *vm) {
  graphics_present(self);

  /* always last. */
  window_t *window = ((engine_t *) wrenGetUserData(vm))->window;
  window_swap_buffers(window);
}

void w_graphics_circle(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);
  i16 x = (i16) wrenGetSlotDouble(vm, 2);
  i16 y = (i16) wrenGetSlotDouble(vm, 3);
  i16 r = (i16) wrenGetSlotDouble(vm, 4);

  graphics_apply_transformation(self, &x, &y);

  bool fill = false;
  if (!strcmp(mode, "fill")) {
    fill = true;
  } else if (!strcmp(mode, "line")) {
    fill = false;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
  }

  if (fill) {
    graphics_circfill(self, x, y, r);
  } else {
    graphics_circ(self, x, y, r);
  }
}

void w_graphics_line(WrenVM *vm) {
  /* retrieve from wren */
  i16 x1 = (i16) wrenGetSlotDouble(vm, 1);
  i16 y1 = (i16) wrenGetSlotDouble(vm, 2);
  i16 x2 = (i16) wrenGetSlotDouble(vm, 3);
  i16 y2 = (i16) wrenGetSlotDouble(vm, 4);

  graphics_apply_transformation(self, &x1, &y1);
  graphics_apply_transformation(self, &x2, &y2);

  graphics_line(self, x1, y1, x2, y2);
}

void w_graphics_tline(WrenVM *vm) {
  /* retrieve from wren */
  i16 x1 = (i16) wrenGetSlotDouble(vm, 1);
  i16 y1 = (i16) wrenGetSlotDouble(vm, 2);
  i16 x2 = (i16) wrenGetSlotDouble(vm, 3);
  i16 y2 = (i16) wrenGetSlotDouble(vm, 4);
  i16 sx = (i16) wrenGetSlotDouble(vm, 5);
  i16 sy = (i16) wrenGetSlotDouble(vm, 6);
  i16 sdx = (i16) wrenGetSlotDouble(vm, 7);
  i16 sdy = (i16) wrenGetSlotDouble(vm, 8);

  graphics_apply_transformation(self, &x1, &y1);
  graphics_apply_transformation(self, &x2, &y2);

  graphics_tline(self, x1, y1, x2, y2, sx, sy, sdx, sdy);
}

void w_graphics_point(WrenVM *vm) {
  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);

  graphics_apply_transformation(self, &x, &y);

  graphics_point(self, x, y);
}

void w_graphics_rectangle(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);
  i16 x = (i16) wrenGetSlotDouble(vm, 2);
  i16 y = (i16) wrenGetSlotDouble(vm, 3);
  i16 w = (i16) wrenGetSlotDouble(vm, 4);
  i16 h = (i16) wrenGetSlotDouble(vm, 5);

  graphics_apply_transformation(self, &x, &y);

  bool fill = false;
  if (!strcmp(mode, "fill")) {
    fill = true;
  } else if (!strcmp(mode, "line")) {
    fill = false;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
    return;
  }

  if (fill) {
    graphics_rectfill(self, x, y, w, h);  
  } else {
    graphics_rect(self, x, y, w, h);
  }
}

void w_graphics_pget(WrenVM *vm) {
  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_pget(self, x, y));
}

void w_graphics_pset(WrenVM *vm) {
  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);
  // uint8_t c = (uint8_t) wrenGetSlotDouble(vm, 3);

  UNUSED(x);
  UNUSED(y);
  STUBBED("Confused about graphics APIs.");
  // graphics_pset(self, x, y, i);
}

/* 
 * sget( x, y )
 * Gets the color value of a pixel on the sprite sheet.
 */
void w_graphics_sget(WrenVM *vm) {
  spritesheet_t *spritesheet = graphics_get_spritesheet(self);
  if (!spritesheet) {
    wrenSetSlotString(vm, 0, "Error: Spritesheet is NULL");
    wrenAbortFiber(vm, 0);
    return;
  }

  printf("DO WE MAKE IT HERE?!\n");

  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);

  /* get the color value of the pixel at (x, y) */
  u8 c = spritesheet_get(spritesheet, x, y);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, c);
}

void w_graphics_sset(WrenVM *vm) {
  spritesheet_t *spritesheet = graphics_get_spritesheet(self);
  if (!spritesheet) {
    wrenSetSlotString(vm, 0, "Error: Spritesheet is NULL");
    wrenAbortFiber(vm, 0);
    return;
  }

  /* retrieve from wren */
  i16 x = (i16) wrenGetSlotDouble(vm, 1);
  i16 y = (i16) wrenGetSlotDouble(vm, 2);
  u8 c = (u8) wrenGetSlotDouble(vm, 3);

  spritesheet_set(spritesheet, x, y, c);
}

/*
 * spr( sx, sy, sw, sh, dx, dy, flip_x, flip_y )
 *
 * Draws a sprite on the screen.
 */
void w_graphics_spr(WrenVM *vm) {
  spritesheet_t *spritesheet = graphics_get_spritesheet(self);
  if (!spritesheet) {
    wrenSetSlotString(vm, 0, "Error: Spritesheet is NULL");
    wrenAbortFiber(vm, 0);
    return;
  }

  /* retrieve from wren */
  i16 sx = (i16) wrenGetSlotDouble(vm, 1);
  i16 sy = (i16) wrenGetSlotDouble(vm, 2);
  i16 sw = (i16) wrenGetSlotDouble(vm, 3);
  i16 sh = (i16) wrenGetSlotDouble(vm, 4);
  i16 dx = (i16) wrenGetSlotDouble(vm, 5);
  i16 dy = (i16) wrenGetSlotDouble(vm, 6);
  bool flip_x = wrenGetSlotBool(vm, 7);
  bool flip_y = wrenGetSlotBool(vm, 8);

  graphics_apply_transformation(self, &dx, &dy);

  /* source */
  rect_t src = (rect_t) {
    .x = sx, .y = sy,
    .w = sw, .h = sh,
  };

  /* destination */
  rect_t dst = (rect_t) {
    .x = dx, .y = dy,
    .w = 0, .h = 0,
  };

  graphics_blit_spritesheet_test(self, &src, &dst, flip_x, flip_y);
}

/* 
 * sspr( sx, sy, sw, sh, dx, dy )
 *
 * Draws a rectangle of pixels from the sprite sheet.
 */
void w_graphics_sspr(WrenVM *vm) {
  spritesheet_t *spritesheet = graphics_get_spritesheet(self);
  if (!spritesheet) {
    wrenSetSlotString(vm, 0, "Error: Spritesheet is NULL");
    wrenAbortFiber(vm, 0);
    return;
  }

  /* retrieve from wren */
  i16 sx = (i16) wrenGetSlotDouble(vm, 1);
  i16 sy = (i16) wrenGetSlotDouble(vm, 2);
  i16 sw = (i16) wrenGetSlotDouble(vm, 3);
  i16 sh = (i16) wrenGetSlotDouble(vm, 4);
  i16 dx = (i16) wrenGetSlotDouble(vm, 5);
  i16 dy = (i16) wrenGetSlotDouble(vm, 6);
  i16 dw = (i16) wrenGetSlotDouble(vm, 7);
  i16 dh = (i16) wrenGetSlotDouble(vm, 8);
  bool flip_x = wrenGetSlotBool(vm, 9);
  bool flip_y = wrenGetSlotBool(vm, 10);

  graphics_apply_transformation(self, &dx, &dy);

  /* source */
  rect_t src = (rect_t) {
    .x = sx, .y = sy,
    .w = sw, .h = sh,
  };

  /* destination */
  rect_t dst = (rect_t) {
    .x = dx, .y = dy,
    .w = dw, .h = dh,
  };

  graphics_blit_spritesheet_test(self, &src, &dst, flip_x, flip_y);
}

const static function_desc functions[] = {
  /* properties */
  { true, "width", w_graphics_get_width },
  { true, "height", w_graphics_get_height },

  /* test */
  { true, "spritesheet", w_graphics_get_spritesheet },
  { true, "spritesheet=(_)", w_graphics_set_spritesheet },

  /* state */
  { true, "background=(_)", w_graphics_set_background },
  { true, "color=(_)", w_graphics_set_color },
  { true, "layer=(_)", w_graphics_set_layer },
  { true, "palette=(_)", w_graphics_set_palette },
  { true, "pattern=(_)", w_graphics_set_pattern },
  { true, "clip()", w_graphics_set_clip },
  { true, "clip(_,_,_,_)", w_graphics_set_clip },
  { true, "pal()", w_graphics_pal },
  { true, "pal(_,_)", w_graphics_pal },

  /* transform */
  { true, "rotate(_)", w_graphics_set_rotation },
  { true, "scale(_,_)", w_graphics_set_scale },
  { true, "translate(_,_)", w_graphics_set_translation },

  /* drawing */
  { true, "blend(_,_)", w_graphics_blend },
  { true, "clear()", w_graphics_clear },
  { true, "clear(_)", w_graphics_clear },
  { true, "print(_,_,_,_)", w_graphics_print },
  { true, "present()", w_graphics_present },

  /* primitives */
  { true, "circle(_,_,_,_)", w_graphics_circle },
  { true, "line(_,_,_,_)", w_graphics_line },
  { true, "tline(_,_,_,_,_,_,_,_)", w_graphics_tline },
  { true, "point(_,_)", w_graphics_point },
  { true, "rectangle(_,_,_,_,_)", w_graphics_rectangle },

  /* stuff */
  { true, "pget(_,_)", w_graphics_pget },
  { true, "pset(_,_)", w_graphics_pset },
  { true, "pset(_,_,_)", w_graphics_pset },

  /* spritesheet */
  { true, "sget(_,_)", w_graphics_sget },
  { true, "sset(_,_,_)", w_graphics_sset },
  { true, "spr(_,_,_,_,_,_,_,_)", w_graphics_spr },
  { true, "sspr(_,_,_,_,_,_,_,_,_,_)", w_graphics_sspr },

  /* always last. */
  { false, NULL, NULL },
};

void wren_graphics(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->graphics = graphics_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Graphics",
    .type = CLASS_GRAPHICS,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
