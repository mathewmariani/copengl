/* engine */
#include "engine.h"
#include "filesystem.h"
#include "common/runtime.h"

#define self ((filesystem_t *) ((engine_t *) wrenGetUserData(vm))->filesystem)

void w_filesystem_exists(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, filesystem_exists(self, filename));
}

void w_filesystem_is_directory(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, filesystem_is_directory(self, filename));
}

void w_filesystem_is_file(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, filesystem_is_file(self, filename));
}

void w_filesystem_read(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  wrenEnsureSlots(vm, 1);
  wrenSetSlotString(vm, 0, (char*) filesystem_read(self, filename, NULL));
}

void w_filesystem_write(WrenVM *vm) {
  const char *filename = wrenGetSlotString(vm, 1);
  const char *data = wrenGetSlotString(vm, 2);
  wrenEnsureSlots(vm, 1);
  wrenSetSlotString(vm, 0, (char*) filesystem_write(self, filename, data));
}

const static function_desc functions[] = {
  /* properties */
  /* functions */
  { true, "exists(_)", w_filesystem_exists },
  { true, "isFile(_)", w_filesystem_is_file },
  { true, "isDirectory(_)", w_filesystem_is_directory },
  { true, "read(_)", w_filesystem_read },
  { true, "write(_,_)", w_filesystem_write },

  /* always last. */
  { false, NULL, NULL },
};

void wren_filesystem(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->filesystem = filesystem_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Filesystem",
    .type = CLASS_FILESYSTEM,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
