/* std */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* engine */
#include "engine.h"
#include "window.h"
#include "graphics.h"
#include "common/runtime.h"

#define self ((window_t *) ((engine_t *) wrenGetUserData(vm))->window)

void w_window_get_width(WrenVM *vm) {
  int width;
  window_size(self, &width, NULL);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, width);
}

void w_window_get_height(WrenVM *vm) {
  int height;
  window_size(self, NULL, &height);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, height);
}

void w_window_set_fullscreen(WrenVM *vm) {
//  // ASSERT_SLOT_TYPE(vm, 1, string)
//  ASSERT_SLOT_TYPE(vm, 1, BOOL, enable)
//
//  /* retrieve from wren */
//  bool enable = wrenGetSlotBool(vm, 1);
//  window_setFullscreen(enable);
}

void w_window_set_title(WrenVM *vm) {
  /* retrieve from wren */
  const char *title = wrenGetSlotString(vm, 1);
  window_set_title(self, title);
}

void w_window_set_mode(WrenVM *vm) {
  /* retrieve from wren */
  int width = (int) wrenGetSlotDouble(vm, 1);
  int height = (int) wrenGetSlotDouble(vm, 2);

  // bool created = window_set_mode(self, width, height);

  /* initializes opengl */
  // graphics_t *graphics = ((engine_t *) wrenGetUserData(vm))->graphics;
  // graphics_set_mode(graphics, width, height);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, true);
}

const static function_desc functions[] = {
  /* properties */
  { true, "width", w_window_get_width },
  { true, "height", w_window_get_height },
  { true, "fullscreen=(_)", w_window_set_fullscreen },
  { true, "title=(_)", w_window_set_title },

  /* functions */
  { true, "setMode(_,_)", w_window_set_mode },

  /* always last. */
  { false, NULL, NULL },
};

void wren_window(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->window = window_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Window",
    .type = CLASS_WINDOW,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
