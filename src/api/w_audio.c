/* std */
#include <stdio.h>

/* engine */
#include "audio.h"
#include "common/runtime.h"

void w_audio_getVolume(WrenVM *vm) {
  printf("%s\n", __FUNCTION__);
}

void w_audio_setVolume(WrenVM *vm) {
  printf("%s\n", __FUNCTION__);
}

const static function_desc functions[] = {
  /* properties */
  { true, "volume", w_audio_setVolume },
  { true, "volume=(_)", w_audio_getVolume },

  /* always last. */
  { false, NULL, NULL },
};

void wren_audio(WrenVM *vm) {
  wren_register_class(vm, &(class_desc) {
    .name = "Audio",
    .type = CLASS_AUDIO,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}