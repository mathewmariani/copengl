/* std */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* engine */
#include "engine.h"
#include "graphics.h"
#include "common/runtime.h"
#include "window.h"
#include "common/platform.h"
#include "font.h"

#define self ((graphics_t *) ((engine_t *) wrenGetUserData(vm))->graphics)

void w_graphics_get_width(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_width(self));
}

void w_graphics_get_height(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_height(self));
}

void w_graphics_get_background(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_background(self));
}

void w_graphics_set_background(WrenVM *vm) {
  /* retrieve from wren */
  u16 color = (u16) wrenGetSlotDouble(vm, 1);
  graphics_set_background(self, color);
}

void w_graphics_get_color(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_color(self));
}

void w_graphics_set_color(WrenVM *vm) {
  /* retrieve from wren */
  u16 color = (u16) wrenGetSlotDouble(vm, 1);

  /* seperate into high and low colors */
  const u8 c0 = (color)      & 0xFF;
  const u8 c1 = (color >> 8) & 0xFF;

  graphics_set_color(self, c0, c1);
}

void w_graphics_get_palette(WrenVM *vm) {
  /* body */
}

void w_graphics_set_palette(WrenVM *vm) {
  ///* retrieve from wren */
  //int count = wrenGetListCount(vm, 1);

  //if (count > 256) {
  //  // FIXME: this should crash as a runtime error IMO
  //  wrenSetSlotString(vm, 0, "Error: Palette too large.");
  //  wrenAbortFiber(vm, 0);
  //  return;
  //}

  //for (int i = 0; i < count; i++) {
  //  /* retrieve from wren */
  //  wrenGetListElement(vm, 1, i, 0);
  //  u32 color = (u32) wrenGetSlotDouble(vm, 0);

  //  graphics_add_color(self, &(colorf_t) {
  //    .r = ((color >> 24) & 0xFF) / 255.0f,
  //    .g = ((color >> 16) & 0xFF) / 255.0f,
  //    .b = ((color >>  8) & 0xFF) / 255.0f,
  //    .a = ((color >>  0) & 0xFF) / 255.0f,
  //  });
  //}
}

void w_graphics_get_pattern(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, graphics_get_pattern(self));
}

void w_graphics_set_pattern(WrenVM *vm) {
  /* retrieve from wren */
  u16 bstr = (u16) wrenGetSlotDouble(vm, 1);
  graphics_set_pattern(self, bstr);
}

void w_graphics_set_clipping(WrenVM *vm) {
  i32 x1, y1, x2, y2;

  /* retrieve from wren */
  int argc = wrenGetSlotCount(vm);
  if (argc > 1) {
    x1 = (i32) wrenGetSlotDouble(vm, 1);
    y1 = (i32) wrenGetSlotDouble(vm, 2);
    x2 = (i32) wrenGetSlotDouble(vm, 3);
    y2 = (i32) wrenGetSlotDouble(vm, 4);
  } else {
    x1 = 0;
    y1 = 0;
    x2 = graphics_get_width(self);
    y2 = graphics_get_height(self);
  }

  graphics_set_clipping(self, x1, y1, x2, y2);
}

void w_graphics_circle(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);
  i16 x = (i16) wrenGetSlotDouble(vm, 2);
  i16 y = (i16) wrenGetSlotDouble(vm, 3);
  i16 r = (i16) wrenGetSlotDouble(vm, 4);

  bool fill = false;
  if (!strcmp(mode, "fill")) {
    fill = true;
  } else if (!strcmp(mode, "line")) {
    fill = false;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
  }

  if (fill) {
    graphics_circfill(self, x, y, r);
  } else {
    graphics_circ(self, x, y, r);
  }
}

void w_graphics_clear(WrenVM *vm) {
  graphics_clear(self);
}

void w_graphics_ellipse(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);
  i32 x = (i32) wrenGetSlotDouble(vm, 2);
  i32 y = (i32) wrenGetSlotDouble(vm, 3);
  i32 r1 = (i32) wrenGetSlotDouble(vm, 4);
  i32 r2 = (i32) wrenGetSlotDouble(vm, 5);

  bool fill = false;
  if (!strcmp(mode, "fill")) {
    fill = true;
  } else if (!strcmp(mode, "line")) {
    fill = false;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
  }

  if (fill) {
    graphics_ellipsefill(self, x, y, r1, r2);
  } else {
    graphics_ellipse(self, x, y, r1, r2);
  }
}

void w_graphics_line(WrenVM *vm) {
  /* retrieve from wren */
  i32 x1 = (i32) wrenGetSlotDouble(vm, 1);
  i32 y1 = (i32) wrenGetSlotDouble(vm, 2);
  i32 x2 = (i32) wrenGetSlotDouble(vm, 3);
  i32 y2 = (i32) wrenGetSlotDouble(vm, 4);

  graphics_line(self, x1, y1, x2, y2);

  // /* retrieve from wren */
  // int count = wrenGetListCount(vm, 1);
  // if (count % 4 != 0) {
  //   // FIXME: this should crash as a runtime error IMO
  //   wrenSetSlotString(vm, 0, "Error: this points error happened");
  //   wrenAbortFiber(vm, 0);
  //   return;
  // }

  // /* ensure we have four scratch slots for (x, y) pairs in the list */
  // /* 0. module */
  // /* 1. list */
  // /* 2. number (x1) */
  // /* 3. number (y1) */
  // /* 5. number (y2) */
  // /* 6. number (y2) */
  // wrenEnsureSlots(vm, 6);

  // u8 c = graphics_get_color(self);
  // u16 x1, y1, x2, y2;
  // for (int i = 0; i < count; i += 2) {
  //   /* retrieve from wren */
  //   wrenGetListElement(vm, 1, i+0, 2);
  //   wrenGetListElement(vm, 1, i+1, 3);
  //   wrenGetListElement(vm, 1, i+1, 4);
  //   wrenGetListElement(vm, 1, i+1, 5);

  //   u16 x1 = (i16) wrenGetSlotDouble(vm, 2);
  //   u16 y1 = (i16) wrenGetSlotDouble(vm, 3);
  //   u16 x2 = (i16) wrenGetSlotDouble(vm, 4);
  //   u16 y2 = (i16) wrenGetSlotDouble(vm, 5);

  //   graphics_set_pixel(self, x, y, c);
  // } 
}

void w_graphics_pal(WrenVM *vm) {
  /* retrieve from wren */
  int argc = wrenGetSlotCount(vm);
  if (argc > 1) {
    u8 c0 = (u8) wrenGetSlotDouble(vm, 1);
    u8 c1 = (u8) wrenGetSlotDouble(vm, 2);

    graphics_pal(self, c0, c1, false);
  } else {
    graphics_pal(self, 0, 0, true);
  }
}

void w_graphics_points(WrenVM *vm) {
  /* retrieve from wren */
  int count = wrenGetListCount(vm, 1);
  if (count % 2 != 0) {
    // FIXME: this should crash as a runtime error IMO
    printf("are we crashing here?\n");
    wrenSetSlotString(vm, 0, "Error: this points error happened");
    wrenAbortFiber(vm, 0);
    return;
  }

  /* ensure we have four scratch slots for (x, y) pairs in the list */
  /* 0. module */
  /* 1. list */
  /* 2. number (x) */
  /* 3. number (y) */
  wrenEnsureSlots(vm, 4);

  i32 *buff = malloc(count * sizeof(i32));
  u8 c = graphics_get_color(self);
  i32 x, y;
  for (int i = 0; i < count; i += 2) {
    /* retrieve from wren */
    wrenGetListElement(vm, 1, i+0, 2);
    wrenGetListElement(vm, 1, i+1, 3);

    //x = (i32) wrenGetSlotDouble(vm, 2);
    //y = (i32) wrenGetSlotDouble(vm, 3);

	*(buff + i + 0) = (i32) wrenGetSlotDouble(vm, 2);
	*(buff + i + 1) = (i32) wrenGetSlotDouble(vm, 3);

    //graphics_set_pixel(self, x, y, c);
  } 

  graphics_points(self, buff, count);
}

void w_graphics_present(WrenVM *vm) {
  graphics_present(self);

  /* always last. */
  window_t *window = ((engine_t *) wrenGetUserData(vm))->window;
  window_swap_buffers(window);
}

void w_graphics_rectangle(WrenVM *vm) {
  /* retrieve from wren */
  const char *mode = wrenGetSlotString(vm, 1);
  i32 x = (i32) wrenGetSlotDouble(vm, 2);
  i32 y = (i32) wrenGetSlotDouble(vm, 3);
  i32 w = (i32) wrenGetSlotDouble(vm, 4);
  i32 h = (i32) wrenGetSlotDouble(vm, 5);

  bool fill = false;
  if (!strcmp(mode, "fill")) {
    fill = true;
  } else if (!strcmp(mode, "line")) {
    fill = false;
  } else {
    // FIXME: this should crash as a runtime error IMO
    wrenSetSlotString(vm, 0, "Error: Unknown FillMode");
    wrenAbortFiber(vm, 0);
    return;
  }

  if (fill) {
    graphics_rectfill(self, x, y, w, h);  
  } else {
    graphics_rect(self, x, y, w, h);
  }
}

void w_graphics_pget(WrenVM *vm) {
  /* retrieve from wren */
  i32 x = (i32) wrenGetSlotDouble(vm, 1);
  i32 y = (i32) wrenGetSlotDouble(vm, 2);

  UNUSED(x);
  UNUSED(y);

  /* return to wren */
  // wrenEnsureSlots(vm, 1);
  // wrenSetSlotDouble(vm, 0, graphics_get_pixel(self, x, y));
}

void w_graphics_pset(WrenVM *vm) {
  /* retrieve from wren */
  i32 x = (i32) wrenGetSlotDouble(vm, 1);
  i32 y = (i32) wrenGetSlotDouble(vm, 2);
  u16 c = (u16) wrenGetSlotDouble(vm, 3);

  int argc = wrenGetSlotCount(vm);
  //if (argc == 4) {
  //  c = (u8) wrenGetSlotDouble(vm, 3);
  //} else {
  //  c = graphics_get_color(self);
  //}

  if (argc > 3) {
    // graphics_set_pixel_color(self, x, y, c);
    graphics_set_pixel(self, x, y);
  } else {
    graphics_set_pixel(self, x, y);
  }
}

const static function_desc functions[] = {
  /* properties */
  { true, "width", w_graphics_get_width },
  { true, "height", w_graphics_get_height },

  /* state */
  { true, "background", w_graphics_get_background },
  { true, "background=(_)", w_graphics_set_background },
  { true, "color", w_graphics_get_color },
  { true, "color=(_)", w_graphics_set_color },
  { true, "palette", w_graphics_get_palette },
  { true, "palette=(_)", w_graphics_set_palette },
  { true, "pattern", w_graphics_get_pattern },
  { true, "pattern=(_)", w_graphics_set_pattern },

  { true, "clip()", w_graphics_set_clipping },
  { true, "clip(_,_,_,_)", w_graphics_set_clipping },

  /* drawing */
  { true, "circle(_,_,_,_)", w_graphics_circle },
  { true, "clear()", w_graphics_clear },
  { true, "ellipse(_,_,_,_,_)", w_graphics_ellipse },
  { true, "line(_,_,_,_)", w_graphics_line },
  { true, "pal()", w_graphics_pal },
  { true, "pal(_,_)", w_graphics_pal },
  { true, "points(_)", w_graphics_points },
  { true, "present()", w_graphics_present },
  { true, "rectangle(_,_,_,_,_)", w_graphics_rectangle },

  /* where? probably wont ever fit in */
  { true, "pget(_,_)", w_graphics_pget },
  { true, "pset(_,_)", w_graphics_pset },
  { true, "pset(_,_,_)", w_graphics_pset },

  /* always last. */
  { false, NULL, NULL },
};

void wren_graphics(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->graphics = graphics_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Graphics",
    .type = CLASS_GRAPHICS,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
