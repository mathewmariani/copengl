/* engine */
#include "engine.h"
#include "mouse.h"
#include "common/runtime.h"

#define self ((mouse_t *) ((engine_t *) wrenGetUserData(vm))->mouse)

void w_mouse_get_x(WrenVM *vm) {
  int x = mouse_get_x(self);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, x);
}

void w_mouse_get_y(WrenVM *vm) {
  int y = mouse_get_y(self);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, y);
}

void w_mouse_get_xrel(WrenVM *vm) {
  int xrel = mouse_get_xrel(self);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, xrel);
}

void w_mouse_get_yrel(WrenVM *vm) {
  int yrel = mouse_get_yrel(self);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, yrel);
}

void w_mouse_is_down(WrenVM *vm) {
  /* retrieve from wren */
  int button = (int) wrenGetSlotDouble(vm, 1);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, mouse_is_down(self, button));
}

const static function_desc functions[] = {
  /* properties */
  { true, "x", w_mouse_get_x },
  { true, "y", w_mouse_get_y },
  { true, "xrel", w_mouse_get_xrel },
  { true, "yrel", w_mouse_get_yrel },

  /* functions */
  { true, "isDown(_)", w_mouse_is_down },  

  /* always last. */
  { false, NULL, NULL },
};

void wren_mouse(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->mouse = mouse_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Mouse",
    .type = CLASS_MOUSE,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}