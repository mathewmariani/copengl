/* std */
#include <stdio.h>

/* engine */
#include "engine.h"
#include "graphics.h"
#include "buffer.h"
#include "common/runtime.h"
#include "filesystem.h"
#include "spritesheet.h"

#include "common/config.h"

/* libs */
#include "libraries/stb/stb_image.h"

static inline u32 convert_to_rgba(u32 abgr) {
  return ((abgr & COLOR_A) >> 24) | /* ______AA */
         ((abgr & COLOR_R) >>  8) | /* ____RR__ */
         ((abgr & COLOR_G) <<  8) | /* __GG____ */
         ((abgr & COLOR_B) << 24);  /* BB______ */
}

static buffer_t *get_image_data(WrenVM *vm, int idx) {
  /* retrieve from wren */
  const char *filename = wrenGetSlotString(vm, idx);

  /* filesystem module */
  filesystem_t *fs = (filesystem_t *) ((engine_t *) wrenGetUserData(vm))->filesystem;
  
  /* load file data */
  size_t filesize;
  void *filedata = filesystem_read(fs, filename, &filesize);
  if (!filedata) {
    wrenSetSlotString(vm, 0, "Error: Could not load file.");
    wrenAbortFiber(vm, 0);
    return NULL;
  }

  /* load image data */
  int width, height;
  u32 *image_data = (u32 *) stbi_load_from_memory(
    (const stbi_uc *) filedata, filesize, &width, &height, NULL, STBI_rgb_alpha
  );

  if (!image_data) {
    wrenSetSlotString(vm, 0, stbi_failure_reason());
    wrenAbortFiber(vm, 0);
    return NULL;
  }

  /* graphics module */
  graphics_t *gfx = (graphics_t *) ((engine_t *) wrenGetUserData(vm))->graphics;
  palette_t *palette = graphics_get_palette(gfx);

  /* allocates memory */
  buffer_t *buffer = buffer_create(width, height);

  /* convert colors to indicies */
  for (int i = 0; i < (width * height); i++) {
    u32 rgba = convert_to_rgba(*(image_data + i));
    u8 idx = palette_find(palette, rgba);
    *(buffer->data + i) = idx;
  }

  /* cleanup */
  free(filedata);

  return buffer;
}

static void w_spritesheet_allocate(WrenVM *vm) {
  /* retrieve from wren */
  spritesheet_t *self = (spritesheet_t *) wrenSetSlotNewForeign(vm, 0, 0, sizeof(spritesheet_t));
  buffer_t *buffer = get_image_data(vm, 1);
  u16 width = (u16) wrenGetSlotDouble(vm, 2);
  u16 height = (u16) wrenGetSlotDouble(vm, 3);

  /* initialize */
  *self = (spritesheet_t) {
    .tile_width = width,
    .tile_height = height,
    .buffer = buffer,
  };
}

static void w_spritesheet_finalize(void *bytes) {
  spritesheet_t *self = (spritesheet_t *) bytes;

  /* FIXME: cleanup */
  free(self->buffer->data);
  free(self->buffer);
  self = NULL;
}

void w_spritesheet_get_width(WrenVM *vm) {
  /* retrieve from wren */
  spritesheet_t *self = (spritesheet_t *) wrenGetSlotForeign(vm, 0);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, spritesheet_get_width(self));
}

void w_spritesheet_get_height(WrenVM *vm) {
  /* retrieve from wren */
  spritesheet_t *self = (spritesheet_t *) wrenGetSlotForeign(vm, 0);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, spritesheet_get_height(self));
}

const static function_desc functions[] = {
  /* properties */
  { false, "width", w_spritesheet_get_width },
  { false, "height", w_spritesheet_get_height },

  /* always last. */
  { false, NULL, NULL },
};

void wren_spritesheet(WrenVM *vm) {
  wren_register_class(vm, &(class_desc) {
    .name = "Spritesheet",
    .type = CLASS_SPRITESHEET,
    .methods = {
      .allocate = w_spritesheet_allocate,
      .finalize = w_spritesheet_finalize,
    },
    .functions = functions,
  });
}
