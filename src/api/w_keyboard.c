/* std */
#include <stdbool.h>

/* engine */
#include "engine.h"
#include "keyboard.h"
#include "common/runtime.h"

#define self ((keyboard_t *) ((engine_t *) wrenGetUserData(vm))->keyboard)

void w_keyboard_get_key_repeat(WrenVM *vm) {
  bool enabled = keyboard_has_key_repeat(self);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, enabled);
}

void w_keyboard_set_key_repeat(WrenVM *vm) {
  bool enable = wrenGetSlotBool(vm, 1);
  keyboard_set_key_repeat(self, enable);
}

void w_keyboard_is_down(WrenVM *vm) {
  /* retrieve from wren */
  const char *key = wrenGetSlotString(vm, 1);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotBool(vm, 0, keyboard_is_down(self, key));
}

void w_keyboard_get_key_constant(WrenVM *vm) {
  /* retrieve from wren */
  const char *key = wrenGetSlotString(vm, 1);
  int index = keyboard_get_key_constant(self, key);

  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, index);
}

const static function_desc functions[] = {
  /* properties */
  { true, "keyRepeat", w_keyboard_get_key_repeat },
  { true, "keyRepeat=(_)", w_keyboard_set_key_repeat },

  /* functions */
  { true, "isDown(_)", w_keyboard_is_down },
  
  /* always last. */
  { false, NULL, NULL },
};

void wren_keyboard(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->keyboard = keyboard_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Keyboard",
    .type = CLASS_KEYBOARD,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}