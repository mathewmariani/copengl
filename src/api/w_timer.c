/* engine */
#include "engine.h"
#include "timer.h"
#include "common/runtime.h"

#define self ((timer_t *) ((engine_t *) wrenGetUserData(vm))->timer)

void w_timer_get_fps(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, timer_get_fps(self));
}

void w_timer_get_time(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, timer_get_time(self));
}

void w_timer_sleep(WrenVM *vm) {
  /* retrieve from wren */
  double ms = wrenGetSlotDouble(vm, 1);
  timer_sleep(ms);
}

void w_timer_step(WrenVM *vm) {
  /* return to wren */
  wrenEnsureSlots(vm, 1);
  wrenSetSlotDouble(vm, 0, timer_step(self));
}

const static function_desc functions[] = {
  /* properties */
  { true, "fps", w_timer_get_fps },
  { true, "time", w_timer_get_time },

  /* functions */
  { true, "sleep(_)", w_timer_sleep },
  { true, "step()", w_timer_step },

  /* always last. */
  { false, NULL, NULL },
};

void wren_timer(WrenVM *vm) {
  /* initialize module */
  engine_t *app = (engine_t *) wrenGetUserData(vm);
  app->timer = timer_create();

  /* register class */
  wren_register_class(vm, &(class_desc) {
    .name = "Timer",
    .type = CLASS_TIMER,
    .methods = {
      .allocate = NULL,
      .finalize = NULL,
    },
    .functions = functions,
  });
}
