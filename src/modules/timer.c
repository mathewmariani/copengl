/* engine */
#include "timer.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* platform specific */
#ifdef OS_WINDOWS
  #ifndef WIN32_LEAN_AND_MEAN
  #define WIN32_LEAN_AND_MEAN
  #endif
  #include <windows.h>
#elif OS_MACOS
  #include <unistd.h>
  #include <mach/mach_time.h>
#endif

#define LITE_SECOND       1.0
#define LITE_MILLISECOND  1000.0
#define LITE_MICROSECOND  1000000.0

/* timer module */
struct timer_t {
  f64 start;
  f64 curr;
  f64 prev;
  f64 delta;
  f64 last;

  i32 fps;
  i32 frames;
};

#ifdef OS_WINDOWS

LITE_PRIVATE f64 get_frequency() {
  LARGE_INTEGER li;
  QueryPerformanceFrequency(&li);
  return ((f64) li.QuadPart) * LITE_SECOND;
}

LITE_PRIVATE u64 get_time_absolute() {
  LARGE_INTEGER li;
  QueryPerformanceCounter(&li);
  return (u64) li.QuadPart;
}

#else

LITE_PRIVATE mach_timebase_info_data_t get_timebase_info() {
  mach_timebase_info_data_t info;
  mach_timebase_info(&info);
  return info;
}

#endif

timer_t *timer_create() {
  timer_t *timer = malloc(sizeof(timer_t));
  *timer = (timer_t) {
    .start = 0.0,
    .curr = 0.0,
    .prev = 0.0,
    .delta = 0.0,
    .last = 0.0,
    .fps = 0,
    .frames = 0,
  };

#ifdef OS_WINDOWS
  timer->start = get_time_absolute();
#elif OS_MACOS
  timer->start = mach_absolute_time();
#endif

  return timer;
}

void timer_destroy(timer_t *timer) {
  LITE_FREE(timer);
  timer = NULL;
}

i32 timer_get_fps(timer_t *timer) {
  return timer->fps;
}

f64 timer_get_time(timer_t *timer) {
#ifdef OS_WINDOWS
  const f64 now = get_time_absolute();
  const f64 frequency = get_frequency();
  return ((f64) (now - timer->start)) / frequency;
#else
  const mach_timebase_info_data_t timebase = get_timebase_info();
  const u64 mach_now = mach_absolute_time() - timer->start;
  return ((f64) mach_now * 1.0e-9) * (f64) timebase.numer / (f64) timebase.denom;
#endif
}

void timer_sleep(f64 ms) {
  if (ms >= 0) {
    SDL_Delay((u32) ms);
  }
}

f64 timer_step(timer_t *timer) {
  timer->frames++;

  timer->prev = timer->curr;
  timer->curr = timer_get_time(timer);
  timer->delta = timer->curr - timer->prev;

  f64 du = timer->curr - timer->last;
  if (du > 1) {
    timer->fps = (i32) ((timer->frames / du) + 0.5);
    timer->last = timer->curr;
    timer->frames = 0;
  }

  return timer->delta;
}
