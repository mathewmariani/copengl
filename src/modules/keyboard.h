#ifndef KEYBOARD_H
#define KEYBOARD_H

/* engine */
#include "common/platform.h"

/* keyboard module */
typedef struct keyboard_t keyboard_t;

keyboard_t *keyboard_create(void);
void keyboard_destroy(keyboard_t *keyboard);

bool keyboard_has_key_repeat(keyboard_t *keyboard);
void keyboard_set_key_repeat(keyboard_t *keyboard, bool enable);

int keyboard_get_key_constant(keyboard_t *keyboard, const char *key_name);

bool keyboard_is_down(keyboard_t *keyboard, const char *key_name);

#endif
