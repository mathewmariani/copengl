#ifndef ENGINE_H
#define ENGINE_H

/* engine */
#include "common/platform.h"

/* forward declarations */
typedef struct filesystem_t filesystem_t;
typedef struct graphics_t graphics_t;
typedef struct keyboard_t keyboard_t;
typedef struct mouse_t mouse_t;
typedef struct timer_t timer_t;
typedef struct window_t window_t;

typedef struct {
  const char *title;
  const char *argv;
  int width;
  int height;
} engine_desc;

typedef struct {
  bool running;

  /* modules */
  filesystem_t *filesystem;
  graphics_t *graphics;
  keyboard_t *keyboard;
  mouse_t *mouse;
  timer_t *timer;
  window_t *window;
} engine_t;

engine_t *engine_create(engine_desc *desc);
void engine_destroy(engine_t *engine);

void engine_run(engine_t *engine);

bool engine_is_running(engine_t *engine);

#endif
