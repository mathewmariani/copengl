/* engine */
#include "engine.h"
#include "filesystem.h"
#include "graphics.h"
#include "keyboard.h"
#include "mouse.h"
#include "timer.h"
#include "window.h"
#include "common/platform.h"
#include "common/runtime.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* scripts */
#include "embedded/boot.wren.h"

/* forward declaration */
void wren_engine(WrenVM *vm);

engine_t *engine_create(engine_desc *desc) {
  /* set sdl option */
  u32 sdl_options = SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_TIMER;
  if (SDL_Init(sdl_options)) {
    return NULL;
  }

  engine_t *engine = (engine_t *) LITE_MALLOC(sizeof(engine_t));
  *engine = (engine_t) {
    .running = true,

    /* initialize modules to null */
    .filesystem = NULL,
    .graphics = NULL,
    .keyboard = NULL,
    .mouse = NULL,
    .timer = NULL,
    .window = NULL,
  };

  return engine;
}

void engine_destroy(engine_t *engine) {
  /* modules */
  filesystem_destroy(engine->filesystem);
  graphics_destroy(engine->graphics);
  keyboard_destroy(engine->keyboard);
  mouse_destroy(engine->mouse);
  timer_destroy(engine->timer);
  window_destroy(engine->window);

  /* always last */
  LITE_FREE(engine);
  engine = NULL;
}

static void handle_keyboard_event(engine_t *engine, const SDL_Event e) {
  keyboard_t *keyboard = engine->keyboard;
  switch (e.type) {
    case SDL_KEYDOWN:
    case SDL_KEYUP:
      /* check for key repeat */
      if (e.key.repeat) {
        if (!keyboard_has_key_repeat(keyboard)) {
          break;
        }
      }
  }
}

static void handle_mouse_event(engine_t *engine, const SDL_Event e) {
  mouse_t *mouse = engine->mouse;
  switch (e.type) {
    case SDL_MOUSEMOTION:
      // mouse->x = e.motion.x;
      // mouse->y = e.motion.y;
      // mouse->xrel = e.motion.xrel;
      // mouse->yrel = -e.motion.yrel;
      //mouse_set_x(mouse, e.motion.x, e.motion.y);
      break;

    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP: {
      bool is_down = (e.type == SDL_MOUSEBUTTONDOWN) ? true : false;
      switch (e.button.button) {
        case SDL_BUTTON_LEFT:
          // mouse->left_button = is_down;
          break;
        case SDL_BUTTON_RIGHT:
          // mouse->right_button = is_down;
          break;
        case SDL_BUTTON_MIDDLE:
          // mouse->middle_button = is_down;
          break;
      }

      // mouse->x = e.button.x;
      // mouse->y = e.button.y;

      if (e.button.clicks == 1) {
        // mouse->click_type = MOUSE_CLICK_SINGLE;
      } else if (e.button.clicks == 2) {
        // mouse->click_type = MOUSE_CLICK_DOUBLE;
      }
      break;
    }
  }
}

static void handle_window_event(engine_t *engine, const SDL_Event e) {
  window_t *window = engine->window;
  switch (e.window.event) {
  case SDL_WINDOWEVENT_RESIZED:
    // window->state.resized = true;
    // window->w = e.window.data1;
    // window->h = e.window.data2;
    break;

  case SDL_WINDOWEVENT_MOVED:
    // window->state.moved = true;
    // window->w = e.window.data1;
    // window->h = e.window.data2;
    break;

  case SDL_WINDOWEVENT_MINIMIZED:
    // window->state.minimized = true;
    break;

  case SDL_WINDOWEVENT_MAXIMIZED:
    // window->state.maximized = true;
    break;

  case SDL_WINDOWEVENT_RESTORED:
    // window->state.restored = true;
    break;

  case SDL_WINDOWEVENT_ENTER:
    // window->state.has_mouse_focus = true;
    break;

  case SDL_WINDOWEVENT_LEAVE:
    // window->state.has_mouse_focus = false;
    break;

  case SDL_WINDOWEVENT_FOCUS_GAINED:
    // window->state.has_keyboard_focus = true;
    break;

  case SDL_WINDOWEVENT_FOCUS_LOST:
    // window->state.has_keyboard_focus = false;
    break;
  }
}

void engine_run(engine_t *engine) {
  /* initialize wren */
  WrenConfiguration config;
  wrenInitConfiguration(&config);

  /* setup callbacks */
  config.loadModuleFn = wren_load_module;
  config.bindForeignClassFn = wren_bind_class;
  config.bindForeignMethodFn = wren_bind_method;
  config.writeFn = wren_write;
  config.errorFn = wren_error;
  config.userData = engine;

  WrenVM *vm = wrenNewVM(&config);

  /* initialize wren modules */
  wren_engine(vm);

  /* create window */
  window_set_mode(engine->window);
  graphics_set_mode(engine->graphics);

  WrenInterpretResult result = wrenInterpret(vm, "boot", (const char *) boot_wren);

  switch (result) {
    case WREN_RESULT_COMPILE_ERROR:
      printf("Compile Error!\n");
      goto close;
      break;
    case WREN_RESULT_RUNTIME_ERROR:
      printf("Runtime Error!\n");
      goto close;
      break;
    case WREN_RESULT_SUCCESS:
      printf("Success!\n");
      break;
  }

  /* get enginelication class */
  wrenEnsureSlots(vm, 1);
  wrenGetVariable(vm, "boot", "app", 0);
  WrenHandle *engineClass = wrenGetSlotHandle(vm, 0);

  /* create handles */
  WrenHandle *tickMethod = wrenMakeCallHandle(vm, "tick()");
  WrenHandle *keyboardMethod = wrenMakeCallHandle(vm, "i_keyboardState(_,_)");
  WrenHandle *mouseMethod = wrenMakeCallHandle(vm, "i_mouseState(_,_)");

  SDL_Event e;
  while (engine->running) {

    /* reset frame specific data */
    // engine->mouse->xrel = 0;
    // engine->mouse->yrel = 0;

    while (SDL_PollEvent(&e) != 0) {
      switch (e.type) {
      /* quit event */
      case SDL_QUIT:
        engine->running = false;
        break;

      /* keyboard events */
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        handle_keyboard_event(engine, e);
        break;

      /* mouse events */
      case SDL_MOUSEMOTION:
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
        handle_mouse_event(engine, e);
        break;

      /* window event */
      case SDL_WINDOWEVENT:
        handle_window_event(engine, e);
        break;

      /* next case here */
      }
    }

    /* tick */
    wrenEnsureSlots(vm, 1);
    wrenSetSlotHandle(vm, 0, engineClass);
    wrenCall(vm, tickMethod);
  }

close:

  /* release handles */
  wrenReleaseHandle(vm, engineClass);
  wrenReleaseHandle(vm, tickMethod);
  wrenReleaseHandle(vm, keyboardMethod);
  wrenReleaseHandle(vm, mouseMethod);

  /* close virtual machine */
  wrenFreeVM(vm);

  /* subsystems */
  /* TODO: uninitialize subsystems */

  /* sdl2 resources */
  SDL_Quit();
}
