#ifndef AUDIO_H
#define AUDIO_H

#include "source.h"

void audio_init(void);
void audio_deinit(void);

float audio_getVolume(void);
void audio_setVolume(float volume);

void audio_play(void *handle);

#endif
