/* std */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* engine */
#include "common/platform.h"
#include "common/config.h"
#include "graphics.h"

/* opengl */
#define GL_USE_GLAD
#define GL_USE_SDL2
#define GL_IMPLEMENTATION
#include "system/opengl.h"

/* embedded */
#include "embedded/vertex.vs.h"
#include "embedded/fragment.fs.h"

/* initialize opengl functions */
typedef struct {
  u8 background;
  clip_t clip;
  struct {
    u8 low;
    u8 high;
  } color;
  u16 pattern;
} drawstate_t;

/* graphics module */
struct graphics_t {
  /* video memory */
  struct {
    u8 *data;
    u8 mapping[256];
  } vram;

  /* current draw state */
  drawstate_t draw_state;

  /* opengl resources */
  struct {
    gl_buffer_t *vbo;
    gl_buffer_t *ebo;
    gl_shader_t *shader;
    gl_pipeline_t *pipeline;
    gl_bindings_t bindings;
    gl_image_t *image;
  } opengl;
};

static inline void poke(const void *dst, const i32 offset, const u8 c) {
  *((u8 *) dst + offset) = c;
}

static inline u8 get_pattern(const graphics_t *graphics, const i32 x, const i32 y) {
  const u8 i = 0xF - ((x & 0x3) + 0x4 * (y & 0x3));
  return (((graphics->draw_state.pattern & (1 << i)) >> i) == 0) ?
    graphics->draw_state.color.low :
    graphics->draw_state.color.high;
}

static inline u8 mapped_color(const graphics_t *graphics, u8 c) {
  return *(graphics->vram.mapping + c);
}

typedef struct { i32 x, y; u8 c; } pixel_t;
static inline void set_pixel(graphics_t *graphics, const pixel_t p) {
  const u32 x = p.x;
  const u32 y = p.y;

  if (x <  graphics->draw_state.clip.x1 ||
      y <  graphics->draw_state.clip.y1 ||
      x >= graphics->draw_state.clip.x2 ||
      y >= graphics->draw_state.clip.y2) {
    return;
  }
  
  const u8 c = p.c ? p.c : get_pattern(graphics, x, y);
  poke(graphics->vram.data, x + y * LITE_WIDTH, mapped_color(graphics, c));
}

static void draw_hline(graphics_t *graphics, i32 x, i32 y, i32 w) {
  for (i16 i = x; i < (x + w); ++i) {
    set_pixel(graphics, (pixel_t) { .x = i, .y = y });
  }
}

static void draw_vline(graphics_t *graphics, i32 x, i32 y, i32 h) {
  for (i16 j = y; j < (y + h); ++j) {
    set_pixel(graphics, (pixel_t) { .x = x, .y = j });
  }
}

static void draw_rect(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  for (i16 j = y; j < (y + h); ++j) {
    for (i16 i = x; i < (x + w); ++i) {
      set_pixel(graphics, (pixel_t) { .x = i, .y = j });
    }
  }
}

graphics_t *graphics_create(void) {
  graphics_t *graphics = LITE_MALLOC(sizeof(graphics_t));
  *graphics = (graphics_t) {
    /* video ram */
    .vram = {
      .data = NULL,
      .mapping = 0,
    },

    /* draw state */
    .draw_state = (drawstate_t) {
      .background = 0x00,
      .clip = (clip_t) {
        .x1 = 0,
        .y1 = 0,
        .x2 = LITE_WIDTH,
        .y2 = LITE_HEIGHT,
      },
      .color = 0x00,
      .pattern = 0b0000000000000000,
    },

    /* opengl resources */
    .opengl = {
      .vbo = NULL,
      .ebo = NULL,
      .shader = NULL,
      .pipeline = NULL,
      .image = NULL,
    },
  };

  return graphics;
}

void graphics_destroy(graphics_t *graphics) {
  /* opengl resources */
  gl_destroy_buffer(graphics->opengl.vbo);
  gl_destroy_buffer(graphics->opengl.ebo);
  gl_destroy_pipeline(graphics->opengl.pipeline);
  gl_destroy_shader(graphics->opengl.shader);
  gl_destroy_image(graphics->opengl.image);

  /* cleanup context */
  gl_shutdown();

  LITE_FREE(graphics->vram.data);
  LITE_FREE(graphics);
  graphics = NULL;
}

void graphics_set_mode(graphics_t *graphics) {
  /* screen buffer */
  graphics->vram.data = LITE_CALLOC(LITE_HEIGHT * LITE_WIDTH, sizeof(u8));
  if (graphics->vram.data == NULL) {
    printf("\n===\nHey I think there was an error here...\n===\n");
  }

  /* initialize palette mapping */
  for (i32 i = 0; i < MAX_COLORS; ++i) {
    *(graphics->vram.mapping + i) = i;
  }

  /* initialize opengl */
  gl_setup();

  /* opengl state */

  /* viewport */
  gl_apply_viewport(0, 0, LITE_WIDTH, LITE_HEIGHT);

  /* set up element data */
  const u8 indices[] = {
    0, 1, 3,
    1, 2, 3,
  };

  /* set up vertex data */
  const f32 vertices[] = {
    /* vertices */  /* texture coords */
    1.0f,  1.0f,    1.0f, 0.0f,
    1.0f, -1.0f,    1.0f, 1.0f,
   -1.0f, -1.0f,    0.0f, 1.0f,
   -1.0f,  1.0f,    0.0f, 0.0f 
  };

  /* screen texture */
  gl_image_t *image = gl_create_image(&(gl_texture_desc) {
    .width = LITE_WIDTH,
    .height = LITE_HEIGHT,
    .target = GL_TEXTURE_2D,
    .internal = GL_R8,
    .filter = GL_NEAREST,
    .wrap = GL_CLAMP_TO_EDGE,
  });

  /* create opengl resources  */
  gl_buffer_t *vbo = gl_create_buffer(&(gl_buffer_desc) {
    .target = GL_ARRAY_BUFFER,
    .usage = GL_STATIC_DRAW,
    .udata = (gl_userdata_t) {
      .data = vertices,
      .size = sizeof(vertices),
    },
  });

  gl_buffer_t *ebo = gl_create_buffer(&(gl_buffer_desc) {
    .target = GL_ELEMENT_ARRAY_BUFFER,
    .usage = GL_STATIC_DRAW,
    .udata = (gl_userdata_t) {
      .data = indices,
      .size = sizeof(indices),
    },
  });

  gl_shader_t *shader = gl_create_shader(&(gl_shader_desc) {
    .vs_source = (const char *) vertex_vs,
    .fs_source = (const char *) fragment_fs,
  });

  gl_pipeline_t *pipeline = gl_create_pipeline(&(gl_pipeline_desc) {
    .shader = shader,
    .blend_mode = GL_NO_MODE,
    .cull_mode = GL_BACK,
    .depth_test = GL_NO_TEST,
    .winding = GL_CW,
    .primitive_type = GL_TRIANGLES,
    .index_type = GL_UNSIGNED_BYTE,
    .layout = {
      .attr = {
        [0] = { .format = GL_FLOAT, .size = 2, .offset = 0 },
        [1] = { .format = GL_FLOAT, .size = 2, .offset = 2 },
      },
    },
  });

  gl_bindings_t bindings = (gl_bindings_t) {
    .vertex_buffer = vbo,
    .index_buffer = ebo,
    .image = {
      [1] = image,
    },
  };

  /* cache */
  graphics->opengl.vbo = vbo;
  graphics->opengl.ebo = ebo;
  graphics->opengl.shader = shader;
  graphics->opengl.pipeline = pipeline;
  graphics->opengl.bindings = bindings;
  graphics->opengl.image = image;
}

i32 graphics_get_width(const graphics_t *graphics) {
  return LITE_WIDTH;
}

i32 graphics_get_height(const graphics_t *graphics) {
  return LITE_HEIGHT;
}

u8 graphics_get_background(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->background;
}

void graphics_set_background(graphics_t *graphics, u8 color) {
  drawstate_t *state = &graphics->draw_state;
  state->background = color;
}

clip_t *graphics_get_clipping(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return &state->clip;
}

void graphics_set_clipping(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 y2) {
  drawstate_t *state = &graphics->draw_state;
  state->clip = (clip_t) {
    .x1 = x1,
    .y1 = y1,
    .x2 = x2,
    .y2 = y2,
  };
}

u8 graphics_get_color(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->color.low;
}

void graphics_set_color(graphics_t *graphics, const u8 c0, const u8 c1) {
  drawstate_t *state = &graphics->draw_state;
  state->color.low = c0;
  state->color.high = c1;
}

u16 graphics_get_pattern(const graphics_t *graphics) {
  const drawstate_t *state = &graphics->draw_state;
  return state->pattern;
}

void graphics_set_pattern(graphics_t *graphics, const u16 bstr) {
  drawstate_t *state = &graphics->draw_state;
  state->pattern = bstr;
}

static void draw_ellipse(graphics_t *graphics, i32 x0, i32 y0, i32 a, i32 b) {
  if ((a <= 0) || (b <= 0)) {
    return;
  }

  i64 aa2 = a * a * 2;
  i64 bb2 = b * b * 2;

  {
    i64 x = a, y = 0;
    i64 dx = (1 - 2 * a)*b*b, dy = a * a;
    i64 sx = bb2 * a, sy = 0;
    i64 e = 0;

    while (sx >= sy) {
      graphics_set_pixel(graphics, x0 + x, y0 + y); /*   I. Quadrant */
      graphics_set_pixel(graphics, x0 + x, y0 - y); /*  II. Quadrant */
      graphics_set_pixel(graphics, x0 - x, y0 + y); /* III. Quadrant */
      graphics_set_pixel(graphics, x0 - x, y0 - y); /*  IV. Quadrant */

      y++; sy += aa2; e += dy; dy += aa2;
      if (2 * e + dx >0) {
        x--; sx -= bb2; e += dx; dx += bb2;
      }
    }
  }

  {
    i64 x = 0, y = b;
    i64 dx = b * b, dy = (1 - 2 * b)*a*a;
    i64 sx = 0, sy = aa2 * b;
    i64 e = 0;

    while (sy >= sx) {
      graphics_set_pixel(graphics, x0 + x, y0 + y); /*   I. Quadrant */
      graphics_set_pixel(graphics, x0 + x, y0 - y); /*  II. Quadrant */
      graphics_set_pixel(graphics, x0 - x, y0 + y); /* III. Quadrant */
      graphics_set_pixel(graphics, x0 - x, y0 - y); /*  IV. Quadrant */

      x++; sx += bb2; e += dx; dx += bb2;
      if (2 * e + dy >0) {
        y--; sy -= aa2; e += dy; dy += aa2;
      }
    }
  }
}

static void draw_ellipse_filled(graphics_t *graphics, i32 x0, i32 y0, i32 a, i32 b) {
  #define FILL_ROW(startx, endx, starty)\
    do {\
      i16 sx = (startx);\
      i16 ex = (endx);\
      i16 sy = (starty);\
      if (sy < 0 || sy >= LITE_HEIGHT) break;\
      if (sx < 0) sx = 0;\
      if (sx > LITE_WIDTH) sx = LITE_WIDTH;\
      if (ex < 0) ex = 0;\
      if (ex > LITE_WIDTH) ex = LITE_WIDTH;\
      if (sx == ex) break;\
      for (i16 i = 0; i < (ex - sx); i++) {\
        graphics_set_pixel(graphics, sx+i, sy);\
      }\
    } while (0)
 
  if ((a <= 0) || (b <= 0)) {
    return;
  }
 
  i64 aa2 = a * a * 2;
  i64 bb2 = b * b * 2;
 
  {
    i64 x = a, y = 0;
    i64 dx = (1 - 2 * a)*b*b, dy = a * a;
    i64 sx = bb2 * a, sy = 0;
    i64 e = 0;
 
    while (sx >= sy) {
      FILL_ROW((x0 - x), (x0 + x), (y0 + y));
      FILL_ROW((x0 - x), (x0 + x), (y0 - y));
 
      y++; sy += aa2; e += dy; dy += aa2;
      if (2 * e + dx >0) {
        x--; sx -= bb2; e += dx; dx += bb2;
      }
    }
  }
 
  {
    i64 x = 0, y = b;
    i64 dx = b * b, dy = (1 - 2 * b)*a*a;
    i64 sx = 0, sy = aa2 * b;
    i64 e = 0;
 
    while (sy >= sx) {
      FILL_ROW((x0 - x), (x0 + x), (y0 + y));
      FILL_ROW((x0 - x), (x0 + x), (y0 - y));
 
      x++; sx += bb2; e += dx; dx += bb2;
      if (2 * e + dy >0) {
        y--; sy -= aa2; e += dy; dy += aa2;
      }
    }
  }

#undef FILL_ROW
}

void graphics_circ(graphics_t *graphics, i32 x, i32 y, i32 r) {
  draw_ellipse(graphics, x, y, r, r);
}

void graphics_circfill(graphics_t *graphics, i32 x, i32 y, i32 r) {
  draw_ellipse_filled(graphics, x, y, r, r);
}

void graphics_clear(graphics_t *graphics) {
  const size_t size = (LITE_WIDTH * LITE_HEIGHT);
  const u8 c = graphics->draw_state.background;
  for (i32 i = 0; i < size; ++i) {
    *(graphics->vram.data + i) = mapped_color(graphics, c);
  }
}

void graphics_ellipse(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b) {
  draw_ellipse(graphics, x, y, a, b);
}

void graphics_ellipsefill(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b) {
  draw_ellipse_filled(graphics, x, y, a, b);
}

void graphics_line(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 y2) {
  #define SWAP_INT(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))
  i16 steep = abs(y2 - y1) > abs(x2 - x1);
  if (steep) {
    SWAP_INT(x1, y1);
    SWAP_INT(x2, y2);
  }
  if (x1 > x2) {
    SWAP_INT(x1, x2);
    SWAP_INT(y1, y2);
  }
  #undef SWAP_INT

  const i16 dx = x2 - x1;
  const i16 dy = abs(y2 - y1);
  const i16 ystep = (y1 < y2) ? 1 : -1;
  i16 err = dx / 2;
  i16 x, y = y1;
  for (x = x1; x < x2; x++) {
    if (steep) {
      graphics_set_pixel(graphics, x, y);
    } else {
      graphics_set_pixel(graphics, x, y);
    }
    err -= dy;
    if (err < 0) {
      y += ystep;
      err += dx;
    }
  }
}

void graphics_pal(graphics_t *graphics, u8 c0, u8 c1, bool reset) {
  if (reset) {
    for (i32 i = 0; i < MAX_COLORS; ++i) {
      *(graphics->vram.mapping + i) = i;
    }
    return;
  }
  *(graphics->vram.mapping + c0) = c1;
}

void graphics_points(graphics_t *graphics, i32 *positions, int count) {
  for (i32 i = 0; i < count; i += 2) {
    set_pixel(graphics, (pixel_t) {
      .x = *(positions + i + 0),
      .y = *(positions + i + 1),
    });
  }
}

void graphics_present(const graphics_t *graphics) {
  /* copy buffer to gpu */
  gl_update_image(graphics->opengl.image, &(gl_userdata_t) {
    .data = graphics->vram.data,
    .size = sizeof(graphics->vram.data),
  });

  /* clear to transparent black */
  gl_clear(&(gl_clear_desc) {
    .action = GL_COLOR_BUFFER_BIT,
    .value = (gl_color) {
      .r = 0.0f,
      .g = 0.0f,
      .b = 0.0f,
      .a = 0.0f,
    },
  });

  gl_apply_pipeline(graphics->opengl.pipeline);
  gl_apply_bindings(&graphics->opengl.bindings);

  /* draw fullscreen quad */
  gl_draw(0, 6, 0);
}

void graphics_rect(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  draw_hline(graphics, x, y, w);
  draw_hline(graphics, x, y + h - 1, w);
  draw_vline(graphics, x, y, h);
  draw_vline(graphics, x + w - 1, y, h);
}

void graphics_rectfill(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h) {
  draw_rect(graphics, x, y, w, h);
}

void graphics_set_pixel(graphics_t *graphics, i32 x, i32 y) {
  set_pixel(graphics, (pixel_t) {
    .x = x,
    .y = y
  });
}
