#ifndef SOURCE_H
#define SOURCE_H

#include "common/platform.h"

#ifdef AUDIO_USE_MINIAUDIO
  #include "libraries/dr/miniaudio.h"
#endif

#ifdef AUDIO_USE_CUTE
  #include "libraries/cute/cute_sound.h"
#endif

#ifdef AUDIO_USE_SOKOL

#endif

typedef struct {
  float volume;
  float duration;

#ifdef AUDIO_USE_CUTE
  cs_loaded_sound_t loaded;
  cs_playing_sound_t sound;
  cs_play_sound_def_t def;
#endif
} source_t;

void source_init(source_t *self, const char *filename);
void source_deinit(source_t *self);

#endif
