#ifndef GRAPHICS_H
#define GRAPHICS_H

/* engine */
#include "common/platform.h"

typedef struct {
  i32 x1, y1;
  i32 x2, y2;
} clip_t;

/* graphics module */
typedef struct graphics_t graphics_t;

graphics_t *graphics_create(void);
void graphics_destroy(graphics_t *graphics);

void graphics_set_mode(graphics_t *graphics);

i32 graphics_get_width(const graphics_t *graphics);
i32 graphics_get_height(const graphics_t *graphics);

u8 graphics_get_background(const graphics_t *graphics);
void graphics_set_background(graphics_t *graphics, u8 color);

clip_t *graphics_get_clipping(const graphics_t *graphics);
void graphics_set_clipping(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 i32);

u8 graphics_get_color(const graphics_t *graphics);
void graphics_set_color(graphics_t *graphics, const u8 c0, const u8 c1);

u16 graphics_get_pattern(const graphics_t *graphics);
void graphics_set_pattern(graphics_t *graphics, const u16 bstr);

void graphics_circ(graphics_t *graphics, i32 x, i32 y, i32 r);
void graphics_circfill(graphics_t *graphics, i32 x, i32 y, i32 r);
void graphics_clear(graphics_t *graphics);
void graphics_ellipse(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b);
void graphics_ellipsefill(graphics_t *graphics, i32 x, i32 y, i32 a, i32 b);
void graphics_line(graphics_t *graphics, i32 x1, i32 y1, i32 x2, i32 y2);
void graphics_pal(graphics_t *graphics, u8 c0, u8 c1, bool reset);
void graphics_points(graphics_t *graphics, i32 *buffer, int count);
void graphics_present(const graphics_t *graphics);
void graphics_rect(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h);
void graphics_rectfill(graphics_t *graphics, i32 x, i32 y, i32 w, i32 h);

void graphics_set_pixel(graphics_t *graphics, i32 x, i32 y);

#endif