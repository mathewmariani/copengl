#ifndef WINDOW_H
#define WINDOW_H

/* engine */
#include "common/platform.h"

/* window module */
typedef struct window_t window_t;

window_t *window_create(void);
void window_destroy(window_t *window);

bool window_set_mode(window_t *window);

void window_set_title(window_t *window, const char *title);

/* could be something new to look into */
void window_size(window_t *window, int *w, int *h);
void window_position(window_t *window, int *x, int *y);

void window_swap_buffers(window_t *window);

// bool window_was_size_changed(engine_t* app);
// bool window_was_moved(engine_t* app);

// bool window_keyboard_lost_focus(engine_t* app);
// bool window_keyboard_gained_focus(engine_t* app);
// bool window_keyboard_has_focus(engine_t* app);

// bool window_was_minimized(engine_t* app);
// bool window_was_maximized(engine_t* app);
// bool window_is_minimized(engine_t* app);
// bool window_is_maximized(engine_t* app);
// bool window_was_restored(engine_t* app);

// bool window_mouse_entered(engine_t* app);
// bool window_mouse_exited(engine_t* app);
// bool window_mouse_inside(engine_t* app);

#endif
