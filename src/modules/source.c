#include <stdio.h>
#include <stdlib.h>

#include "source.h"
#include "filesystem.h"

void source_init(source_t *self, const char *filename) {
  /* load file data */
//   size_t length;
//   void *memory = filesystem_read(filename, &length);
//   if (!memory) {
//     printf("could not read file\n");
//     return;
//   }

// #ifdef AUDIO_USE_CUTE
//   cs_read_mem_ogg(memory, length, &self->loaded);
//   self->def = cs_make_def(&self->loaded);
//   self->sound = cs_make_playing_sound(&self->loaded);
// #endif

  /* free resources */
  // free(memory);

  // stb_vorbis *stream = stb_vorbis_open_memory(filedata, size, NULL, NULL);

  // /* free resources */
  // free(filedata);

  // if (!stream) {
  //   printf("could not load sound data\n");
  // }

  // NOTE: OGG bit rate defaults to 16 bit
  // unsigned int sampleCount = stb_vorbis_stream_length_in_samples(stream);
  // stb_vorbis_info info = stb_vorbis_get_info(stream);

  // self->volume = 1.0f;
  // self->duration = stb_vorbis_stream_length_in_seconds(stream);
  // self->data = (int16_t *) malloc(sampleCount * info.channels * sizeof(int16_t));

  // stb_vorbis_get_samples_short_interleaved(stream, info.channels, (int16_t *)buffer, sampleCount * info.channels);

// #ifdef DEBUG 
  // printf("[%s] info:\n", filename);
  // printf("\tsample rate: %d\n", info.sample_rate);
  // printf("\tsampleCount: %d\n", stb_vorbis_stream_length_in_samples(stream));
  // printf("\tchannels   : %d\n", info.channels);
  // printf("\tduration   : %f\n", stb_vorbis_stream_length_in_seconds(stream));
// #endif

  // ummmmm maybe...
  // cs_read_mem_ogg(filedata, size, &self->loaded);
  // self->def = cs_make_def(&self->loaded);
  // self->sound = cs_make_playing_sound(&self->loaded);

  // ma_audio_buffer_config config = ma_audio_buffer_config_init(ma_format_f32, info.channels, sampleCount, NULL, NULL);
  // ma_result result = ma_audio_buffer_init_copy(&config, &self->buffer); // NOTE: It will allocate but won't copy.

  /* free resources */
  // free(filedata);
  // stb_vorbis_close(stream);
}

void source_deinit(source_t *self) {
  // ma_audio_buffer_uninit(&self->buffer);
}