/* std */
#include <string.h>

/* engine */
#include "keyboard.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* keyboard module */
struct keyboard_t {
  bool key_repeat;
};

/* FIXME: use scancode */
/* NOTE: 191 keycodes */
static struct { char *name; SDL_Keycode keycode; } key_entries[] = {
  { "unknown", SDLK_UNKNOWN },

  { "return", SDLK_RETURN },
  { "escape", SDLK_ESCAPE },
  { "backspace", SDLK_BACKSPACE },
  { "tab", SDLK_TAB },
  { "space", SDLK_SPACE },
  { "!", SDLK_EXCLAIM },
  { "\"", SDLK_QUOTEDBL },
  { "#", SDLK_HASH },
  { "%", SDLK_PERCENT },
  { "$", SDLK_DOLLAR },
  { "&", SDLK_AMPERSAND },
  { "'", SDLK_QUOTE },
  { "(", SDLK_LEFTPAREN },
  { ")", SDLK_RIGHTPAREN },
  { "*", SDLK_ASTERISK },
  { "+", SDLK_PLUS },
  { ",", SDLK_COMMA },
  { "-", SDLK_MINUS },
  { ".", SDLK_PERIOD },
  { "/", SDLK_SLASH },
  { "0", SDLK_0 },
  { "1", SDLK_1 },
  { "2", SDLK_2 },
  { "3", SDLK_3 },
  { "4", SDLK_4 },
  { "5", SDLK_5 },
  { "6", SDLK_6 },
  { "7", SDLK_7 },
  { "8", SDLK_8 },
  { "9", SDLK_9 },
  { ":", SDLK_COLON },
  { ";", SDLK_SEMICOLON },
  { "<", SDLK_LESS },
  { "=", SDLK_EQUALS },
  { ">", SDLK_GREATER },
  { "?", SDLK_QUESTION },
  { "@", SDLK_AT },

  { "[", SDLK_LEFTBRACKET },
  { "\\", SDLK_BACKSLASH },
  { "]", SDLK_RIGHTBRACKET },
  { "^", SDLK_CARET },
  { "_", SDLK_UNDERSCORE },
  { "`", SDLK_BACKQUOTE },
  { "a", SDLK_a },
  { "b", SDLK_b },
  { "c", SDLK_c },
  { "d", SDLK_d },
  { "e", SDLK_e },
  { "f", SDLK_f },
  { "g", SDLK_g },
  { "h", SDLK_h },
  { "i", SDLK_i },
  { "j", SDLK_j },
  { "k", SDLK_k },
  { "l", SDLK_l },
  { "m", SDLK_m },
  { "n", SDLK_n },
  { "o", SDLK_o },
  { "p", SDLK_p },
  { "q", SDLK_q },
  { "r", SDLK_r },
  { "s", SDLK_s },
  { "t", SDLK_t },
  { "u", SDLK_u },
  { "v", SDLK_v },
  { "w", SDLK_w },
  { "x", SDLK_x },
  { "y", SDLK_y },
  { "z", SDLK_z },

  { "capslock", SDLK_CAPSLOCK },

  { "f1", SDLK_F1 },
  { "f2", SDLK_F2 },
  { "f3", SDLK_F3 },
  { "f4", SDLK_F4 },
  { "f5", SDLK_F5 },
  { "f6", SDLK_F6 },
  { "f7", SDLK_F7 },
  { "f8", SDLK_F8 },
  { "f9", SDLK_F9 },
  { "f10", SDLK_F10 },
  { "f11", SDLK_F11 },
  { "f12", SDLK_F12 },

  { "printscreen", SDLK_PRINTSCREEN },
  { "scrolllock", SDLK_SCROLLLOCK },
  { "pause", SDLK_PAUSE },
  { "insert", SDLK_INSERT },
  { "home", SDLK_HOME },
  { "pageup", SDLK_PAGEUP },
  { "delete", SDLK_DELETE },
  { "end", SDLK_END },
  { "pagedown", SDLK_PAGEDOWN },
  { "right", SDLK_RIGHT },
  { "left", SDLK_LEFT },
  { "down", SDLK_DOWN },
  { "up", SDLK_UP },

  { "numlock", SDLK_NUMLOCKCLEAR },
  { "kp/", SDLK_KP_DIVIDE },
  { "kp*", SDLK_KP_MULTIPLY },
  { "kp-", SDLK_KP_MINUS },
  { "kp+", SDLK_KP_PLUS },
  { "kpenter", SDLK_KP_ENTER },
  { "kp0", SDLK_KP_0 },
  { "kp1", SDLK_KP_1 },
  { "kp2", SDLK_KP_2 },
  { "kp3", SDLK_KP_3 },
  { "kp4", SDLK_KP_4 },
  { "kp5", SDLK_KP_5 },
  { "kp6", SDLK_KP_6 },
  { "kp7", SDLK_KP_7 },
  { "kp8", SDLK_KP_8 },
  { "kp9", SDLK_KP_9 },
  { "kp.", SDLK_KP_PERIOD },
  { "kp,", SDLK_KP_COMMA },
  { "kp=", SDLK_KP_EQUALS },

  { "application", SDLK_APPLICATION },
  { "power", SDLK_POWER },
  { "f13", SDLK_F13 },
  { "f14", SDLK_F14 },
  { "f15", SDLK_F15 },
  { "f16", SDLK_F16 },
  { "f17", SDLK_F17 },
  { "f18", SDLK_F18 },
  { "f19", SDLK_F19 },
  { "f20", SDLK_F20 },
  { "f21", SDLK_F21 },
  { "f22", SDLK_F22 },
  { "f23", SDLK_F23 },
  { "f24", SDLK_F24 },
  { "execute", SDLK_EXECUTE },
  { "help", SDLK_HELP },
  { "menu", SDLK_MENU },
  { "select", SDLK_SELECT },
  { "stop", SDLK_STOP },
  { "again", SDLK_AGAIN },
  { "undo", SDLK_UNDO },
  { "cut", SDLK_CUT },
  { "copy", SDLK_COPY },
  { "paste", SDLK_PASTE },
  { "find", SDLK_FIND },
  { "mute", SDLK_MUTE },
  { "volumeup", SDLK_VOLUMEUP },
  { "volumedown", SDLK_VOLUMEDOWN },

  { "alterase", SDLK_ALTERASE },
  { "sysreq", SDLK_SYSREQ },
  { "cancel", SDLK_CANCEL },
  { "clear", SDLK_CLEAR },
  { "prior", SDLK_PRIOR },
  { "return2", SDLK_RETURN2 },
  { "separator", SDLK_SEPARATOR },
  { "out", SDLK_OUT },
  { "oper", SDLK_OPER },
  { "clearagain", SDLK_CLEARAGAIN },

  { "thsousandsseparator", SDLK_THOUSANDSSEPARATOR },
  { "decimalseparator", SDLK_DECIMALSEPARATOR },
  { "currencyunit", SDLK_CURRENCYUNIT },
  { "currencysubunit", SDLK_CURRENCYSUBUNIT },

  { "lctrl", SDLK_LCTRL },
  { "lshift", SDLK_LSHIFT },
  { "lalt", SDLK_LALT },
  { "lgui", SDLK_LGUI },
  { "rctrl", SDLK_RCTRL },
  { "rshift", SDLK_RSHIFT },
  { "ralt", SDLK_RALT },
  { "rgui", SDLK_RGUI },

  { "mode", SDLK_MODE },

  { "audionext", SDLK_AUDIONEXT },
  { "audioprev", SDLK_AUDIOPREV },
  { "audiostop", SDLK_AUDIOSTOP },
  { "audioplay", SDLK_AUDIOPLAY },
  { "audiomute", SDLK_AUDIOMUTE },
  { "mediaselect", SDLK_MEDIASELECT },
  { "www", SDLK_WWW },
  { "mail", SDLK_MAIL },
  { "calculator", SDLK_CALCULATOR },
  { "computer", SDLK_COMPUTER },
  { "appsearch", SDLK_AC_SEARCH },
  { "apphome", SDLK_AC_HOME },
  { "appback", SDLK_AC_BACK },
  { "appforward", SDLK_AC_FORWARD },
  { "appstop", SDLK_AC_STOP },
  { "apprefresh", SDLK_AC_REFRESH },
  { "appbookmarks", SDLK_AC_BOOKMARKS },

  { "brightnessdown", SDLK_BRIGHTNESSDOWN },
  { "brightnessup", SDLK_BRIGHTNESSUP },
  { "displayswitch", SDLK_DISPLAYSWITCH },
  { "kbdillumtoggle", SDLK_KBDILLUMTOGGLE },
  { "kbdillumdown", SDLK_KBDILLUMDOWN },
  { "kbdillumup", SDLK_KBDILLUMUP },
  { "eject", SDLK_EJECT },
  { "sleep", SDLK_SLEEP },

  /* always last. */
  { 0, 0 },
};

static const SDL_Keycode search_for_key(const char *key) {
  int i = 0;
  char *entry = key_entries[i].name;
  while (entry) {
    if (strcmp(entry, key) == 0) {
      return key_entries[i].keycode;
    }
    entry = key_entries[++i].name;
  }
  return 0;
}

keyboard_t *keyboard_create(void) {
  keyboard_t *keyboard = LITE_MALLOC(sizeof(keyboard_t));
  *keyboard = (keyboard_t) {
    .key_repeat = false,
  };

  return keyboard;
}

void keyboard_destroy(keyboard_t *keyboard) {
  LITE_FREE(keyboard);
  keyboard = NULL;
}

bool keyboard_has_key_repeat(keyboard_t *keyboard) {
  return keyboard->key_repeat;
}

void keyboard_set_key_repeat(keyboard_t *keyboard, bool enable) {
  keyboard->key_repeat = enable;
}

int keyboard_get_key_constant(keyboard_t *keyboard, const char *key_name) {
  return search_for_key(key_name);
}

bool keyboard_is_down(keyboard_t *keyboard, const char *key_name) {
  const u8 *state = SDL_GetKeyboardState(NULL);

  /* FIXME: use scancode */
  SDL_Keycode keycode = search_for_key(key_name);
  SDL_Scancode scancode = SDL_GetScancodeFromKey(keycode);

  if (state[scancode]) {
    return true;
  }

  return false;
}
