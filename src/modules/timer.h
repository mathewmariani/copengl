#ifndef TIMER_H
#define TIMER_H

/* engine */
#include "common/platform.h"

/* timer module */
typedef struct timer_t timer_t;

timer_t *timer_create();
void timer_destroy(timer_t *timer);

i32 timer_get_fps(timer_t *timer);
f64 timer_get_time(timer_t *timer);
void timer_sleep(f64 ms);
f64 timer_step(timer_t *timer);

#endif
