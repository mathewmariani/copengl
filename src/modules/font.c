/* std */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* engine */
#include "font.h"

static const char *ttf_utf8_to_codepoint(const char *p, i16 *res) {
  i16 x, mask, shift;
  switch (*p & 0xf0) {
    case 0xf0:
      mask = 0x07; shift = 18; break;
    case 0xe0:
      mask = 0x0f; shift = 12; break;
    case 0xc0:
    case 0xd0:
      mask = 0x1f; shift =  6; break;
    default:
      *res = *p;
      return p + 1;
  }
  x = (*p & mask) << shift;
  do {
    if (*(++p) == '\0') {
      *res = x;
      return p;
    }
    shift -= 6;
    x |= (*p & 0x3f) << shift;
  } while (shift);
  *res = x;
  return p + 1; 
}

static float ttf_char_width(ttf_t *self, i16 c, i16 last) {
  i32 res = 0;
  i32 width, lsb;
  stbtt_GetCodepointHMetrics(&self->font, c, &width, &lsb);
  res = width;
  if (last) {
    i16 kerning = stbtt_GetCodepointKernAdvance(&self->font, last, c);
    res += kerning;
  }
  return res * self->scale;
}

void ttf_init(ttf_t *self, i16 ptsize) {
  /* initialize font */
  if (!stbtt_InitFont(&self->font, self->data, 0)) {
    printf("could not load font\n");
    return;
  }

  /* get font metrics */
  i32 ascent, descent, lineGap;
  stbtt_GetFontVMetrics(&self->font, &ascent, &descent, &lineGap);
  self->ptsize = ptsize;
  self->scale = stbtt_ScaleForMappingEmToPixels(&self->font, self->ptsize);
  self->baseline = ascent * self->scale + 1;
}

void ttf_deinit(ttf_t *self) {
  LITE_FREE(self->data);
  self = NULL;
}

i16 ttf_width(ttf_t *self, const char *str) {
  f32 res = 0;
  i16 last = 0;
  const char *p = str;
  while (*p) {
    i16 c;
    p = ttf_utf8_to_codepoint(p, &c);
    res += ttf_char_width(self, c, last);
    last = c;
  }
  return ceil(res);
}

i16 ttf_height(ttf_t *self) {
  i32 ascent, descent, lineGap;
  stbtt_GetFontVMetrics(&self->font, &ascent, &descent, &lineGap);
  return ceil((ascent - descent + lineGap) * self->scale) + 1;
}

u8 *ttf_render(ttf_t *self, const char *str, i16 *w, i16 *h) {
  *w = ttf_width(self, str);
  *h = ttf_height(self);

  u8 *pixels = (u8 *) LITE_CALLOC(*w * *h, sizeof(u8));
  if (!pixels) {
    return NULL;
  }

  const char *p = str;
  f32 xoffset = 0;
  f32 xfract = 0;
  i16 last = 0;
  while (*p) {
    /* Get unicode codepoint */
    i16 c;
    p = ttf_utf8_to_codepoint(p, &c);

    /* Get char placement coords */
    i32 x0, y0, x1, y1;
    stbtt_GetCodepointBitmapBox(
      &self->font, c, self->scale, self->scale,
      &x0, &y0, &x1, &y1);

    /* Work out position / max size */
    i16 x = xoffset + x0;
    i16 y = self->baseline + y0;
    if (x < 0) {
      x = 0;
    }

    if (y < 0) {
      y = 0;
    }

    /* Render char */
    stbtt_MakeCodepointBitmap(
      &self->font,
      pixels + x + (y * *w),
      *w - x, *h - y, *w, self->scale, self->scale, c);

    /* Next */
    xoffset += ttf_char_width(self, c, last);
    xfract = xoffset - (i16) xoffset;
    last = c;
  }

  return pixels;
}
