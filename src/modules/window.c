/* std */
#include <stdio.h>

/* engine */
#include "graphics.h"
#include "window.h"
#include "common/config.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* window state */
typedef struct {
  bool has_mouse_focus;
  bool has_keyboard_focus;
  bool minimized;
  bool maximized;
  bool restored;
  bool resized;
  bool moved;
} window_state_t;

/* window module */
struct window_t {
  SDL_Window *window;
  SDL_GLContext *context;
  window_state_t state;
  int x;
  int y;
  int w;
  int h;
};

//bool window_isFullscreen(void) {
//  uint32_t flags = SDL_GetWindowFlags(window);
//  return (flags & SDL_WINDOW_FULLSCREEN_DESKTOP);
//}
//
//void window_setFullscreen(bool enable) {
//  if (enable) {
//    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
//  } else {
//    SDL_SetWindowFullscreen(window, 0);
//  }
//}
//
//void window_swap(void) {
//  SDL_GL_SwapWindow(window);
//}
//
//void *window_handle(void) {
//  return window;
//}

window_t *window_create(void) {
  window_t *window = LITE_MALLOC(sizeof(window_t));
  *window = (window_t) {
    .state = (window_state_t) {
      .has_mouse_focus = false,
      .has_keyboard_focus = false,
      .minimized = false,
      .maximized = false,
      .restored = false,
      .resized = false,
      .moved = false,
    },
    .x = 0,
    .y = 0,
    .w = 0,
    .h = 0,

    /* sdl2 resources */
    .window = NULL,
    .context = NULL,
  };

  return window;
}

void window_destroy(window_t *window) {
  /* sdl2 resources */
  SDL_DestroyWindow(window->window);
  SDL_GL_DeleteContext(window->context);

  LITE_FREE(window);
  window = NULL;
}

bool window_set_mode(window_t *window) {
  /* set sdl window flags */
  u32 sdl_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

  /* set opengl context attributes */
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

  int x = SDL_WINDOWPOS_CENTERED_DISPLAY(0);
  int y = SDL_WINDOWPOS_CENTERED_DISPLAY(0);

  /* create sdl2 window */
  window->window = SDL_CreateWindow("LITE", x, y, LITE_WIDTH, LITE_HEIGHT, sdl_flags);
  window->w = LITE_WIDTH;
  window->h = LITE_HEIGHT;

  if (!window->window) {
    fprintf(stderr, "Unable to create SDL window.\n");
    goto fail;
  }

  // FIXME: this might not be needed.
  SDL_GL_SetSwapInterval(1);

  /* create opengl context */
  window->context = SDL_GL_CreateContext(window->window);

  if (!window->context) {
    fprintf(stderr, "Unable to create OpenGL context.\n");
    goto fail;
  }

  return true;
  
fail:
  SDL_DestroyWindow(window->window);
  return false;
}

void window_set_title(window_t *window, const char *title) {
  SDL_SetWindowTitle(window->window, title);
}

void window_size(window_t *window, int *w, int *h) {
  if (w) { *w = window->w; }
  if (h) { *h = window->h; }
}

void window_position(window_t *window, int *x, int *y) {
  if (x) { *x = window->x; }
  if (y) { *y = window->y; }
}

void window_swap_buffers(window_t *window) {
  SDL_GL_SwapWindow(window->window);
}
