#ifndef FILESYSTEM_H
#define FILESYSTEM_H

/* engine */
#include "common/data.h"
#include "common/platform.h"

/* filesystem module */
typedef struct filesystem_t filesystem_t;

filesystem_t *filesystem_create(void);
void filesystem_destroy(filesystem_t *filesystem);

bool filesystem_exists(const filesystem_t *filesystem, const char *filename);
bool filesystem_is_file(const filesystem_t *filesystem, const char *filename);
bool filesystem_is_directory(const filesystem_t *filesystem, const char *filename);
void *filesystem_read(const filesystem_t *filesystem, const char *path, size_t *size);
data_t *filesystem_read_data(const filesystem_t *filesystem, const char *path);
bool filesystem_write(const filesystem_t *filesystem, const char *filename, const char *data);

#endif
