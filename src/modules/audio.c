#include <stdio.h>

#include "audio.h"
#include "source.h"
#include "window.h"
#include "common/platform.h"

#ifdef AUDIO_USE_MINIAUDIO
  #include "libraries/dr/miniaudio.h"
  ma_device_config config;
  ma_device device;
#endif

#ifdef AUDIO_USE_CUTE
  #include "libraries/cute/cute_sound.h"
  cs_context_t *context;
#endif

#ifdef AUDIO_USE_SOKOL
  #include "libraries/sokol/sokol_audio.h"
#endif

#ifdef AUDIO_USE_MINIAUDIO
void data_callback(ma_device *device, void *output, const void *input, ma_uint32 count) {
  printf("%s : %d\n", __FUNCTION__, count);
}
#endif

#ifdef AUDIO_USE_SOKOL

#endif

void audio_init(void) {

#ifdef AUDIO_USE_MINIAUDIO
  ma_device_config config = ma_device_config_init(ma_device_type_playback);
  config.playback.format   = ma_format_unknown;
  config.playback.channels = 0;
  config.sampleRate        = 0;
  config.dataCallback      = data_callback;
  // deviceConfig.pUserData         = &decoder;

  if (ma_device_init(NULL, &config, &device) != MA_SUCCESS) {
    printf("Failed to open playback device.\n");
    return;
  }
#endif

#ifdef AUDIO_USE_CUTE
  context = cs_make_context(NULL, 44100, 4096 * 2, 0, NULL);
#endif

#ifdef AUDIO_USE_SOKOL
  saudio_setup(&(saudio_desc){ 0 });
#endif
}

void audio_deinit(void) {
#ifdef AUDIO_USE_MINIAUDIO
  ma_device_uninit(&device);
#endif

#ifdef AUDIO_USE_CUTE
  cs_shutdown_context(context);
#endif

#ifdef AUDIO_USE_SOKOL
  saudio_shutdown();
#endif
}

float audio_getVolume() {
#ifdef AUDIO_USE_MINIAUDIO
  float volume;
  ma_device_get_master_volume(&device, &volume);
  return volume;
#endif

  return 0.0f;
}

void audio_setVolume(float volume) {
#ifdef AUDIO_USE_MINIAUDIO
  ma_device_set_master_volume(&device, volume);
#endif
}

void audio_play(void *handle) {
#ifdef AUDIO_USE_MINIAUDIO
  if (ma_device_start(&device) != MA_SUCCESS) {
    printf("Failed to start playback device.\n");
  }
#endif

#ifdef AUDIO_USE_CUTE
  source_t *source = (source_t *) handle;

  printf("playing: %p\n", source);
  // cs_loop_sound((cs_playing_sound_t *) handle, 1);
  // cs_set_volume((cs_playing_sound_t *) handle, 0.5f, 0.5f);
  int success = cs_insert_sound(context, &source->sound);
  if (success) {
    printf("sound was insterted\n");
  }
  cs_play_sound(context, source->def);
#endif
}