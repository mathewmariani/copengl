#ifndef MOUSE_H
#define MOUSE_H

/* engine */
#include "common/platform.h"

/* mouse module */
typedef struct mouse_t mouse_t;

mouse_t *mouse_create(void);
void mouse_destroy(mouse_t *mouse);

i32 mouse_get_x(const mouse_t *mouse);
void mouse_set_x(mouse_t *mouse, i32 x);

i32 mouse_get_y(const mouse_t *mouse);
void mouse_set_y(mouse_t *mouse, i32 y);

i32 mouse_get_xrel(const mouse_t *mouse);
void mouse_set_xrel(mouse_t *mouse, i32 xrel);

i32 mouse_get_yrel(const mouse_t *mouse);
void mouse_set_yrel(mouse_t *mouse, i32 yrel);

bool mouse_is_down(i32 button);

#endif
