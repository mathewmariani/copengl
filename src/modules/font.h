#ifndef TTF_H
#define TTF_H

/* engine */
#include "common/platform.h"

/* libs */
#include "libraries/stb/stb_truetype.h"

/* font module */
typedef struct {
  stbtt_fontinfo font;
  void *data;

  f32 ptsize;
  f32 scale;
  i16 baseline;
} ttf_t;

void ttf_init(ttf_t *self, i16 ptsize);
void ttf_deinit(ttf_t *self);

i16 ttf_width(ttf_t *self, const char *str);
i16 ttf_height(ttf_t *self);

u8 *ttf_render(ttf_t *self, const char *str, i16 *w, i16 *h);

#endif
