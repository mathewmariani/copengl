/* engine */
#include "mouse.h"

/* sdl2 */
#include <SDL2/SDL.h>

/* mouse module */
struct mouse_t {
  i32 x;
  i32 y;
  i32 xrel;
  i32 yrel;
};

mouse_t *mouse_create(void) {
  mouse_t *mouse = LITE_MALLOC(sizeof(mouse_t));
  *mouse = (mouse_t) {
    .x = 0,
    .y = 0,
    .xrel = 0,
    .yrel = 0,
  };

  return mouse;
}

void mouse_destroy(mouse_t *mouse) {
  LITE_FREE(mouse);
  mouse = NULL;
}

i32 mouse_get_x(const mouse_t *mouse) {
  i32 x;
  SDL_GetMouseState(&x, NULL);
  return x;
}

void mouse_set_x(mouse_t *mouse, i32 x) {
  mouse->x = x;
}

i32 mouse_get_y(const mouse_t *mouse) {
  i32 y;
  SDL_GetMouseState(NULL, &y);
  return y;
}

void mouse_set_y(mouse_t *mouse, i32 y) {
  mouse->y = y;
}

i32 mouse_get_xrel(const mouse_t *mouse) {
  return mouse->xrel;
}

void mouse_set_xrel(mouse_t *mouse, i32 xrel) {
  mouse->xrel = xrel;
}

i32 mouse_get_yrel(const mouse_t *mouse) {
  return mouse->yrel;
}

void mouse_set_yrel(mouse_t *mouse, i32 yrel) {
  mouse->yrel = yrel;
}

bool mouse_is_down(i32 button) {
  /* swap SDL buttons */
  switch (button) {
    case 2:
      button = SDL_BUTTON_RIGHT;
      break;

    case 3:
      button = SDL_BUTTON_MIDDLE;
      break;
  }

  u32 buttonstate = SDL_GetMouseState(NULL, NULL);
  return buttonstate & SDL_BUTTON(button);
}