/* std */
#include <stdio.h>
#include <string.h>

/* libs */
#include "libraries/dr/dr_fs.h"

/* engine */
#include "common/platform.h"
#include "filesystem.h"

#ifdef OS_WINDOWS
  #include <direct.h>
  #define GetCurrentDir _getcwd
#else
  #include <unistd.h>
  #define GetCurrentDir getcwd
#endif

/* filesystem module */
struct filesystem_t {
  drfs_context* context;
};

filesystem_t *filesystem_create(void) {
  filesystem_t *filesystem = LITE_MALLOC(sizeof(filesystem_t));
  *filesystem = (filesystem_t) {
    .context = NULL,
  };

  /* create drfs context */
  filesystem->context = drfs_create_context();
  if (filesystem->context == NULL) {
    printf("Filesystem context cannot be created\n");
    return NULL;
  }

  /* find current directory of executable */
  char buff[FILENAME_MAX];
  GetCurrentDir(buff, FILENAME_MAX);

  /* no file writing can be done outside this directory */
  drfs_add_base_directory(filesystem->context, buff);

  return filesystem;
}

void filesystem_destroy(filesystem_t *filesystem) {
  if (filesystem->context) {
    drfs_delete_context(filesystem->context);
  }

  LITE_FREE(filesystem);
  filesystem = NULL;
}

bool filesystem_exists(const filesystem_t *filesystem, const char *filename) {
  return drfs_exists(filesystem->context, filename);
}

bool filesystem_is_file(const filesystem_t *filesystem, const char *filename) {
  return drfs_is_existing_file(filesystem->context, filename);
}

bool filesystem_is_directory(const filesystem_t *filesystem, const char *filename) {
  return drfs_is_existing_directory(filesystem->context, filename);
}

void *filesystem_read(const filesystem_t *filesystem, const char *path, size_t *size) {
  drfs_file *file;
  if (drfs_open(filesystem->context, path, DRFS_READ, &file) != drfs_success) {
    goto fail;
  }

  dr_uint64 fileSize = drfs_size(file);
  if (fileSize > SIZE_MAX) {
    goto fail;
  }

  void *data = LITE_MALLOC((size_t)fileSize + 1);
  if (data == NULL) {
    goto fail;
  }

  if (drfs_read(file, data, (size_t)fileSize, NULL) != drfs_success) {
    LITE_FREE(data);
    if (size != NULL) {
      *size = 0;
    }
    goto fail;
  }

  ((char *)data)[fileSize] = '\0';

  if (size != NULL) {
    *size = (size_t)fileSize;
  }

  drfs_close(file);
  return data;

fail:
  drfs_close(file);
  return NULL;
}

data_t *filesystem_read_data(const filesystem_t *filesystem, const char *path) {
  size_t size;
  void *filedata = filesystem_read(filesystem, path, &size);
  if (!filedata) {
    return NULL;
  }

  return &(data_t) {
    .data = filedata,
    .size = size,
  };
}

bool filesystem_write(const filesystem_t *filesystem, const char *filename, const char *data) {
  drfs_result result = drfs_unknown_error;

  drfs_file *file;
  result = drfs_open(filesystem->context, filename, DRFS_WRITE | DRFS_TRUNCATE, &file);
  if (result != drfs_success) {
    return false;
  }

  result = drfs_write(file, data, strlen(data), NULL);
  drfs_close(file);

  return result == drfs_success;
}
