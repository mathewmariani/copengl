import "lite" for
  Graphics,
  Filesystem,
  Font,
  Keyboard,
  Mouse,
  Window,
  Source,
  Timer

class Application {
  construct new() {
    System.print("Application constructed")

    import "main" for Main
    _game = Main.new()
	  _game.load()

    /* ignore the first step */
    Timer.step()
  }

  tick() {
    var dt = Timer.step()

    /* update */
    _game.update(dt)

    /* graphics */
    Graphics.present()
    _game.draw()

    Timer.sleep(1)
  }
}

var app = Application.new()