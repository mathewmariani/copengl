import "acorn" for
  Graphics,
  Filesystem,
  Font,
  Keyboard,
  Mouse,
  Window,
  Source,
  Spritesheet,
  Timer,
  Rect

class Main {
  construct new() {
    System.print("constructed")
  }

  load() {
    // position
    _x = 0
    _y = 0

    // vanilla milkshake
    Graphics.palette = [
      0x28282eff, 0x6c5671ff, 0xd9c8bfff, 0xf98284ff,
      0xb0a9e4ff, 0xaccce4ff, 0xb3e3daff, 0xfeaae4ff,
      0x87a889ff, 0xb0eb93ff, 0xe9f59dff, 0xffe6c6ff,
      0xdea38bff, 0xffc384ff, 0xfff7a0ff, 0xfff7e4ff,
    ]

    _spr = Spritesheet.new("palette64.png", 0, 0)
    Graphics.spritesheet = _spr

    // quads
    _src = Rect.new(16, 16, 32, 32)
    _dst = Rect.new(0, 0, 64, 64)

    // color to clear the screen
    Graphics.background = 0x1
  }

  update(dt) {
    Window.title = (Timer.fps).toString

    if (Keyboard.isDown("a") || Keyboard.isDown("left")) {
      _dst.x = _dst.x + 1
    } else if (Keyboard.isDown("d") || Keyboard.isDown("right")) {
      _dst.x = _dst.x + 1
    }
  }

  draw() {
    Graphics.clear(0x1)
    Graphics.sspr(_src, _dst)

    Graphics.color = 0x0303
    Graphics.rectangle("line", 0, 0, 64, 64)
  }
}