import "acorn" for
  Graphics,
  Filesystem,
  Font,
  Keyboard,
  Mouse,
  Window,
  Source,
  Timer,
  Random,
  Rect

class Main {
  construct new() {
    System.print("constructed")
  }

  load() {
    // position
//     _x = 0
//     _y = 0

    // vanilla milkshake
    Graphics.palette = [
      0x28282eff, 0x6c5671ff, 0xd9c8bfff, 0xf98284ff,
      0xb0a9e4ff, 0xaccce4ff, 0xb3e3daff, 0xfeaae4ff,
      0x87a889ff, 0xb0eb93ff, 0xe9f59dff, 0xffe6c6ff,
      0xdea38bff, 0xffc384ff, 0xfff7a0ff, 0xfff7e4ff,
    ]

    // color to clear the screen
    Graphics.background = 0x02
    Graphics.color = 0x03

    // 1110
    // 1010
    // 1110
    // 0000
    //Graphics.pattern = 0xEAE0

    // 1100
    // 1100
    // 0011
    // 0011
//     Graphics.pattern = 0xCC33


    _t = 0
    _blink_frame = false
  }

  update(dt) {
    Window.title = (Timer.fps).toString

//     if (Keyboard.isDown("a") || Keyboard.isDown("left")) {
//       _x = _x - 1
//     } else if (Keyboard.isDown("d") || Keyboard.isDown("right")) {
//       _x = _x + 1
//     }

//     if (Keyboard.isDown("w") || Keyboard.isDown("up")) {
//       _y = _y - 1
//     } else if (Keyboard.isDown("s") || Keyboard.isDown("down")) {
//       _y = _y + 1
//     }

    _t = (_t + 1) % 30
    _blink_frame = (_t == 0)
  }

  draw() {
    Graphics.clear()

//     Graphics.pal()

//     if (_blink_frame) {
//       Graphics.pal(0x02, 0x0A)
//     }
    var pts = [1, 1, 2, 2, 3, 3, 4, 4]
    Graphics.points(pts)

//     Graphics.color = 0x03
//     Graphics.rectangle("fill", _x, _y, 16, 16)
  }
}