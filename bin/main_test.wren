import "acorn" for
  Graphics,
  Filesystem,
  Image,
  Keyboard,
  Mouse,
  Window,
  Source,
  Timer,
  Font

class Main {
  construct new() {
    System.print("constructed")
  }

  load() {
    Window.title = "Entertainment Engine"
    Window.fullscreen = false

    // pico-8
//     Graphics.palette = [
//       0x000000FF, 0x1D2B53FF, 0x7E2553FF, 0x008751FF,
//       0xAB5236FF, 0x5F574FFF, 0xC2C3C7FF, 0xFFF1E8FF,
//       0xFF004DFF, 0xFFA300FF, 0xFFEC27FF, 0x00E436FF,
//       0x29ADFFFF, 0x83769CFF, 0xFF77A8FF, 0xFFCCAAFF,
//     ]

    // vanilla milkshake
    Graphics.palette = [
      0x28282eff, 0x6c5671ff, 0xd9c8bfff, 0xf98284ff,
      0xb0a9e4ff, 0xaccce4ff, 0xb3e3daff, 0xfeaae4ff,
      0x87a889ff, 0xb0eb93ff, 0xe9f59dff, 0xffe6c6ff,
      0xdea38bff, 0xffc384ff, 0xfff7a0ff, 0xfff7e4ff,
    ]

    // 1110
    // 1010
    // 1110
    // 0000
    Graphics.pattern = 0xEAE0

    _fnt = Font.new("font.ttf", 16)
  }

  update(dt) {
    Window.title = (Timer.fps).toString
  }

  draw() {
    Graphics.clear(0x1)

    Graphics.color = 0x0204
    Graphics.circle("fill", 25, 25, 12)
    Graphics.print(_fnt, "Hello, World!", 0, 0)
  }
}