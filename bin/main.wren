import "lite" for
  Graphics,
  Filesystem,
  Font,
  Keyboard,
  Mouse,
  Window,
  Source,
  Timer,
  Rect

class Main {
  construct new() {
    System.print("constructed")
  }

  load() {
    Window.title = "Entertainment Engine"
    Window.fullscreen = true

    // color to clear the screen
    Graphics.background = 0x1

    // 0000
    // 0100
    // 0000
    // 1111
    Graphics.pattern = 0x040F

    // 1110
    // 1010
    // 1110
    // 0000
    Graphics.pattern = 0xEAE0

    // 1010
    // 0000
    // 1010
    // 0000
    //Graphics.pattern = 0xA0A0
    
    // 1010
    // 0000
    // 0101
    // 0000
    //Graphics.pattern = 0xA050

    // 1100
    // 1100
    // 0011
    // 0011
    //Graphics.pattern = 0xCC33
  }

  update(dt) {
    Window.title = (Timer.fps).toString
  }

  draw() {
    var time = Timer.time

    var w = 256
    var h = 144

    var b = 16
    for (y in 0..h-1) {
      for (x in 0..w-1) {
        //// creates a simple circle
        //var cx = (x - w / 2.0)
        //var cy = (y - h / 2.0)
        //var cc = 0.035 * (cx * cx + cy * cy).sqrt - (time * 0.8)
        //var c1 = cc
        //var c2 = cc + 0.5

        // polar cooridate flower
        var cx = (x - w / 2.0)
        var cy = (y - h / 2.0)
        var cr = (cx * cx + cy * cy + 1).sqrt
        var theta = cy.atan(cx) + (time * 0.075)
        var r = 60 * (4 * theta).cos + cr

        var cc = 0.025 * r - (time * 0.375)
        var c1 = cc
        var c2 = cc + 0.5

        // just picking colors based off c
        var low = (c1 - (b * (c1 / b).floor)).floor + 1
        var high = (c2 - (b * (c2 / b).floor)).floor + 1

        Graphics.color = (low << 8) | high
        Graphics.pset(x, y)
      }
    }
  }
}