# lite API

 * [Audio](#audio)
 * [Filesystem](#filesystem)
 * [Graphics](#graphics)
 * [Keyboard](#keyboard)
 * [Mouse](#mouse)
 * [Random](#random)
 * [Timer](#timer)
 * [Window](#window)


## Overview

All functions in lite take the built-in atomic object types provided by Wren; Booleans, Numbers, Strings, and Null.


## Audio

 * [foo](#foo)
 * [bar](#bar)

#### `foo(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.

#### `bar(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.


## Filesystem

 * [exists](#exists(_))
 * [isFile](#isFile(_))
 * [isDirectory](#isDirectory(_))
 * [read](#read(_))
 * [write](#write(_,_))

#### `exists(_)`
Lorem ipsem something else in latin
`_`: This descirbes args1.

#### `isFile(_)`
Lorem ipsem something else in latin
`_`: This descirbes args1.

#### `isDirectory(_)`
Lorem ipsem something else in latin
`_`: This descirbes args1.

#### `read(_)`
Lorem ipsem something else in latin
`_`: This descirbes args1.

#### `write(_,_)`
Lorem ipsem something else in latin
`_`: This descirbes args1.
`_`: This descirbes args2.


## Graphics

 * [foo](#foo)
 * [bar](#bar)

 * [circle](#circle)
 * [line](#line)
 * [tline](#tline)
 * [point](#point)
 * [rectangle](#rectangle)
 * [print](#print)

#### `foo(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.

#### `bar(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.

#### `circle( mode, x, y, r )`
Draws a circle centered at (`x`, `y`) with radius `r`.
`mode`: this describes mode.
`x`: this describes x.
`y`: this describes y.
`r`: this describes r.

#### `line( x1, y1, x2, y2 )`
Draws a line between (`x1`, `y1`) and (`x2`, `y2`).
`x1`: this describes x1.
`y1`: this describes y1.
`x2`: this describes x2.
`y2`: this describes y2.

#### `tline( x1, y1, x2, y2, sx, sy, sdx, sdy )`
Lore ipseum something else in latin.
`x1`: this describes x1.
`y1`: this describes y1.
`x2`: this describes x2.
`y2`: this describes y2.
`sx`: this describes sx.
`sy`: this describes sy.
`sdx`: this describes sdx.
`sdy`: this describes sdy.

#### `point( x, y )`
Draws a point at (`x`, `y`).
`x`: this describes x.
`y`: this describes y.

#### `rectangle( mode, x, y, w, h )`
Draws a rectangle at (`x`, `y`) with dimensions (`w`, `h`).
`mode`: this describes mode.
`x`: The position of top-left corner along the x-axis.
`y`: The position of top-left corner along the x-axis.
`w`: The width of the rectangle.
`h`: The height of the rectangle.

#### `print( font, str, x, y )`
Lore ipseum something else in latin.
`font`: this describes font.
`str`: this describes str.
`x`: this describes x.
`y`: this describes y.


## Keyboard

 * [keyRepeat](#keyRepeat)
 * [isDown](#isDown(_))

#### `keyRepeat`
Lorem ipsum something else in latin.

#### `keyRepeat=(_)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.

#### `isDown(_)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.


## Mouse

 * [x](#x)
 * [y](#y)
 * [xrel](#xrel)
 * [yrel](#yrel)
 * [isDown](#isDown(_))

#### `x`
Lorem ipsum something else in latin.

#### `y`
Lorem ipsum something else in latin.

#### `xrel`
Lorem ipsum something else in latin.

#### `yrel`
Lorem ipsum something else in latin.

#### `isDown(_)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.


## Random

A pseudo random number generator that repeats after 27776 successive calls.

 * [number](#number())
 * [reset](#reset())

#### `number()`
Generates a new 16-bit random number from the previous state.

#### `resets()`
Resets the random seed to its initial state.


## Timer

 * [fps](#fps)
 * [time](#time)
 * [sleep](#sleep(_))
 * [step](#step())

#### `fps`
Lorem ipsum something else in latin.

#### `time`
Lorem ipsum something else in latin.

#### `sleep(_)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.

#### `step()`
Lorem ipsum something else in latin.


## Window

 * [foo](#foo)
 * [bar](#bar)

#### `foo(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.

#### `bar(arg1, arg2)`
Lorem ipsum something else in latin.
`arg1`: This describes arg1.
`arg2`: This describes arg2.
