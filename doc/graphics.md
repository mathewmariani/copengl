# Graphics

## Draw State

- [ ] Background Color
- [ ] Draw Color

- [ ] Clipping Mask
- [ ] Clipping Region

- [ ] Dither Pattern
- [ ] Fill Pattern

- [ ] Palette Modifications
- [ ] Draw Palette
- [ ] Screen Palette

## Draw State

 - [ ] `background( col )`
 - [ ] `clip( [x, y, w, h] )`
 - [ ] `color( col )`
 - [ ] `fillp( [pat] )`
 - [ ] `pal( [c0, c1,] [p] )`

## Drawing

 - [ ] `circ( mode, x, y, r )`
 - [ ] `clear()`
 - [ ] `pget( x, y )`
 - [ ] `present()`
 - [ ] `print( str, x, y )`
 - [ ] `pset( x, y, [c] )`
 - [ ] `line( x1, y1, x2, y2 )`
 - [ ] `rect( mode, x, y, w, h )`

### Sprites

 - [ ] `spr( n, x, y, [w,] [h,] [flip_x,] [flip_y] )`
 - [ ] `sspr( sx, sy, sw, sh, dx, dy, [dw,] [dh,] [flip_x,] [flip_y] )`
 - [ ] `sget( x, y )`
 - [ ] `sset( x, y, [c] )`

### Transform

 - [ ] `translate( x, y )`
 - [ ] `scale( sx, sy )`
 - [ ] `rotate( r )`