import os

def listdir(path):
  return [os.path.join(dp, f) for dp, dn, fn in os.walk(path) for f in fn]

def file_is_hidden(p):
	f = os.path.basename(p)
	print(f, f.startswith('.'))
	return f.startswith('.')

[os.remove(f) for f in listdir('./src') if file_is_hidden(f)]