# LITE

LITE is a \*small\* framework for making 2D games in Wren.


## Building

To build LITE, look inside the `projects/` folder. In here you’ll find ready to go projects for Visual Studio, XCode, and tools like make.

 - **Windows** Open `lite.sln` inside `projects/vs2017`.
 - **macOS** Open `lite.xcworkspace` inside `projects/xcode/`.
 - **Linux** Run `make` inside of `projects/make/`.

In each case, there will be an executable file generated into the root `bin/` folder.

To rebuild these projects run `premake5` inside `projects/premake`.


### Dependencies

LITE uses two open-source libraries [Wren](https://wren.io/) for scripting, and [SDL2](https://www.libsdl.org/) for platform abstractions. These libraries and their development files are required for bulding LITE.


#### Windows

The environment variables `WRENDIR` and `SDL2DIR` are expected by `premake5`.


#### macOS

The required pre-built SDL2 framework is available [here](https://www.libsdl.org/download-2.0.php) and should be placed in `/Library/Frameworks/` in order for the XCode project to recognize it.
Wren includes and lib should be placed in `/usr/local/include` and `/usr/local/lib`, respectfully.