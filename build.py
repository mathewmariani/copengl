#!/usr/bin/python2.7
import os, sys, shutil, re, textwrap

# project variables
BUILD       = "debug"
COMPILER    = "gcc"
SRC_DIR     = "src"
BIN_DIR     = "bin"
BIN_NAME    = "lite"
EMBED_DIR   = "src/embedded"
TEMPSRC_DIR = ".temp"

# environment variables
SDL2DIR = os.environ.get("SDL2DIR")
WRENDIR = os.environ.get("WRENDIR")

# compiler variables
CFLAGS      = [ "-Wall", "-std=gnu11", "-fno-strict-aliasing" ]
DLIBS       = [ "m" ]
DEFINES     = [  ]
INCLUDES    = [ "src", "src/modules", TEMPSRC_DIR ]
FRAMEWORKS  = [  ]
LIBRARIES   = [  ]

# embedded resources variables
EXTENSIONS  = (".wren", ".fs", ".vs")
PATTERN     = "const unsigned char %s[] = {\n%s\n};"


# check platform
if sys.platform == "linux" or sys.platform == "linux2":
  print("building for Linux")
elif sys.platform == "darwin":
  print("building for macOS...")
  DLIBS.extend([ "wren" ])
  FRAMEWORKS.extend([ "SDL2", "OpenGL", "CoreAudio" ])
  COMMAND = "{compiler} {flags} {defines} {includes} -o {outfile} {srcfiles} -F/Library/Frameworks {frameworks} {libs} {argv}"
elif sys.platform == "win32":
  print("building for Windows...")
  DLIBS.extend([ "SDL2main", "SDL2" ])
  INCLUDES.extend([ SDL2DIR + "/include", WRENDIR + "/src/include" ])
  LIBRARIES.extend([ SDL2DIR + "/lib/x86", WRENDIR + "/lib" ])
  COMMAND = "{compiler} {flags} {defines} {includes} -o {outfile} {srcfiles} {libs} {libraries} {argv}"


# optimize build
if BUILD == "release":
  print("optimizing for release...")
  CFLAGS.extend(["-O3", "-s", "-DNDEBUG"])
elif BUILD == "debug":
  print("optimizing for debugging...")
  CFLAGS.extend(["-O0", "-g"])


def fmt(fmt, var):
  for k in var:
    fmt = fmt.replace("{%s}" % str(k), var[k])
  return fmt


def listdir(path):
  return [os.path.join(dp, f) for dp, dn, fn in os.walk(path) for f in fn]


def make_c_include(name, data):
  res = ""
  for c in data:
    res += str("0x{:02x}".format(ord(c))) + ", "
  res = res.rstrip(", ")

  name = re.sub("[^a-z0-9]", "_", name.lower())
  text = textwrap.fill(res, width=79, initial_indent='  ', subsequent_indent='  ')
  return PATTERN % (name, text)

def file_len(name):
  with open(name) as f:
    i = 0
    for i, l in enumerate(f, 1):
      pass

    return i + 1

def analytics():
  # print ("{:<8} {:<15} {:<10} {:<10}".format(k, lang, perc, change))
  data = []
  for extension in [ ".py", ".h", ".c", ".vs", ".fs" ]:
    lines = 0
    for filename in filter(lambda x: x.endswith(extension), listdir(SRC_DIR)):
      lines += file_len(filename)

    data.append([extension, lines]);

  for d in data:
    print("{:<8} {:<10}".format(d[0], d[1]))

def main():
  os.chdir(sys.path[0])

  print("creating directories...")
  if not os.path.exists(BIN_DIR):
    os.makedirs(BIN_DIR)

  if not os.path.exists(TEMPSRC_DIR):
    os.makedirs(TEMPSRC_DIR)

  print("converting embedded files...")
  embedded_files = filter(lambda x: x.endswith((".wren", ".fs", ".vs")), listdir(EMBED_DIR))
  for filename in embedded_files:
    name = os.path.basename(filename)
    text = make_c_include(name, open(filename).read())
    open("%s/%s.h" % (EMBED_DIR, name), "wb").write(text.encode())

  cmd = fmt(COMMAND,
    {
      "compiler"    : COMPILER,
      "flags"       : " ".join(CFLAGS),
      "defines"     : " ".join(map(lambda x: "-D" + x, DEFINES)),
      "includes"    : " ".join(map(lambda x: "-I" + x, INCLUDES)),
      "outfile"     : BIN_DIR + "/" + BIN_NAME,
      "srcfiles"    : " ".join(filter(lambda x: x.endswith(".c"), listdir(SRC_DIR))),
      "libraries"   : " ".join(map(lambda x: "-L" + x, LIBRARIES)),
      "frameworks"  : " ".join(map(lambda x: "-framework " + x, FRAMEWORKS)),
      "libs"        : " ".join(map(lambda x: "-l" + x, DLIBS)),
      "argv"        : " ".join(sys.argv[1:])
    })

  print("compiling...")
  res = os.system(cmd)

  print("deleting temporary files...")
  if os.path.exists(TEMPSRC_DIR):
    shutil.rmtree(TEMPSRC_DIR)

  print("done" + (" with errors" if res else ""))

  print("printing analytics...")
  analytics()

  if not res:
    print("launching...\n")
    os.system("./" + BIN_DIR + "/" + BIN_NAME)


if __name__ == "__main__":
  main()